import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_ecommerce/Pages/Cart.dart';
import 'package:flutter_app_ecommerce/Pages/MyOrders.dart';
import 'package:flutter_app_ecommerce/Pages/Notifications.dart';
import 'package:flutter_app_ecommerce/Pages/Productlist.dart';
import 'package:flutter_app_ecommerce/Pages/ReferandEarn.dart';
import 'package:flutter_app_ecommerce/Pages/SearchPage.dart';
import 'package:flutter_app_ecommerce/Pages/Wallet.dart';
import 'package:flutter_app_ecommerce/Pages/Wishlist.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/Advertisement.dart';
import 'package:flutter_app_ecommerce/domain/Banners.dart';

import 'package:flutter_app_ecommerce/domain/MainCategory.dart';
import 'package:flutter_app_ecommerce/domain/Offer.dart';
import 'package:flutter_app_ecommerce/domain/OfferProduct.dart';
import 'package:flutter_app_ecommerce/domain/Offerlist.dart';
import 'package:flutter_app_ecommerce/domain/ProductDetailsWeb.dart' as prw;
import 'package:flutter_app_ecommerce/domain/ProductWithStocklist.dart';
import 'package:flutter_app_ecommerce/domain/Productwithstock.dart';
import 'package:flutter_app_ecommerce/domain/Slider.dart';
import 'package:flutter_app_ecommerce/domain/SliderData.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:page_indicator/page_indicator.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;

import 'Database/DatabaseHelper.dart';
import 'Database/DatabaseTables.dart';
import 'Pages/ProductDetails.dart';
import 'Pages/Settings.dart';
import 'Pages/withdrawalrequests.dart';
import 'ProfilePage.dart';
import 'constants/DataConstants.dart';
import 'constants/UrlConstants.dart';
import 'dart:convert';



import 'domain/Productwithcategory.dart';
import 'domain/SearchedByWordData.dart';




void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'HomePage'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool _fromTop = true;

  List<SliderData> nsd = [];

  List<Sliderdata> sliderurl = [  ];

  List<Widget> nsdwidget = [];
  List<Offer> offerlist = [];

  List<Categorydata> data = [];

List<Widget> categorydata=[];

bool ishistoryavailable=false;

int selectedindex=0;


List<Data>offerdata=[];

  List<Advertisedata> adv_data=[];

List<prw.Data>prhistory=[];

int cartcount=0;

  @override
  void initState() {
    // TODO: implement initState

getCategories();
getSliders();
getOfferedProduct();
getAdvertisement();
getCartCount();
getHistory();



    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;

    for (var i = 0; i < sliderurl.length; i++) {
      nsdwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.89349112,
        width: double.infinity,
        child: Card(
            child:
            Image.network(UrlData.sliderbaseurl+sliderurl[i].image)),
      ));
    }

   return Scaffold(
     resizeToAvoidBottomInset : false,

     appBar: AppBar(
       elevation: 0,
       flexibleSpace:Container(

         width: double.infinity,
         height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,



         padding: EdgeInsets.all(10),
         color: Color(0xffEFECEC),


         child: new Row(

             mainAxisAlignment: MainAxisAlignment.start,
             crossAxisAlignment: CrossAxisAlignment.stretch,
             children: [

               Expanded(child: Container(


                   margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(5, 15, 0, 0):EdgeInsets.fromLTRB(8, 25, 0, 0):EdgeInsets.fromLTRB(5, 35, 0, 0) ,
                   alignment: Alignment.center,
                   child:
                   new InkWell(
                       onTap: () {

                         showGeneralDialog(
                           barrierLabel: "Label",
                           barrierDismissible: true,
                           barrierColor: Colors.black.withOpacity(0.5),
                           transitionDuration: Duration(milliseconds: 700),
                           context: context,
                           pageBuilder: (context, anim1, anim2) {
                             return Align(
                               alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
                               child: Container(
                                 width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?200:250:300,
                                 height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?350:390:450,

                                 child: SingleChildScrollView(


                                     child: Column(

                                       children: [


                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[




                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {


                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => ProfilePage(title: "profile",)));


                                                         }, child: Text('Profile',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),



                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[





                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {


                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => ReferEarnPage(title: "Refer earn",)));




                                                         }, child: Text('Refer and earn',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),

                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[




                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {

                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => MyOrderPage(title: "My Orders",)));



                                                         }, child: Text('My Orders',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),

                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[




                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {

                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => WishlistPage(title: "Wishlist",)));



                                                         }, child: Text('Wishlist',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),
                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[



                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {

                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => WithdrawalPage(title: "Withdrawal request",)));



                                                         }, child: Text('Withdrawal requests',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),
                                         Container(child: Padding(
                                           padding: const EdgeInsets.fromLTRB(10, 2, 2, 2),

                                           //padding: EdgeInsets.symmetric(horizontal: 15),
                                           child: Row(
                                             textDirection: TextDirection.ltr,
                                             children: <Widget>[



                                               Expanded(child: new Theme(data: new ThemeData(
                                                   hintColor: Colors.white
                                               ), child: Align(
                                                   alignment: Alignment.centerLeft,



                                                   child:Padding(


                                                       padding: EdgeInsets.fromLTRB(10, 0, 0, 0),
                                                       child: TextButton(


                                                         onPressed: () {

                                                           Navigator.push(
                                                               context, MaterialPageRoute(builder: (_) => SettingsPage(title: "Settings",)));



                                                         }, child: Text('Settings',style: TextStyle(color: Colors.black,fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),),
                                                       )))))
                                             ],
                                           ),
                                         )),




                                       ],


                                     )),
                                 margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                                 decoration: BoxDecoration(
                                   color: Colors.white,
                                   borderRadius: BorderRadius.circular(10),
                                 ),
                               ),
                             );
                           },
                           transitionBuilder: (context, anim1, anim2, child) {
                             return SlideTransition(
                               position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                               child: child,
                             );
                           },
                         );




                       },
                       child:    Padding(padding: EdgeInsets.all(2),child:Container(


                           child:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?Icon(Icons.menu,size: 30,):Icon(Icons.menu,size: 40,):Icon(Icons.menu,size: 60,)


                       )))),
                 flex: 1,

               )

               ,

               Expanded(child: Padding (padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(5, 15, 0, 0):EdgeInsets.fromLTRB( 10, 15, 0, 0):EdgeInsets.fromLTRB(20, 13, 0, 0),

                 child:  Container(
                     alignment: Alignment.bottomCenter,

                     child:   Image.asset("images/letterkolica.png",width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?90:130:130,height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?31:44: 44,fit: BoxFit.fill,),

                   )


               ) ,flex: 2),



               Expanded(child: Container(


                   margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(6, 25, 0, 0):EdgeInsets.fromLTRB(0, 25, 0, 0):EdgeInsets.fromLTRB(0, 25, 0, 0),

                   alignment: Alignment.centerRight,
                   child:

                   new InkWell(
                       onTap: () {


                         Navigator.push(
                             context, MaterialPageRoute(builder: (_) => NotificationPage(title: "Notification",)));



                       }
                       ,

                       child:Padding(padding: EdgeInsets.all(5),child:Container(

                           child:  Icon(Icons.notifications,size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:28:28,))))),
                 flex:1,),


               Expanded(child: Stack(

                   children:   [

                     (cartcount>0)?    Align(

                         alignment: Alignment.topRight,

                         child: Padding(
                             padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(5,15,5,5):EdgeInsets.fromLTRB(10,25,10,10),
                             child:Container(
                               width: ResponsiveInfo.isMobile(context)?20:40,
                               height: ResponsiveInfo.isMobile(context)?20:40,

                               decoration: BoxDecoration(
                                   color: Color(0xfff55245),
                                   shape: BoxShape.circle
                               ),

                               child: Center(

                                 child:

                                 (cartcount>0)? Text(cartcount.toString()+"",style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 12,color: Colors.white):TextStyle(fontSize: 19,color: Colors.white)):Container(),



                               ),
                             ))

                     ):Container(),
                     Container(
                     margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(6, 22, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0),
                     alignment: Alignment.centerRight,
                     child:  new InkWell(
                         onTap: () {

                           Navigator.push(
                               context, MaterialPageRoute(builder: (_) => CartPage(title: "Cart",)));



                         }
                         ,

                         child:Padding(padding: EdgeInsets.all(2),child:Container(child:
                         Icon(Icons.shopping_cart,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:28: 28)))))
              ] )  ,
                 flex:1,),

               Expanded(child:   Container(
            margin: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(6, 25, 0, 0):EdgeInsets.fromLTRB(0, 25, 0, 0):EdgeInsets.fromLTRB(0, 25, 0, 0),
                   alignment: Alignment.centerRight,
                   child:  new InkWell(
                       onTap: () {


                         Navigator.push(
                             context, MaterialPageRoute(builder: (_) => WalletPage(title: "profile",)));

                       }
                       ,

                       child:Padding(padding: EdgeInsets.all(2),child:Container(child:
                       Icon(Icons.account_balance_wallet_outlined,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:28: 28))))),
                 flex:1,),


             ]),




       ),





     ),


     body: Stack(


         children: <Widget>[

         Container(
         width: width,
         height:double.infinity,

         color: Color(0xffEFECEC),

         ),

    SingleChildScrollView(
    child: Column(

      children: [

        Container(
          width: width,
          height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:130 ,

          color: Color(0xffEFECEC),

          child: Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,10,10,10):EdgeInsets.fromLTRB(35,15,15,15):EdgeInsets.fromLTRB(50,20,20,20),

            child:Card(

              elevation: 8,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 0),
                borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
              ),

              child:  Container(




                child:Container(



                  child: GestureDetector(

                    onTap: (){

                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => SearchPage(title: "Cart",)));


                    },


                child:  Row(
                    textDirection: TextDirection.ltr,
                    children: <Widget>[

                      Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                          child:

                          ResponsiveInfo.isMobile(context)? Image.asset("images/search.png",width: 25,height: 25,

                          ):Image.asset("images/search.png",width: 45,height: 45,

                          )
                      )
                      ,
                      Expanded(child: new Theme(data: new ThemeData(
                          hintColor: Colors.black45
                      ), child:Text("Search ...",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?14:19 ),


                      )))
                    ],
                  ))



                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  ,




                )



                ,),


            ) ,



          )


          ,



        ),

        Container(
          width: width,
          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 80:120:160,

          color: Color(0xfff6f6f6),
          child:

          Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,0,0,0):EdgeInsets.fromLTRB(35,0,0,0):EdgeInsets.fromLTRB(50,0,0,0),

            child:new ListView(
              scrollDirection: Axis.horizontal,

              children: List.generate(
                data.length,
                    (i) => GestureDetector(
                      child:Padding(padding:  EdgeInsets.all(5),
                          child:Column(
                            children: [
                              Container(child:Image.network(UrlData.categorybaseurl+data[i].icon,
                                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,

                                errorBuilder:
                                    (BuildContext context, Object exception, StackTrace? stackTrace) {

                                  return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,
                                  );
                                },


                              )),
                              Text(data[i].catname,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54)     :TextStyle(fontSize: 22,color: Colors.black),)
                            ],
                          )
                      ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (_) => ProductlistPage(title: "Product", categoryid: data[i].id,)));
                      },
                    ),
              ),
            ) ,


          )

          ,



        ),

        (ishistoryavailable)?  Align(


          alignment: Alignment.centerLeft,

          child:      Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,10,0,10):EdgeInsets.fromLTRB(35,15,0,15):EdgeInsets.fromLTRB(50,20,0,15),

              child:  Text("Keep Shopping",style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 15,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),)



          ),

        ) : Container(),




        (ishistoryavailable)?  Container(

          width: width,
          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 240:290:350,

          color: Color(0xfff6f6f6),

          child:

          Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,10,0,0):EdgeInsets.fromLTRB(35,18,0,0):EdgeInsets.fromLTRB(50,30,0,0),


            child:  new ListView(
            scrollDirection: Axis.horizontal,
                children: List.generate(
                  (prhistory.length>4)?4: prhistory.length,
                      (i) => GestureDetector(
                    child:  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(15),

                        child:Column(

                          mainAxisAlignment: MainAxisAlignment.center,

                          children: [

                            Container(

                      //   width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120:140:180,
                      // height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120:140:180,

                                child:

                            Card(
                              elevation: 4,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10),
                              ),



                              child:Container(
                                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 180:220:270 ,


                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      child:  ClipOval(

                                        child: Image.network(UrlData.productimageurl+prhistory[i].image1,
                                          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:120,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:120,
                                          errorBuilder:
                                              (BuildContext context, Object exception, StackTrace? stackTrace) {
                                            return

                                              ClipOval(
                                            child:  Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120,
                                            ) );
                                          },
                                        ))











                                    ),

                                    SizedBox(


                                      child: Padding(
                                        padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(3):EdgeInsets.all(8),

                                        child: Text(prhistory[i].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),) ,
                                      width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?100:130:180 ,
                                    )
                                    ,
                                    Row(

                                      children: [

                                        Padding(padding: EdgeInsets.all(5),

                                          child: Text("₹ "+prhistory[i].mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),

                                          )
                                          ,)
                                        ,
                                        Padding(padding: EdgeInsets.all(5),

                                          child: Text("₹ "+prhistory[i].salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                          )
                                          ,)
                                      ],


                                    )


                                  ],
                                ),


                              )






                              ,



                            )


                            ),




                          ],

                        )

                    ),
                    onTap: (){




                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: prhistory[i].productid, stockid: prhistory[i].stockid,)));




                    },
                  ),
                ),




          ))


        ): Container(),





        Container(



          color: Color(0xfff6f6f6),
          child:Container(
              height: (MediaQuery.of(context).size.width) / 1.89349112,
              width: double.infinity,
              child: PageIndicatorContainer(
                  length: sliderurl.length,
                  align: IndicatorAlign.bottom,
                  indicatorSpace: 14.0,
                  padding: const EdgeInsets.all(10),
                  indicatorColor: Colors.black12,
                  indicatorSelectorColor: Colors.blueGrey,
                  shape: IndicatorShape.circle(size: 10),
                  child: PageView.builder(
                    scrollDirection: Axis.horizontal,
                    // controller: controller,
                    itemBuilder: (BuildContext context, int index) {
                      return getNetWorkWidget(index);
                    },
                    itemCount: sliderurl.length,
                    // children: nsdwidget,
                  )))






        ),



        (offerdata.length>0) ?  Container(
          width: width,

          color: Color(0xffffffff),
          child:Card(

            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,

            child:  GestureDetector(

              onTap: (){

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: offerdata[selectedindex].productdata.id, stockid: offerdata[selectedindex].offerdata.id,)));



              },

              child:Padding(
                  padding: EdgeInsets.all(10),
                  child:Column(
                    children: [
                      Image.network(UrlData.productimageurl+offerdata[selectedindex].productdata.image1,
                        width: width,height: width/2.3,
                        errorBuilder:
                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                          return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,
                          );
                        },
                      ),
                      Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),
                        child: Text(offerdata[selectedindex].productdata.productName,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:18:22 )),
                      ),
                      Padding(padding: EdgeInsets.all(5),
                          child:Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Padding(padding: EdgeInsets.all(5),
                                child: Text("₹ "+offerdata[selectedindex].offerdata.mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),
                                )
                                ,)
                              ,
                              Padding(padding: EdgeInsets.all(5),
                                child: Text("₹ "+offerdata[selectedindex].offerdata.salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),
                                )
                                ,)
                            ],
                          )
                      ),


                      (double.parse(offerdata[selectedindex].offerdata.cashbackPercent)>0)? Container(
                        width:ResponsiveInfo.isMobile(context)?120:160,
                        height:ResponsiveInfo.isMobile(context)? 50:80,
                        color: Color(0xffFCE8E7),
                        child: Center(
                            child:Text("₹ "+offerdata[selectedindex].offerdata.cashbackPercent+" % Cashback",maxLines:1,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 12:20,color: Colors.black),)
                        ),
                      ):Container()
                    ],
                  )
              )




            ),



          )





        ):Container(),


        (offerdata.length>1)?Container(
          color: Color(0xffffffff),
          child:Card(


            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            child: Padding(

                padding: EdgeInsets.all(10),
                child:ListView(
                  scrollDirection: Axis.horizontal,
                  children: List.generate(
                    offerdata.length,
                        (i) => GestureDetector(
                      child:Padding(padding:  EdgeInsets.all(5),
                          child:Column(
                            children: [
                              Container(child:ClipOval(child:

                              Image.network(UrlData.productimageurl+offerdata[i].productdata.image1,
                                width:ResponsiveInfo.isMobile(context)?70:90 ,height: ResponsiveInfo.isMobile(context)?70:90,
                                errorBuilder:
                                    (BuildContext context, Object exception, StackTrace? stackTrace) {

                                  return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?70:90 ,height: ResponsiveInfo.isMobile(context)?70:90,
                                  );
                                },
                              ))),

                            ],
                          )
                      ),
                      onTap: (){

                        setState(() {

                          selectedindex=i;
                        });



                                             },
                    ),
                  ),
                )




            ),


          ),

          width: width,
          height: ResponsiveInfo.isMobile(context)?110:130,




        ):Container(),




        Container(
            color: Color(0xffffffff),
          child:ListView.builder(
            physics: BouncingScrollPhysics(),

            shrinkWrap: true,

            itemCount: adv_data.length,
            itemBuilder: (context, index) {
              return  Card(
             elevation: ResponsiveInfo.isMobile(context)?5:10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                child:GestureDetector(

                  child:  Container(




                      child:Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                          child: Column(

                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                child:
                                Text(adv_data[index].title,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 15,color: Colors.black,)     :TextStyle(fontSize: 23,color: Colors.black),
                                ),),
                              Center(child:Image.network(UrlData.advertiseurl+adv_data[index].photo,
                                width:width ,height: width/1.89349112,fit: BoxFit.fill,
                                errorBuilder:
                                    (BuildContext context, Object exception, StackTrace? stackTrace) {

                                  return Image.asset("images/pi.png",width:width ,height: width/1.89349112,fit: BoxFit.fill,
                                  );
                                },
                              ))

                            ],


                          )






                      )


                  ),

                      onTap:(){

                    if(adv_data[index].type.compareTo(UrlData.producttype)==0)
                      {

                        Navigator.push(
                            context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: adv_data[index].parameter, stockid: adv_data[index].stockid,)));


                      }

                  else  if(adv_data[index].type.compareTo(UrlData.categorytype)==0)
                    {

                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => ProductlistPage(title: "Product", categoryid: adv_data[index].parameter,)));


                    }
                    else  if(adv_data[index].type.compareTo(UrlData.pricewise)==0)
                    {

                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => ProductlistPage(title: "Product", categoryid: adv_data[index].parameter,)));


                    }
                    else  if(adv_data[index].type.compareTo(UrlData.cashback)==0)
                    {

                      Navigator.push(
                          context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: adv_data[index].parameter, stockid: adv_data[index].stockid,)));


                    }


                      }


                )






              )



                ;


            },
          )


        ),



        Container(
            color: Color(0xffffffff),
            child:ListView.builder(
              physics: BouncingScrollPhysics(),

              shrinkWrap: true,

              itemCount: data.length,
              itemBuilder: (context, index) {
                return GestureDetector(

                  child: Card(
                      elevation: ResponsiveInfo.isMobile(context)?5:10,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child:Container(




                          child:Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                              child: Column(

                                mainAxisAlignment: MainAxisAlignment.start,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                    child:
                                    Text(data[index].catname,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black,)     :TextStyle(fontSize: 18,color: Colors.black),
                                    ),),
                                  Center(child:Image.network(UrlData.categorybaseurl+data[index].bannerImg,
                                    width:width ,height: width/1.89349112,fit: BoxFit.fill,
                                    errorBuilder:
                                        (BuildContext context, Object exception, StackTrace? stackTrace) {

                                      return Image.asset("images/pi.png",width:width ,height: width/1.89349112,fit: BoxFit.fill,
                                      );
                                    },
                                  ))

                                ],


                              )






                          )


                      )


                  ),
                  onTap:(){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => ProductlistPage(title: "Product", categoryid: data[index].id,)));


                  }
                )






                ;


              },
            )


        ),








      ],






    )

    )
    ]),




   );
  }

  Widget getNetWorkWidget(int p) {
    return nsdwidget[p];
  }

  getCartCount() async {
    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getCartCount),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token: datastorage.getString(DataConstants.tokenkey)!,
        'Timestamp':date

      },



    );


    //  _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);



    if(jsondata['status']==1)
    {


String cc=jsondata['cartcount'];



setState(() {

  cartcount=int.parse(cc);
});





    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }





  }

  getCategories() async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getCategories),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(response);

    MainCategory mcategory=MainCategory.fromJson(jsondata);

    if(mcategory.status==1)
      {

        if(mcategory.categorydata.length>0)
          {

         setState(() {
           data.addAll(mcategory.categorydata);
         });


          }
        else{
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("No category data found"),
          ));

        }



      }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }



  }



  getSliders()async{

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getSlider),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(response);

    Banners bnrs=Banners.fromJson(jsondata);

    if(bnrs.status==1)
      {

List<Sliderdata>sr=[];

for(Sliderdata sldr in bnrs.sliderdata)
  {

    sr.add(sldr);

  }

setState(() {

  sliderurl.addAll(sr);
});


      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));

    }



  }


  getOfferedProduct() async{

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getOfferProducts),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(response);

    OfferProduct offerProduct=OfferProduct.fromJson(jsondata);

    if(offerProduct.status==1)
      {

        if(offerProduct.data.length>0)
          {


            setState(() {

              offerdata.addAll(offerProduct.data);


            });


          }
        else{

          // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          //   content: Text("No offerdata"),
          // ));
        }
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }


  }

  getHistory() async{

    List<prw.Data>h=[];
    h.clear();

    List<Map<String, dynamic>> a =
    await new DatabaseHelper().queryAllRows(DatabaseTables.History);

    if(a.length>0)
      {
        


        for (Map ab in a) {
          print(ab.length.toString() + " \n");
          print("data \n");


          int id = ab["keyid"];
          String data = ab["data"];



          var jsondata = jsonDecode(data);

          String d1=jsondata['product'];

          var js = jsonDecode(d1);

          prw.ProductDetailsWeb p=prw.ProductDetailsWeb.fromJson(js);
          h.add(p.data);
        }

       // h.reversed();


        setState(() {

          prhistory.addAll(h.reversed.toList());

          ishistoryavailable=true;
        });


      }
    else{

      setState(() {

        ishistoryavailable=false;
      });

    }



  }


  getAdvertisement() async{

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getAdvertisements),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(response);

    Advertisement ad_data=Advertisement.fromJson(jsondata);

    if(ad_data.status==1)
    {

      if(ad_data.advertisedata.length>0)
      {


        setState(() {

          adv_data.addAll(ad_data.advertisedata);


        });


      }
      else{

        // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        //   content: Text("No offerdata"),
        // ));
      }
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }


  }



}
