/// status : 1
/// message : "Success"
/// infodata : {"id":"36","userid":"59","addressid":"7","order_type":"online","order_date":"2022-05-23 06:05:57","item_count":"2","expected_deli_date":"2022-05-24","total_price":"492.00","delevery_charge":"50.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-05-23 06:06:57","sale_status":"0","deliveryboy_id":"0","payment_methode":"2","isPaymentUPi":"0","transactionid":null}

class Orderinfo {
  Orderinfo({
      required int status,
    required  String message,
    required  Infodata infodata,}){
    _status = status;
    _message = message;
    _infodata = infodata;
}

  Orderinfo.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _infodata = (json['infodata'] != null ? Infodata.fromJson(json['infodata']) : null)!;
  }
  int _status=0;
  String _message="";
 late Infodata _infodata;

  int get status => _status;
  String get message => _message;
  Infodata get infodata => _infodata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_infodata != null) {
      map['infodata'] = _infodata.toJson();
    }
    return map;
  }

}

/// id : "36"
/// userid : "59"
/// addressid : "7"
/// order_type : "online"
/// order_date : "2022-05-23 06:05:57"
/// item_count : "2"
/// expected_deli_date : "2022-05-24"
/// total_price : "492.00"
/// delevery_charge : "50.00"
/// discount : "0.00"
/// discounted_total_price : "0.00"
/// cancel_status : "0"
/// delivery_completion_status : "0"
/// comments : ""
/// created_at : null
/// updated_at : "2022-05-23 06:06:57"
/// sale_status : "0"
/// deliveryboy_id : "0"
/// payment_methode : "2"
/// isPaymentUPi : "0"
/// transactionid : null

class Infodata {
  Infodata({
      required String id,
    required  String userid,
    required  String addressid,
    required  String orderType,
    required  String orderDate,
    required String itemCount,
    required  String expectedDeliDate,
    required  String totalPrice,
    required  String deleveryCharge,
    required  String discount,
    required  String discountedTotalPrice,
    required  String cancelStatus,
    required  String deliveryCompletionStatus,
    required  String comments,
      dynamic createdAt,
    required  String updatedAt,
    required  String saleStatus,
    required  String deliveryboyId,
    required  String paymentMethode,
    required  String isPaymentUPi,
      dynamic transactionid,}){
    _id = id;
    _userid = userid;
    _addressid = addressid;
    _orderType = orderType;
    _orderDate = orderDate;
    _itemCount = itemCount;
    _expectedDeliDate = expectedDeliDate;
    _totalPrice = totalPrice;
    _deleveryCharge = deleveryCharge;
    _discount = discount;
    _discountedTotalPrice = discountedTotalPrice;
    _cancelStatus = cancelStatus;
    _deliveryCompletionStatus = deliveryCompletionStatus;
    _comments = comments;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _saleStatus = saleStatus;
    _deliveryboyId = deliveryboyId;
    _paymentMethode = paymentMethode;
    _isPaymentUPi = isPaymentUPi;
    _transactionid = transactionid;
}

  Infodata.fromJson(dynamic json) {
    _id = json['id'];
    _userid = json['userid'];
    _addressid = json['addressid'];
    _orderType = json['order_type'];
    _orderDate = json['order_date'];
    _itemCount = json['item_count'];
    _expectedDeliDate = json['expected_deli_date'];
    _totalPrice = json['total_price'];
    _deleveryCharge = json['delevery_charge'];
    _discount = json['discount'];
    _discountedTotalPrice = json['discounted_total_price'];
    _cancelStatus = json['cancel_status'];
    _deliveryCompletionStatus = json['delivery_completion_status'];
    _comments = json['comments'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _saleStatus = json['sale_status'];
    _deliveryboyId = json['deliveryboy_id'];
    _paymentMethode = json['payment_methode'];
    _isPaymentUPi = json['isPaymentUPi'];
    _transactionid = json['transactionid'];
  }
  String _id="";
  String _userid="";
  String _addressid="";
  String _orderType="";
  String _orderDate="";
  String _itemCount="";
  String _expectedDeliDate="";
  String _totalPrice="";
  String _deleveryCharge="";
  String _discount="";
  String _discountedTotalPrice="";
  String _cancelStatus="";
  String _deliveryCompletionStatus="";
  String _comments="";
  dynamic _createdAt="";
  String _updatedAt="";
  String _saleStatus="";
  String _deliveryboyId="";
  String _paymentMethode="";
  String _isPaymentUPi="";
  dynamic _transactionid="";

  String get id => _id;
  String get userid => _userid;
  String get addressid => _addressid;
  String get orderType => _orderType;
  String get orderDate => _orderDate;
  String get itemCount => _itemCount;
  String get expectedDeliDate => _expectedDeliDate;
  String get totalPrice => _totalPrice;
  String get deleveryCharge => _deleveryCharge;
  String get discount => _discount;
  String get discountedTotalPrice => _discountedTotalPrice;
  String get cancelStatus => _cancelStatus;
  String get deliveryCompletionStatus => _deliveryCompletionStatus;
  String get comments => _comments;
  dynamic get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get saleStatus => _saleStatus;
  String get deliveryboyId => _deliveryboyId;
  String get paymentMethode => _paymentMethode;
  String get isPaymentUPi => _isPaymentUPi;
  dynamic get transactionid => _transactionid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['userid'] = _userid;
    map['addressid'] = _addressid;
    map['order_type'] = _orderType;
    map['order_date'] = _orderDate;
    map['item_count'] = _itemCount;
    map['expected_deli_date'] = _expectedDeliDate;
    map['total_price'] = _totalPrice;
    map['delevery_charge'] = _deleveryCharge;
    map['discount'] = _discount;
    map['discounted_total_price'] = _discountedTotalPrice;
    map['cancel_status'] = _cancelStatus;
    map['delivery_completion_status'] = _deliveryCompletionStatus;
    map['comments'] = _comments;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['sale_status'] = _saleStatus;
    map['deliveryboy_id'] = _deliveryboyId;
    map['payment_methode'] = _paymentMethode;
    map['isPaymentUPi'] = _isPaymentUPi;
    map['transactionid'] = _transactionid;
    return map;
  }

}