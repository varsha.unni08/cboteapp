import 'Product.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Offer.g.dart';


@JsonSerializable(explicitToJson: true)
class Offer{

 int status=0;
String message="";
 String id="";
 String productid="";
 String offerpercent="";
String productprice="";
 String actualprice="";
 late Product product;

 Offer();

 factory Offer.fromJson(Map<String,dynamic>data) => _$OfferFromJson(data);


 Map<String,dynamic> toJson() => _$OfferToJson(this);



}