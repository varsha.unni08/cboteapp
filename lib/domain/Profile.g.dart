// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Profile.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileData _$ProfileDataFromJson(Map<String, dynamic> json) {
  return ProfileData()
    ..id = json['id'] as String
    ..customer_name = json['customer_name'] as String
    ..image = json['image'] as String
    ..phone1 = json['phone1'] as String
    ..email = json['email'] as String
    ..password = json['password'] as String
    ..district = json['district'] as String
    ..createdAt = json['createdAt'] as String
    ..updatedAt = json['updatedAt'] as String;
}

Map<String, dynamic> _$ProfileDataToJson(ProfileData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'customer_name': instance.customer_name,
      'image': instance.image,
      'phone1': instance.phone1,
      'email': instance.email,
      'password': instance.password,
      'district': instance.district,
      'createdAt': instance.createdAt,
      'updatedAt': instance.updatedAt,
    };
