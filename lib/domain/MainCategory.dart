/// status : 1
/// message : "Success"
/// categorydata : [{"id":"1","catname":"Powder","banner_img":"powderbanner.jpg","icon":"powdericon.png","description":"A fine mixture of particles"},{"id":"2","catname":"Oil","banner_img":"oilbanner.jpg","icon":"oilicon.png","description":"A liquid form of substances"},{"id":"3","catname":"Dress","banner_img":"cloths.jpg","icon":"dress1.png","description":"A form of materials"},{"id":"4","catname":"Electronics","banner_img":"eclectronicsbanner.png","icon":"electronicsicon.png","description":"Wired and wireless"}]

class MainCategory {
  MainCategory({
      int status=0,
      String message="",
      required List<Categorydata> categorydata,}){
    _status = status;
    _message = message;
    _categorydata = categorydata;
}

  MainCategory.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['categorydata'] != null) {
      _categorydata = [];
      json['categorydata'].forEach((v) {
        _categorydata.add(Categorydata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Categorydata> _categorydata=[];

  int get status => _status;
  String get message => _message;
  List<Categorydata> get categorydata => _categorydata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_categorydata != null) {
      map['categorydata'] = _categorydata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// catname : "Powder"
/// banner_img : "powderbanner.jpg"
/// icon : "powdericon.png"
/// description : "A fine mixture of particles"

class Categorydata {
  Categorydata({
      required String id,
      required String catname,
      required String bannerImg,
      required String icon,
      required String description,}){
    _id = id;
    _catname = catname;
    _bannerImg = bannerImg;
    _icon = icon;
    _description = description;
}

  Categorydata.fromJson(dynamic json) {
    _id = json['id'];
    _catname = json['catname'];
    _bannerImg = json['banner_img'];
    _icon = json['icon'];
    _description = json['description'];
  }
  String _id="";
  String _catname="";
  String _bannerImg="";
  String _icon="";
  String _description="";

  String get id => _id;
  String get catname => _catname;
  String get bannerImg => _bannerImg;
  String get icon => _icon;
  String get description => _description;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['catname'] = _catname;
    map['banner_img'] = _bannerImg;
    map['icon'] = _icon;
    map['description'] = _description;
    return map;
  }

}