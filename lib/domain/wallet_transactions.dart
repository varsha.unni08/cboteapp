/// status : 1
/// message : "Success"
/// data : [{"id":"10","amount":"101.0","date":"2022-02-18 06:01:38 AM","debit":"0","credit":"1","userid":"28","current_amount":"101.0","description":"amount added to wallet","transactionid":"2022-02-18T11:31:36.789157","status":"1"},{"id":"11","amount":"350","date":"2022-02-18 06:02:57 AM","debit":"0","credit":"1","userid":"28","current_amount":"451","description":"amount added to wallet","transactionid":"124698755","status":"1"}]

class WalletTransactions {
  int _status=0;
  String _message="";
  List<Data> _data=[];

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  WalletTransactions({
      required int status,
      required String message,
      required List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  WalletTransactions.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "10"
/// amount : "101.0"
/// date : "2022-02-18 06:01:38 AM"
/// debit : "0"
/// credit : "1"
/// userid : "28"
/// current_amount : "101.0"
/// description : "amount added to wallet"
/// transactionid : "2022-02-18T11:31:36.789157"
/// status : "1"

class Data {
  String _id="";
  String _amount="";
  String _date="";
  String _debit="";
  String _credit="";
  String _userid="";
  String _currentAmount="";
  String _description="";
  String _transactionid="";
  String _status="";

  String get id => _id;
  String get amount => _amount;
  String get date => _date;
  String get debit => _debit;
  String get credit => _credit;
  String get userid => _userid;
  String get currentAmount => _currentAmount;
  String get description => _description;
  String get transactionid => _transactionid;
  String get status => _status;

  Data({
      required String id,
      required String amount,
      required String date,
      required String debit,
      required String credit,
      required String userid,
      required String currentAmount,
      required String description,
      required String transactionid,
      required String status}){
    _id = id;
    _amount = amount;
    _date = date;
    _debit = debit;
    _credit = credit;
    _userid = userid;
    _currentAmount = currentAmount;
    _description = description;
    _transactionid = transactionid;
    _status = status;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _amount = json['amount'];
    _date = json['date'];
    _debit = json['debit'];
    _credit = json['credit'];
    _userid = json['userid'];
    _currentAmount = json['current_amount'];
    _description = json['description'];
    _transactionid = json['transactionid'];
    _status = json['status'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['amount'] = _amount;
    map['date'] = _date;
    map['debit'] = _debit;
    map['credit'] = _credit;
    map['userid'] = _userid;
    map['current_amount'] = _currentAmount;
    map['description'] = _description;
    map['transactionid'] = _transactionid;
    map['status'] = _status;
    return map;
  }

}