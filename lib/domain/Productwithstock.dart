import 'package:json_annotation/json_annotation.dart';

part 'Productwithstock.g.dart';


@JsonSerializable(explicitToJson: true)
class ProductwithStock{

 String stockid="";
 String productid="";
String qtystock="";
 String price="";
 String discountedprice="";
 String discount="";
 String productname="";
 String subCatid="";
 String subcategoryid="";
 String subcatname="";
 String categoryid="";
 String catname="";
 String productimage="";

   ProductwithStock();


 factory ProductwithStock.fromJson(Map<String,dynamic>data) => _$ProductwithStockFromJson(data);


 Map<String,dynamic> toJson() => _$ProductwithStockToJson(this);
}