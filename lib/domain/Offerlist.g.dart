// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Offerlist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Offerlist _$OfferlistFromJson(Map<String, dynamic> json) {
  return Offerlist()
    ..status = json['status'] as int
    ..message = json['message'] as String
    ..data = (json['data'] as List<dynamic>)
        .map((e) => Offer.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$OfferlistToJson(Offerlist instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
