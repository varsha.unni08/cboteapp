/// status : 1
/// message : "Success"
/// sliderdata : [{"id":"1","image":"oil.png"},{"id":"2","image":"corona.png"},{"id":"3","image":"sambarr.png"}]

class Banners {
  Banners({
      required int status,
      required String message,
      required List<Sliderdata> sliderdata,}){
    _status = status;
    _message = message;
    _sliderdata = sliderdata;
}

  Banners.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['sliderdata'] != null) {
      _sliderdata = [];
      json['sliderdata'].forEach((v) {
        _sliderdata.add(Sliderdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Sliderdata> _sliderdata=[];

  int get status => _status;
  String get message => _message;
  List<Sliderdata> get sliderdata => _sliderdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_sliderdata != null) {
      map['sliderdata'] = _sliderdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// image : "oil.png"

class Sliderdata {
  Sliderdata({
      required String id,
      required String image,}){
    _id = id;
    _image = image;
}

  Sliderdata.fromJson(dynamic json) {
    _id = json['id'];
    _image = json['image'];
  }
  String _id="";
  String _image="";

  String get id => _id;
  String get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['image'] = _image;
    return map;
  }

}