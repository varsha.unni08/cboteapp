// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Offer.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Offer _$OfferFromJson(Map<String, dynamic> json) {
  return Offer()
    ..status = json['status'] as int
    ..message = json['message'] as String
    ..id = json['id'] as String
    ..productid = json['productid'] as String
    ..offerpercent = json['offerpercent'] as String
    ..productprice = json['productprice'] as String
    ..actualprice = json['actualprice'] as String
    ..product = Product.fromJson(json['product'] as Map<String, dynamic>);
}

Map<String, dynamic> _$OfferToJson(Offer instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'id': instance.id,
      'productid': instance.productid,
      'offerpercent': instance.offerpercent,
      'productprice': instance.productprice,
      'actualprice': instance.actualprice,
      'product': instance.product.toJson(),
    };
