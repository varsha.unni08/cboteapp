import 'package:flutter_app_ecommerce/domain/Offer.dart';

import 'package:json_annotation/json_annotation.dart';

part 'Offerlist.g.dart';


@JsonSerializable(explicitToJson: true)
class Offerlist{


 int status=0;
 String message="";
 List<Offer> data = [];

 Offerlist();

 factory Offerlist.fromJson(Map<String,dynamic>data) => _$OfferlistFromJson(data);


 Map<String,dynamic> toJson() => _$OfferlistToJson(this);
}