/// status : 1
/// message : "Success"
/// data : [{"productid":"1","Stockid":"1","id":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","price":"175.00","discount":"25.00","discounted_price":"150.00","isOffer":"1","offer_percent":"14","isCashBack":"0","cashback_amount":"","cgst":"4.00","sgst":"4.00","igst":"0.00","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null,"purchase_without_gst":"0.00","purchase_with_tax":"0.00","MRP":"200.00","discount_percent":"12.00","cashback_percent":"0.00","salespri_without_gst":"0.00","salespri_with_gst":"140.00","reff_income_percent":"0.00","reff_gen_status":"0","cashback_gen_status":"0","sub_catid":"1","product_name":"Melam Sambar pwdr 1kg","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate","isreturnable":"0","make":null,"height":null,"width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"melamsambar.jpg","image2":"10images12.jpg","image3":"49images10.jpg","image4":"95download4.jpg","image5":"119images5.jpg","Cartid":"9","stockid":"1","quantity":"4","userid":"28"}]

class CartProduct {
  CartProduct({
     required int status,
    required String message,
    required List<Data> data,}){
    _status = status;
    _message = message;
    _data = data;
}

  CartProduct.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Data> _data=[];

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// productid : "1"
/// Stockid : "1"
/// id : "1"
/// batchno : "AAA"
/// shop_id : "1"
/// warehouse_id : "1"
/// distributor_id : "1"
/// make_date : "2022-02-20"
/// expe_date : "2022-04-30"
/// qty_stock : "10"
/// qty_damage : "0"
/// qty_rol : "25"
/// price : "175.00"
/// discount : "25.00"
/// discounted_price : "150.00"
/// isOffer : "1"
/// offer_percent : "14"
/// isCashBack : "0"
/// cashback_amount : ""
/// cgst : "4.00"
/// sgst : "4.00"
/// igst : "0.00"
/// total_with_gst : "0.00"
/// stock_status : "0"
/// iswide : "0"
/// stock_update_date : "2021-05-19"
/// commition_agent_id : null
/// added_user_id : "1"
/// barcode_no : "sdgsdgdsfhdfh"
/// created_at : null
/// updated_at : null
/// purchase_without_gst : "0.00"
/// purchase_with_tax : "0.00"
/// MRP : "200.00"
/// discount_percent : "12.00"
/// cashback_percent : "0.00"
/// salespri_without_gst : "0.00"
/// salespri_with_gst : "140.00"
/// reff_income_percent : "0.00"
/// reff_gen_status : "0"
/// cashback_gen_status : "0"
/// sub_catid : "1"
/// product_name : "Melam Sambar pwdr 1kg"
/// description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate"
/// isreturnable : "0"
/// make : null
/// height : null
/// width : null
/// weight : null
/// size : null
/// thickness : null
/// color : null
/// sizedifference : null
/// image1 : "melamsambar.jpg"
/// image2 : "10images12.jpg"
/// image3 : "49images10.jpg"
/// image4 : "95download4.jpg"
/// image5 : "119images5.jpg"
/// Cartid : "9"
/// stockid : "1"
/// quantity : "4"
/// userid : "28"

class Data {
  Data({
      required  String productid,
      required String stockid,
      required  String id,
      required String batchno,
      required String shopId,
      required String warehouseId,
      required String distributorId,
      required String makeDate,
      required String expeDate,
      required String qtyStock,
      required String qtyDamage,
      required String qtyRol,
      required String price,
      required String discount,
      required String discountedPrice,
      required String isOffer,
      required String offerPercent,
      required String isCashBack,
      required String cashbackAmount,
      required String cgst,
      required String sgst,
      required String igst,
      required String totalWithGst,
      required String stockStatus,
      required String iswide,
      required String stockUpdateDate,
      dynamic commitionAgentId, 
      required String addedUserId,
      required String barcodeNo,
      dynamic createdAt, 
      dynamic updatedAt, 
      required String purchaseWithoutGst,
      required String purchaseWithTax,
      required String mrp,
      required String discountPercent,
      required String cashbackPercent,
      required String salespriWithoutGst,
      required String salespriWithGst,
      required String reffIncomePercent,
      required String reffGenStatus,
      required String cashbackGenStatus,
      required String subCatid,
      required String productName,
      required String description,
      required String isreturnable,
      dynamic make, 
      dynamic height, 
      dynamic width, 
      dynamic weight, 
      dynamic size, 
      dynamic thickness, 
      dynamic color, 
      dynamic sizedifference, 
      required String image1,
      required String image2,
      required String image3,
      required String image4,
      required String image5,
      required String cartid,
      required String stockids,
      required String quantity,
      required String userid,}){
    _productid = productid;
    _stockid = stockid;
    _id = id;
    _batchno = batchno;
    _shopId = shopId;
    _warehouseId = warehouseId;
    _distributorId = distributorId;
    _makeDate = makeDate;
    _expeDate = expeDate;
    _qtyStock = qtyStock;
    _qtyDamage = qtyDamage;
    _qtyRol = qtyRol;
    _price = price;
    _discount = discount;
    _discountedPrice = discountedPrice;
    _isOffer = isOffer;
    _offerPercent = offerPercent;
    _isCashBack = isCashBack;
    _cashbackAmount = cashbackAmount;
    _cgst = cgst;
    _sgst = sgst;
    _igst = igst;
    _totalWithGst = totalWithGst;
    _stockStatus = stockStatus;
    _iswide = iswide;
    _stockUpdateDate = stockUpdateDate;
    _commitionAgentId = commitionAgentId;
    _addedUserId = addedUserId;
    _barcodeNo = barcodeNo;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _purchaseWithoutGst = purchaseWithoutGst;
    _purchaseWithTax = purchaseWithTax;
    _mrp = mrp;
    _discountPercent = discountPercent;
    _cashbackPercent = cashbackPercent;
    _salespriWithoutGst = salespriWithoutGst;
    _salespriWithGst = salespriWithGst;
    _reffIncomePercent = reffIncomePercent;
    _reffGenStatus = reffGenStatus;
    _cashbackGenStatus = cashbackGenStatus;
    _subCatid = subCatid;
    _productName = productName;
    _description = description;
    _isreturnable = isreturnable;
    _make = make;
    _height = height;
    _width = width;
    _weight = weight;
    _size = size;
    _thickness = thickness;
    _color = color;
    _sizedifference = sizedifference;
    _image1 = image1;
    _image2 = image2;
    _image3 = image3;
    _image4 = image4;
    _image5 = image5;
    _cartid = cartid;
    _stockid = stockid;
    _quantity = quantity;
    _userid = userid;
}



  Data.fromJson(dynamic json) {
    _productid = json['productid'];
    _stockid = json['Stockid'];
    _id = json['id'];
    _batchno = json['batchno'];
    _shopId = json['shop_id'];
    _warehouseId = json['warehouse_id'];
    _distributorId = json['distributor_id'];
    _makeDate = json['make_date'];
    _expeDate = json['expe_date'];
    _qtyStock = json['qty_stock'];
    _qtyDamage = json['qty_damage'];
    _qtyRol = json['qty_rol'];
    _price = json['price'];
    _discount = json['discount'];
    _discountedPrice = json['discounted_price'];
    _isOffer = json['isOffer'];
    _offerPercent = json['offer_percent'];
    _isCashBack = json['isCashBack'];
    _cashbackAmount = json['cashback_amount'];
    _cgst = json['cgst'];
    _sgst = json['sgst'];
    _igst = json['igst'];
    _totalWithGst = json['total_with_gst'];
    _stockStatus = json['stock_status'];
    _iswide = json['iswide'];
    _stockUpdateDate = json['stock_update_date'];
    _commitionAgentId = json['commition_agent_id'];
    _addedUserId = json['added_user_id'];
    _barcodeNo = json['barcode_no'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _purchaseWithoutGst = json['purchase_without_gst'];
    _purchaseWithTax = json['purchase_with_tax'];
    _mrp = json['MRP'];
    _discountPercent = json['discount_percent'];
    _cashbackPercent = json['cashback_percent'];
    _salespriWithoutGst = json['salespri_without_gst'];
    _salespriWithGst = json['salespri_with_gst'];
    _reffIncomePercent = json['reff_income_percent'];
    _reffGenStatus = json['reff_gen_status'];
    _cashbackGenStatus = json['cashback_gen_status'];
    _subCatid = json['sub_catid'];
    _productName = json['product_name'];
    _description = json['description'];
    _isreturnable = json['isreturnable'];
    _make = json['make'];
    _height = json['height'];
    _width = json['width'];
    _weight = json['weight'];
    _size = json['size'];
    _thickness = json['thickness'];
    _color = json['color'];
    _sizedifference = json['sizedifference'];
    _image1 = json['image1'];
    _image2 = json['image2'];
    _image3 = json['image3'];
    _image4 = json['image4'];
    _image5 = json['image5'];
    _cartid = json['Cartid'];
    _stockid = json['stockid'];
    _quantity = json['quantity'];
    _userid = json['userid'];
  }
  String _productid="";
  String _stockid="";
  String _id="";
  String _batchno="";
  String _shopId="";
  String _warehouseId="";
  String _distributorId="";
  String _makeDate="";
  String _expeDate="";
  String _qtyStock="";
  String _qtyDamage="";
  String _qtyRol="";
  String _price="";
  String _discount="";
  String _discountedPrice="";
  String _isOffer="";
  String _offerPercent="";
  String _isCashBack="";
  String _cashbackAmount="";
  String _cgst="";
  String _sgst="";
  String _igst="";
  String _totalWithGst="";
  String _stockStatus="";
  String _iswide="";
  String _stockUpdateDate="";
  dynamic _commitionAgentId="";
  String _addedUserId="";
  String _barcodeNo="";
  dynamic _createdAt="";
  dynamic _updatedAt="";
  String _purchaseWithoutGst="";
  String _purchaseWithTax="";
  String _mrp="";
  String _discountPercent="";
  String _cashbackPercent="";
  String _salespriWithoutGst="";
  String _salespriWithGst="";
  String _reffIncomePercent="";
  String _reffGenStatus="";
  String _cashbackGenStatus="";
  String _subCatid="";
  String _productName="";
  String _description="";
  String _isreturnable="";
  dynamic _make="";
  dynamic _height="";
  dynamic _width="";
  dynamic _weight="";
  dynamic _size="";
  dynamic _thickness="";
  dynamic _color="";
  dynamic _sizedifference="";
  String _image1="";
  String _image2="";
  String _image3="";
  String _image4="";
  String _image5="";
  String _cartid="";
  String _stockids="";
  String _quantity="";
  String _userid="";

  String get productid => _productid;
  String get stockid => _stockid;
  String get id => _id;
  String get batchno => _batchno;
  String get shopId => _shopId;
  String get warehouseId => _warehouseId;
  String get distributorId => _distributorId;
  String get makeDate => _makeDate;
  String get expeDate => _expeDate;
  String get qtyStock => _qtyStock;
  String get qtyDamage => _qtyDamage;
  String get qtyRol => _qtyRol;
  String get price => _price;
  String get discount => _discount;
  String get discountedPrice => _discountedPrice;
  String get isOffer => _isOffer;
  String get offerPercent => _offerPercent;
  String get isCashBack => _isCashBack;
  String get cashbackAmount => _cashbackAmount;
  String get cgst => _cgst;
  String get sgst => _sgst;
  String get igst => _igst;
  String get totalWithGst => _totalWithGst;
  String get stockStatus => _stockStatus;
  String get iswide => _iswide;
  String get stockUpdateDate => _stockUpdateDate;
  dynamic get commitionAgentId => _commitionAgentId;
  String get addedUserId => _addedUserId;
  String get barcodeNo => _barcodeNo;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  String get purchaseWithoutGst => _purchaseWithoutGst;
  String get purchaseWithTax => _purchaseWithTax;
  String get mrp => _mrp;
  String get discountPercent => _discountPercent;
  String get cashbackPercent => _cashbackPercent;
  String get salespriWithoutGst => _salespriWithoutGst;
  String get salespriWithGst => _salespriWithGst;
  String get reffIncomePercent => _reffIncomePercent;
  String get reffGenStatus => _reffGenStatus;
  String get cashbackGenStatus => _cashbackGenStatus;
  String get subCatid => _subCatid;
  String get productName => _productName;
  String get description => _description;
  String get isreturnable => _isreturnable;
  dynamic get make => _make;
  dynamic get height => _height;
  dynamic get width => _width;
  dynamic get weight => _weight;
  dynamic get size => _size;
  dynamic get thickness => _thickness;
  dynamic get color => _color;
  dynamic get sizedifference => _sizedifference;
  String get image1 => _image1;
  String get image2 => _image2;
  String get image3 => _image3;
  String get image4 => _image4;
  String get image5 => _image5;
  String get cartid => _cartid;
  String get stockids => _stockid;
  String get quantity => _quantity;
  String get userid => _userid;

  set setquantity(String q) {
    _quantity = q;
  }



  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['productid'] = _productid;
    map['Stockid'] = _stockid;
    map['id'] = _id;
    map['batchno'] = _batchno;
    map['shop_id'] = _shopId;
    map['warehouse_id'] = _warehouseId;
    map['distributor_id'] = _distributorId;
    map['make_date'] = _makeDate;
    map['expe_date'] = _expeDate;
    map['qty_stock'] = _qtyStock;
    map['qty_damage'] = _qtyDamage;
    map['qty_rol'] = _qtyRol;
    map['price'] = _price;
    map['discount'] = _discount;
    map['discounted_price'] = _discountedPrice;
    map['isOffer'] = _isOffer;
    map['offer_percent'] = _offerPercent;
    map['isCashBack'] = _isCashBack;
    map['cashback_amount'] = _cashbackAmount;
    map['cgst'] = _cgst;
    map['sgst'] = _sgst;
    map['igst'] = _igst;
    map['total_with_gst'] = _totalWithGst;
    map['stock_status'] = _stockStatus;
    map['iswide'] = _iswide;
    map['stock_update_date'] = _stockUpdateDate;
    map['commition_agent_id'] = _commitionAgentId;
    map['added_user_id'] = _addedUserId;
    map['barcode_no'] = _barcodeNo;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['purchase_without_gst'] = _purchaseWithoutGst;
    map['purchase_with_tax'] = _purchaseWithTax;
    map['MRP'] = _mrp;
    map['discount_percent'] = _discountPercent;
    map['cashback_percent'] = _cashbackPercent;
    map['salespri_without_gst'] = _salespriWithoutGst;
    map['salespri_with_gst'] = _salespriWithGst;
    map['reff_income_percent'] = _reffIncomePercent;
    map['reff_gen_status'] = _reffGenStatus;
    map['cashback_gen_status'] = _cashbackGenStatus;
    map['sub_catid'] = _subCatid;
    map['product_name'] = _productName;
    map['description'] = _description;
    map['isreturnable'] = _isreturnable;
    map['make'] = _make;
    map['height'] = _height;
    map['width'] = _width;
    map['weight'] = _weight;
    map['size'] = _size;
    map['thickness'] = _thickness;
    map['color'] = _color;
    map['sizedifference'] = _sizedifference;
    map['image1'] = _image1;
    map['image2'] = _image2;
    map['image3'] = _image3;
    map['image4'] = _image4;
    map['image5'] = _image5;
    map['Cartid'] = _cartid;
    map['stockid'] = _stockid;
    map['quantity'] = _quantity;
    map['userid'] = _userid;
    return map;
  }



}