/// status : 1
/// message : "Success"
/// ratingdata : [{"id":"1","stockid":"1","userid":"28","rating":"3","date":"2022-02-24\n09:01:58","description":"3.5","customer_name":"antony"}]

class ProductRating {
  ProductRating({
      required int status,
      required String message,
      required List<Ratingdata> ratingdata,}){
    _status = status;
    _message = message;
    _ratingdata = ratingdata;
}

  ProductRating.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['ratingdata'] != null) {
      _ratingdata = [];
      json['ratingdata'].forEach((v) {
        _ratingdata.add(Ratingdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Ratingdata> _ratingdata=[];

  int get status => _status;
  String get message => _message;
  List<Ratingdata> get ratingdata => _ratingdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_ratingdata != null) {
      map['ratingdata'] = _ratingdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// stockid : "1"
/// userid : "28"
/// rating : "3"
/// date : "2022-02-24\n09:01:58"
/// description : "3.5"
/// customer_name : "antony"

class Ratingdata {
  Ratingdata({
      required String id,
      required String stockid,
      required String userid,
      required String rating,
      required String date,
      required String description,
      required String customerName,
  required String image}){
    _id = id;
    _stockid = stockid;
    _userid = userid;
    _rating = rating;
    _date = date;
    _description = description;
    _customerName = customerName;
    _image=image;
}

  Ratingdata.fromJson(dynamic json) {
    _id = json['id'];
    _stockid = json['stockid'];
    _userid = json['userid'];
    _rating = json['rating'];
    _date = json['date'];
    _description = json['description'];
    _customerName = json['customer_name'];
    _image = json['image'];
  }
  String _id="";
  String _stockid="";
  String _userid="";
  String _rating="";
  String _date="";
  String _description="";
  String _customerName="";
  String _image="";

  String get id => _id;
  String get stockid => _stockid;
  String get userid => _userid;
  String get rating => _rating;
  String get date => _date;
  String get description => _description;
  String get customerName => _customerName;
  String get image => _image;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['stockid'] = _stockid;
    map['userid'] = _userid;
    map['rating'] = _rating;
    map['date'] = _date;
    map['description'] = _description;
    map['customer_name'] = _customerName;
    map['image'] = _image;
    return map;
  }

}