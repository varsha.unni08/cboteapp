import 'package:json_annotation/json_annotation.dart';

part 'Product.g.dart';


@JsonSerializable(explicitToJson: true)
class Product{

 String id="";
 String subCatid="";
 String productName="";
 String description="";
 String image="";

 Product();


 factory Product.fromJson(Map<String,dynamic>data) => _$ProductFromJson(data);


 Map<String,dynamic> toJson() => _$ProductToJson(this);
}