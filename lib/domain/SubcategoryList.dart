/// status : 1
/// message : "Success"
/// subcategorydata : [{"id":"1","categoryid":"1","sub_catname":"sambar POWDER Melam","description":"curry making powders","created_at":"2021-05-18 20:20:51","updated_at":"2021-05-18 20:24:07"},{"id":"2","categoryid":"1","sub_catname":"Puttu Podi","description":"puttu making powder","created_at":"2021-05-18 20:23:11","updated_at":"2021-05-18 20:23:11"},{"id":"3","categoryid":"1","sub_catname":"Eastern Sambar Powder","description":"asasf","created_at":"2021-05-18 20:24:30","updated_at":"2021-05-18 20:24:30"},{"id":"4","categoryid":"1","sub_catname":"Bramins Puttu podi","description":"sdf","created_at":"2021-05-18 20:24:46","updated_at":"2021-05-18 20:24:46"}]

class SubcategoryList {
  SubcategoryList({
      required int status,
      required String message,
      required List<Subcategorydata> subcategorydata,}){
    _status = status;
    _message = message;
    _subcategorydata = subcategorydata;
}

  SubcategoryList.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['subcategorydata'] != null) {
      _subcategorydata = [];
      json['subcategorydata'].forEach((v) {
        _subcategorydata.add(Subcategorydata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Subcategorydata> _subcategorydata=[];

  int get status => _status;
  String get message => _message;
  List<Subcategorydata> get subcategorydata => _subcategorydata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_subcategorydata != null) {
      map['subcategorydata'] = _subcategorydata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// categoryid : "1"
/// sub_catname : "sambar POWDER Melam"
/// description : "curry making powders"
/// created_at : "2021-05-18 20:20:51"
/// updated_at : "2021-05-18 20:24:07"

class Subcategorydata {
  Subcategorydata({
      required String id,
      required String categoryid,
      required String subCatname,
      required String description,
      required String createdAt,
      required String updatedAt,}){
    _id = id;
    _categoryid = categoryid;
    _subCatname = subCatname;
    _description = description;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Subcategorydata.fromJson(dynamic json) {
    _id = json['id'];
    _categoryid = json['categoryid'];
    _subCatname = json['sub_catname'];
    _description = json['description'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  String _id="";
  String _categoryid="";
  String _subCatname="";
  String _description="";
  String _createdAt="";
  String _updatedAt="";
  bool isSelected=false;

  String get id => _id;
  String get categoryid => _categoryid;
  String get subCatname => _subCatname;
  String get description => _description;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['categoryid'] = _categoryid;
    map['sub_catname'] = _subCatname;
    map['description'] = _description;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }

}