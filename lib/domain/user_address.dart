/// status : 1
/// message : "Success"
/// address : {"id":"5","user_id":"28","name":"antony kj","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"}

class UserAddress {
  int _status=0;
  String _message="";
  late  Address _address;

  int get status => _status;
  String get message => _message;
  Address get address => _address;

  UserAddress({
      required int status,
      required String message,
      required Address address}){
    _status = status;
    _message = message;
    _address = address;
}

  UserAddress.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _address = (json['address'] != null ? Address.fromJson(json['address']) : null)!;
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_address != null) {
      map['address'] = _address.toJson();
    }
    return map;
  }

}

/// id : "5"
/// user_id : "28"
/// name : "antony kj"
/// houseno : "12B"
/// area : "oomapadi"
/// landmark : "kacheripady"
/// latitude : "10.338325"
/// longitude : "76.2294367"
/// town : "Ernakulam"
/// phone : "9748566325"
/// pincode : "235689"
/// addresstype : "Office"

class Address {
  String _id="";
  String _userId="";
  String _name="";
  String _houseno="";
  String _area="";
  String _landmark="";
  String _latitude="";
  String _longitude="";
  String _town="";
  String _phone="";
  String _pincode="";
  String _addresstype="";

  String get id => _id;
  String get userId => _userId;
  String get name => _name;
  String get houseno => _houseno;
  String get area => _area;
  String get landmark => _landmark;
  String get latitude => _latitude;
  String get longitude => _longitude;
  String get town => _town;
  String get phone => _phone;
  String get pincode => _pincode;
  String get addresstype => _addresstype;

  Address({
      required String id,
      required String userId,
      required String name,
      required String houseno,
      required String area,
      required String landmark,
      required String latitude,
      required String longitude,
      required String town,
      required String phone,
      required String pincode,
      required String addresstype}){
    _id = id;
    _userId = userId;
    _name = name;
    _houseno = houseno;
    _area = area;
    _landmark = landmark;
    _latitude = latitude;
    _longitude = longitude;
    _town = town;
    _phone = phone;
    _pincode = pincode;
    _addresstype = addresstype;
}

  Address.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _name = json['name'];
    _houseno = json['houseno'];
    _area = json['area'];
    _landmark = json['landmark'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _town = json['town'];
    _phone = json['phone'];
    _pincode = json['pincode'];
    _addresstype = json['addresstype'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['name'] = _name;
    map['houseno'] = _houseno;
    map['area'] = _area;
    map['landmark'] = _landmark;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['town'] = _town;
    map['phone'] = _phone;
    map['pincode'] = _pincode;
    map['addresstype'] = _addresstype;
    return map;
  }

}