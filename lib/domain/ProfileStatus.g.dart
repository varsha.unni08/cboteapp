// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProfileStatus.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProfileStatus _$ProfileStatusFromJson(Map<String, dynamic> json) {
  return ProfileStatus()
    ..status = json['status'] as int
    ..data = ProfileData.fromJson(json['data'] as Map<String, dynamic>)
    ..message = json['message'] as String;
}

Map<String, dynamic> _$ProfileStatusToJson(ProfileStatus instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data.toJson(),
      'message': instance.message,
    };
