/// status : 1
/// message : "Success"
/// data : [{"id":"1","platform":"Android","version":"4"}]

class AppUpdate {
  AppUpdate({
      required int status,
      required String message,
      required List<Data> data,}){
    _status = status;
    _message = message;
    _data = data;
}

  AppUpdate.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Data> _data=[];

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// platform : "Android"
/// version : "4"

class Data {
  Data({
      required String id,
      required String platform,
      required String version,}){
    _id = id;
    _platform = platform;
    _version = version;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _platform = json['platform'];
    _version = json['version'];
  }
  String _id="";
  String _platform="";
  String _version="";

  String get id => _id;
  String get platform => _platform;
  String get version => _version;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['platform'] = _platform;
    map['version'] = _version;
    return map;
  }

}