import 'Productwithstock.dart';
import 'package:json_annotation/json_annotation.dart';

part 'ProductWithStocklist.g.dart';


@JsonSerializable(explicitToJson: true)
class ProductWithStocklist{

 int status=0;
 String message="";
 List<ProductwithStock> data = [];

 ProductWithStocklist();

 factory ProductWithStocklist.fromJson(Map<String,dynamic>data) => _$ProductWithStocklistFromJson(data);


 Map<String,dynamic> toJson() => _$ProductWithStocklistToJson(this);
}