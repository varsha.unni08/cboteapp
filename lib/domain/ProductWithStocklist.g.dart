// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'ProductWithStocklist.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductWithStocklist _$ProductWithStocklistFromJson(Map<String, dynamic> json) {
  return ProductWithStocklist()
    ..status = json['status'] as int
    ..message = json['message'] as String
    ..data = (json['data'] as List<dynamic>)
        .map((e) => ProductwithStock.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$ProductWithStocklistToJson(
        ProductWithStocklist instance) =>
    <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
