import 'package:flutter_app_ecommerce/domain/SliderData.dart';
import 'package:json_annotation/json_annotation.dart';

part 'Slider.g.dart';


@JsonSerializable(explicitToJson: true)
class Sliders{



   int status=0;

   String message="";

   List<SliderData> data = [];

   Sliders();

   factory Sliders.fromJson(Map<String,dynamic>data) => _$SlidersFromJson(data);


   Map<String,dynamic> toJson() => _$SlidersToJson(this);
}