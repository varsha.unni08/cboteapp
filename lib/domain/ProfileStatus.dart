import 'package:flutter_app_ecommerce/domain/Profile.dart';


import 'package:json_annotation/json_annotation.dart';

part 'ProfileStatus.g.dart';


@JsonSerializable(explicitToJson: true)
class ProfileStatus{

 int status=0;
 late ProfileData data;
 String message="";

 ProfileStatus();


 factory ProfileStatus.fromJson(Map<String,dynamic>data) => _$ProfileStatusFromJson(data);


 Map<String,dynamic> toJson() => _$ProfileStatusToJson(this);
}