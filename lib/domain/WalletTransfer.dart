/// status : 1
/// message : "Success"
/// data : [{"id":"4","date":"2022-05-28 00:00:00","credit":"13.12","debit":"0.00","balance":"13.12","userid":"63","current_amount":"13.12","description":null,"transactionid":null,"status":"0","type_from":"cashback_sales","type_id":"2","proces_satus":"0"},{"id":"5","date":"2022-05-28 00:00:00","credit":"23.12","debit":"0.00","balance":"36.12","userid":"63","current_amount":"13.12","description":null,"transactionid":null,"status":"0","type_from":"cashback_sales","type_id":"2","proces_satus":"0"}]

class WalletTransfer {
  WalletTransfer({
     required int status,
    required  String message,
    required List<Data> data,}){
    _status = status;
    _message = message;
    _data = data;
}

  WalletTransfer.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Data> _data=[];

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "4"
/// date : "2022-05-28 00:00:00"
/// credit : "13.12"
/// debit : "0.00"
/// balance : "13.12"
/// userid : "63"
/// current_amount : "13.12"
/// description : null
/// transactionid : null
/// status : "0"
/// type_from : "cashback_sales"
/// type_id : "2"
/// proces_satus : "0"

class Data {
  Data({
     required String id,
    required String date,
    required String credit,
    required  String debit,
    required String balance,
    required  String userid,
    required String currentAmount,
    required dynamic description,
    required dynamic transactionid,
    required  String status,
    required  String typeFrom,
    required  String typeId,
    required  String procesSatus,}){
    _id = id;
    _date = date;
    _credit = credit;
    _debit = debit;
    _balance = balance;
    _userid = userid;
    _currentAmount = currentAmount;
    _description = description;
    _transactionid = transactionid;
    _status = status;
    _typeFrom = typeFrom;
    _typeId = typeId;
    _procesSatus = procesSatus;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _date = json['date'];
    _credit = json['credit'];
    _debit = json['debit'];
    _balance = json['balance'];
    _userid = json['userid'];
    _currentAmount = json['current_amount'];
    _description = json['description'];
    _transactionid = json['transactionid'];
    _status = json['status'];
    _typeFrom = json['type_from'];
    _typeId = json['type_id'];
    _procesSatus = json['proces_satus'];
  }
  String _id="";
  String _date="";
  String _credit="";
  String _debit="";
  String _balance="";
  String _userid="";
  String _currentAmount="";
  dynamic _description="";
  dynamic _transactionid="";
  String _status="";
  String _typeFrom="";
  String _typeId="";
  String _procesSatus="";

  String get id => _id;
  String get date => _date;
  String get credit => _credit;
  String get debit => _debit;
  String get balance => _balance;
  String get userid => _userid;
  String get currentAmount => _currentAmount;
  dynamic get description => _description;
  dynamic get transactionid => _transactionid;
  String get status => _status;
  String get typeFrom => _typeFrom;
  String get typeId => _typeId;
  String get procesSatus => _procesSatus;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['date'] = _date;
    map['credit'] = _credit;
    map['debit'] = _debit;
    map['balance'] = _balance;
    map['userid'] = _userid;
    map['current_amount'] = _currentAmount;
    map['description'] = _description;
    map['transactionid'] = _transactionid;
    map['status'] = _status;
    map['type_from'] = _typeFrom;
    map['type_id'] = _typeId;
    map['proces_satus'] = _procesSatus;
    return map;
  }

}