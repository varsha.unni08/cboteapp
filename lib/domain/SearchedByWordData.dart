/// status : 1
/// message : "Success"
/// searchdata : [{"Productid":"1","id":"1","sub_catid":"1","product_name":"Melam Sambar pwdr 1kg","description":"asd","make":null,"height":null,"width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"melamsambar.jpg","image2":"10images12.jpg","image3":"49images10.jpg","image4":"95download4.jpg","image5":"119images5.jpg","Stockid":"1","productid":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","price":"175.00","discount":"25.00","discounted_price":"150.00","isOffer":"1","offer_percent":"14","isCashBack":"0","cashback_amount":"","cgst":"0.00","sgst":"0.00","igst":"0.00","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null},{"Productid":"1","id":"2","sub_catid":"1","product_name":"Melam Sambar pwdr 1kg","description":"asd","make":null,"height":null,"width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"melamsambar.jpg","image2":"10images12.jpg","image3":"49images10.jpg","image4":"95download4.jpg","image5":"119images5.jpg","Stockid":"2","productid":"1","batchno":"BBB","shop_id":"1","warehouse_id":"1","distributor_id":"5","make_date":"2022-02-20","expe_date":"2022-02-28","qty_stock":"10","qty_damage":"0","qty_rol":"10","price":"100.00","discount":"0.00","discounted_price":"100.00","isOffer":"0","offer_percent":"","isCashBack":"1","cashback_amount":"10","cgst":"0.00","sgst":"0.00","igst":"0.00","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"lkl;kl;","created_at":null,"updated_at":null},{"Productid":"1","id":"26","sub_catid":"1","product_name":"Melam Sambar pwdr 1kg","description":"asd","make":null,"height":null,"width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"melamsambar.jpg","image2":"10images12.jpg","image3":"49images10.jpg","image4":"95download4.jpg","image5":"119images5.jpg","Stockid":"26","productid":"1","batchno":null,"shop_id":"1","warehouse_id":null,"distributor_id":"5","make_date":"2022-02-20","expe_date":"2022-02-28","qty_stock":"10","qty_damage":"0","qty_rol":"0","price":"200.00","discount":"0.00","discounted_price":"0.00","isOffer":"0","offer_percent":"","isCashBack":"0","cashback_amount":"","cgst":"0.00","sgst":"0.00","igst":"0.00","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":null,"commition_agent_id":null,"added_user_id":null,"barcode_no":null,"created_at":null,"updated_at":null}]

class SearchedByWordData {
  SearchedByWordData({
      required int status,
      required String message,
      required List<Searchdata> searchdata,}){
    _status = status;
    _message = message;
    _searchdata = searchdata;
}

  SearchedByWordData.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['searchdata'] != null) {
      _searchdata = [];
      json['searchdata'].forEach((v) {
        _searchdata.add(Searchdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Searchdata> _searchdata=[];

  int get status => _status;
  String get message => _message;
  List<Searchdata> get searchdata => _searchdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_searchdata != null) {
      map['searchdata'] = _searchdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// Productid : "1"
/// id : "1"
/// sub_catid : "1"
/// product_name : "Melam Sambar pwdr 1kg"
/// description : "asd"
/// make : null
/// height : null
/// width : null
/// weight : null
/// size : null
/// thickness : null
/// color : null
/// sizedifference : null
/// image1 : "melamsambar.jpg"
/// image2 : "10images12.jpg"
/// image3 : "49images10.jpg"
/// image4 : "95download4.jpg"
/// image5 : "119images5.jpg"
/// Stockid : "1"
/// productid : "1"
/// batchno : "AAA"
/// shop_id : "1"
/// warehouse_id : "1"
/// distributor_id : "1"
/// make_date : "2022-02-20"
/// expe_date : "2022-04-30"
/// qty_stock : "10"
/// qty_damage : "0"
/// qty_rol : "25"
/// price : "175.00"
/// discount : "25.00"
/// discounted_price : "150.00"
/// isOffer : "1"
/// offer_percent : "14"
/// isCashBack : "0"
/// cashback_amount : ""
/// cgst : "0.00"
/// sgst : "0.00"
/// igst : "0.00"
/// total_with_gst : "0.00"
/// stock_status : "0"
/// iswide : "0"
/// stock_update_date : "2021-05-19"
/// commition_agent_id : null
/// added_user_id : "1"
/// barcode_no : "sdgsdgdsfhdfh"
/// created_at : null
/// updated_at : null

class Searchdata {
  Searchdata({
      required productid,
      required String id,
      required String subCatid,
      required String productName,
      required String description,
      dynamic make, 
      dynamic height, 
      dynamic width, 
      dynamic weight, 
      dynamic size, 
      dynamic thickness, 
      dynamic color, 
      dynamic sizedifference, 
      required String image1,
      required String image2,
      required String image3,
      required String image4,
      required String image5,
      required String stockid,
      required String productids,
      required String batchno,
      required String shopId,
      required String warehouseId,
      required String distributorId,
      required String makeDate,
      required String expeDate,
      required String qtyStock,
      required String qtyDamage,
      required String qtyRol,
      required String price,
      required String discount,
      required String discountedPrice,
      required String isOffer,
      required String offerPercent,
      required String isCashBack,
      required String cashbackAmount,
      required String cgst,
      required String sgst,
      required String igst,
      required String totalWithGst,
      required String stockStatus,
      required String iswide,
      required String stockUpdateDate,
      dynamic commitionAgentId, 
      required String addedUserId,
      required String barcodeNo,
      dynamic createdAt, 
      dynamic updatedAt,}){
    _productid = productid;
    _id = id;
    _subCatid = subCatid;
    _productName = productName;
    _description = description;
    _make = make;
    _height = height;
    _width = width;
    _weight = weight;
    _size = size;
    _thickness = thickness;
    _color = color;
    _sizedifference = sizedifference;
    _image1 = image1;
    _image2 = image2;
    _image3 = image3;
    _image4 = image4;
    _image5 = image5;
    _stockid = stockid;
    _productid = productid;
    _batchno = batchno;
    _shopId = shopId;
    _warehouseId = warehouseId;
    _distributorId = distributorId;
    _makeDate = makeDate;
    _expeDate = expeDate;
    _qtyStock = qtyStock;
    _qtyDamage = qtyDamage;
    _qtyRol = qtyRol;
    _price = price;
    _discount = discount;
    _discountedPrice = discountedPrice;
    _isOffer = isOffer;
    _offerPercent = offerPercent;
    _isCashBack = isCashBack;
    _cashbackAmount = cashbackAmount;
    _cgst = cgst;
    _sgst = sgst;
    _igst = igst;
    _totalWithGst = totalWithGst;
    _stockStatus = stockStatus;
    _iswide = iswide;
    _stockUpdateDate = stockUpdateDate;
    _commitionAgentId = commitionAgentId;
    _addedUserId = addedUserId;
    _barcodeNo = barcodeNo;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Searchdata.fromJson(dynamic json) {
    _productid = json['Productid'];
    _id = json['id'];
    _subCatid = json['sub_catid'];
    _productName = json['product_name'];
    _description = json['description'];
    _make = json['make'];
    _height = json['height'];
    _width = json['width'];
    _weight = json['weight'];
    _size = json['size'];
    _thickness = json['thickness'];
    _color = json['color'];
    _sizedifference = json['sizedifference'];
    _image1 = json['image1'];
    _image2 = json['image2'];
    _image3 = json['image3'];
    _image4 = json['image4'];
    _image5 = json['image5'];
    _stockid = json['Stockid'];
    _productid = json['productid'];
    _batchno = json['batchno'];
    _shopId = json['shop_id'];
    _warehouseId = json['warehouse_id'];
    _distributorId = json['distributor_id'];
    _makeDate = json['make_date'];
    _expeDate = json['expe_date'];
    _qtyStock = json['qty_stock'];
    _qtyDamage = json['qty_damage'];
    _qtyRol = json['qty_rol'];
    _price = json['price'];
    _discount = json['discount'];
    _discountedPrice = json['discounted_price'];
    _isOffer = json['isOffer'];
    _offerPercent = json['offer_percent'];
    _isCashBack = json['isCashBack'];
    _cashbackAmount = json['cashback_amount'];
    _cgst = json['cgst'];
    _sgst = json['sgst'];
    _igst = json['igst'];
    _totalWithGst = json['total_with_gst'];
    _stockStatus = json['stock_status'];
    _iswide = json['iswide'];
    _stockUpdateDate = json['stock_update_date'];
    _commitionAgentId = json['commition_agent_id'];
    _addedUserId = json['added_user_id'];
    _barcodeNo = json['barcode_no'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  String _productid="";
  String _id="";
  String _subCatid="";
  String _productName="";
  String _description="";
  dynamic _make="";
  dynamic _height="";
  dynamic _width="";
  dynamic _weight="";
  dynamic _size="";
  dynamic _thickness="";
  dynamic _color="";
  dynamic _sizedifference="";
  String _image1="";
  String _image2="";
  String _image3="";
  String _image4="";
  String _image5="";
  String _stockid="";
  String productid="";
  String _batchno="";
  String _shopId="";
  String _warehouseId="";
  String _distributorId="";
  String _makeDate="";
  String _expeDate="";
  String _qtyStock="";
  String _qtyDamage="";
  String _qtyRol="";
  String _price="";
  String _discount="";
  String _discountedPrice="";
  String _isOffer="";
  String _offerPercent="";
  String _isCashBack="";
  String _cashbackAmount="";
  String _cgst="";
  String _sgst="";
  String _igst="";
  String _totalWithGst="";
  String _stockStatus="";
  String _iswide="";
  String _stockUpdateDate="";
  dynamic _commitionAgentId="";
  String _addedUserId="";
  String _barcodeNo="";
  dynamic _createdAt="";
  dynamic _updatedAt="";

  String get productids => _productid;
  String get id => _id;
  String get subCatid => _subCatid;
  String get productName => _productName;
  String get description => _description;
  dynamic get make => _make;
  dynamic get height => _height;
  dynamic get width => _width;
  dynamic get weight => _weight;
  dynamic get size => _size;
  dynamic get thickness => _thickness;
  dynamic get color => _color;
  dynamic get sizedifference => _sizedifference;
  String get image1 => _image1;
  String get image2 => _image2;
  String get image3 => _image3;
  String get image4 => _image4;
  String get image5 => _image5;
  String get stockid => _stockid;
  String get producti => _productid;
  String get batchno => _batchno;
  String get shopId => _shopId;
  String get warehouseId => _warehouseId;
  String get distributorId => _distributorId;
  String get makeDate => _makeDate;
  String get expeDate => _expeDate;
  String get qtyStock => _qtyStock;
  String get qtyDamage => _qtyDamage;
  String get qtyRol => _qtyRol;
  String get price => _price;
  String get discount => _discount;
  String get discountedPrice => _discountedPrice;
  String get isOffer => _isOffer;
  String get offerPercent => _offerPercent;
  String get isCashBack => _isCashBack;
  String get cashbackAmount => _cashbackAmount;
  String get cgst => _cgst;
  String get sgst => _sgst;
  String get igst => _igst;
  String get totalWithGst => _totalWithGst;
  String get stockStatus => _stockStatus;
  String get iswide => _iswide;
  String get stockUpdateDate => _stockUpdateDate;
  dynamic get commitionAgentId => _commitionAgentId;
  String get addedUserId => _addedUserId;
  String get barcodeNo => _barcodeNo;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Productid'] = _productid;
    map['id'] = _id;
    map['sub_catid'] = _subCatid;
    map['product_name'] = _productName;
    map['description'] = _description;
    map['make'] = _make;
    map['height'] = _height;
    map['width'] = _width;
    map['weight'] = _weight;
    map['size'] = _size;
    map['thickness'] = _thickness;
    map['color'] = _color;
    map['sizedifference'] = _sizedifference;
    map['image1'] = _image1;
    map['image2'] = _image2;
    map['image3'] = _image3;
    map['image4'] = _image4;
    map['image5'] = _image5;
    map['Stockid'] = _stockid;
    map['productid'] = _productid;
    map['batchno'] = _batchno;
    map['shop_id'] = _shopId;
    map['warehouse_id'] = _warehouseId;
    map['distributor_id'] = _distributorId;
    map['make_date'] = _makeDate;
    map['expe_date'] = _expeDate;
    map['qty_stock'] = _qtyStock;
    map['qty_damage'] = _qtyDamage;
    map['qty_rol'] = _qtyRol;
    map['price'] = _price;
    map['discount'] = _discount;
    map['discounted_price'] = _discountedPrice;
    map['isOffer'] = _isOffer;
    map['offer_percent'] = _offerPercent;
    map['isCashBack'] = _isCashBack;
    map['cashback_amount'] = _cashbackAmount;
    map['cgst'] = _cgst;
    map['sgst'] = _sgst;
    map['igst'] = _igst;
    map['total_with_gst'] = _totalWithGst;
    map['stock_status'] = _stockStatus;
    map['iswide'] = _iswide;
    map['stock_update_date'] = _stockUpdateDate;
    map['commition_agent_id'] = _commitionAgentId;
    map['added_user_id'] = _addedUserId;
    map['barcode_no'] = _barcodeNo;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }

}