/// status : 1
/// message : "Success."
/// data : {"id":"1","pincode_data":"680567","place":"valapad","status":"1"}

class Pincode {
  Pincode({
     required int status,
     required String message,
     required Data data,}){
    _status = status;
    _message = message;
    _data = data;
}

  Pincode.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = (json['data'] != null ? Data.fromJson(json['data']) : null)!;
  }
  int _status=0;
  String _message="";
 late Data _data;

  int get status => _status;
  String get message => _message;
  Data get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.toJson();
    }
    return map;
  }

}

/// id : "1"
/// pincode_data : "680567"
/// place : "valapad"
/// status : "1"

class Data {
  Data({
      required String id,
      required String pincodeData,
      required String place,
      required String status,}){
    _id = id;
    _pincodeData = pincodeData;
    _place = place;
    _status = status;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _pincodeData = json['pincode_data'];
    _place = json['place'];
    _status = json['status'];
  }
  String _id="";
  String _pincodeData="";
  String _place="";
  String _status="";

  String get id => _id;
  String get pincodeData => _pincodeData;
  String get place => _place;
  String get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['pincode_data'] = _pincodeData;
    map['place'] = _place;
    map['status'] = _status;
    return map;
  }

}