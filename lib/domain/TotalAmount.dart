/// status : 1
/// message : "Success"
/// totalamount : "1100"
/// deliverycharge : "0"

class TotalAmount {
  TotalAmount({
     required int status,
    required String message,
    required  String totalamount,
    required String deliverycharge,
  required String free_deliveryamount}){
    _status = status;
    _message = message;
    _totalamount = totalamount;
    _deliverycharge = deliverycharge;
    _free_deliveryamount=free_deliveryamount;
}

  TotalAmount.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _totalamount = json['totalamount'];
    _deliverycharge = json['deliverycharge'];
    _free_deliveryamount=json['free_deliveryamount'];
  }
  int _status=0;
  String _message="";
  String _totalamount="";
  String _deliverycharge="";
  String _free_deliveryamount="";

  int get status => _status;
  String get message => _message;
  String get totalamount => _totalamount;
  String get deliverycharge => _deliverycharge;
  String get free_deliveryamount => _free_deliveryamount;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    map['totalamount'] = _totalamount;
    map['deliverycharge'] = _deliverycharge;
    map['free_deliveryamount'] = _free_deliveryamount;
    return map;
  }

}