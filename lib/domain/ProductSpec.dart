/// status : 1
/// message : "Success"
/// specdata : [{"id":"2","productid":"1","spec_head":"ASAA","spec_value":"998.00","created_at":"2021-08-02 18:29:00","updated_at":"2021-08-02 18:29:00"}]

class ProductSpec {
  ProductSpec({
      required int status,
      required String message,
      required List<Specdata> specdata,}){
    _status = status;
    _message = message;
    _specdata = specdata;
}

  ProductSpec.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['specdata'] != null) {
      _specdata = [];
      json['specdata'].forEach((v) {
        _specdata.add(Specdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Specdata> _specdata=[];

  int get status => _status;
  String get message => _message;
  List<Specdata> get specdata => _specdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_specdata != null) {
      map['specdata'] = _specdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "2"
/// productid : "1"
/// spec_head : "ASAA"
/// spec_value : "998.00"
/// created_at : "2021-08-02 18:29:00"
/// updated_at : "2021-08-02 18:29:00"

class Specdata {
  Specdata({
      required String id,
      required String productid,
      required String specHead,
      required String specValue,
      required String createdAt,
      required String updatedAt,}){
    _id = id;
    _productid = productid;
    _specHead = specHead;
    _specValue = specValue;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
}

  Specdata.fromJson(dynamic json) {
    _id = json['id'];
    _productid = json['productid'];
    _specHead = json['spec_head'];
    _specValue = json['spec_value'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
  }
  String _id="";
  String _productid="";
  String _specHead="";
  String _specValue="";
  String _createdAt="";
  String _updatedAt="";

  String get id => _id;
  String get productid => _productid;
  String get specHead => _specHead;
  String get specValue => _specValue;
  String get createdAt => _createdAt;
  String get updatedAt => _updatedAt;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['productid'] = _productid;
    map['spec_head'] = _specHead;
    map['spec_value'] = _specValue;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    return map;
  }

}