// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'SliderData.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

SliderData _$SliderDataFromJson(Map<String, dynamic> json) {
  return SliderData()
    ..id = json['id'] as String
    ..image = json['image'] as String;
}

Map<String, dynamic> _$SliderDataToJson(SliderData instance) =>
    <String, dynamic>{
      'id': instance.id,
      'image': instance.image,
    };
