/// status : 1
/// message : "Success"
/// data : [{"productid":"1","Stockid":"1","qty_stock":"10","price":"175.00","discount":"25.00","discounted_price":"150.00","isOffer":"1","offer_percent":"14","isCashBack":"0","make_date":"2022-02-20","expe_date":"2022-04-30","id":"1","sub_catid":"1","product_name":"Melam Sambar pwdr 1kg","description":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate","isreturnable":"0","make":null,"height":null,"width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"melamsambar.jpg","image2":"10images12.jpg","image3":"49images10.jpg","image4":"95download4.jpg","image5":"119images5.jpg","Wishlistid":"7","stockid":"1","userid":"28"}]

class WishlistProduct {
  WishlistProduct({
     required int status,
    required String message,
    required  List<Data> data,}){
    _status = status;
    _message = message;
    _data = data;
}

  WishlistProduct.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
late   List<Data> _data;

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// productid : "1"
/// Stockid : "1"
/// qty_stock : "10"
/// price : "175.00"
/// discount : "25.00"
/// discounted_price : "150.00"
/// isOffer : "1"
/// offer_percent : "14"
/// isCashBack : "0"
/// make_date : "2022-02-20"
/// expe_date : "2022-04-30"
/// id : "1"
/// sub_catid : "1"
/// product_name : "Melam Sambar pwdr 1kg"
/// description : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate"
/// isreturnable : "0"
/// make : null
/// height : null
/// width : null
/// weight : null
/// size : null
/// thickness : null
/// color : null
/// sizedifference : null
/// image1 : "melamsambar.jpg"
/// image2 : "10images12.jpg"
/// image3 : "49images10.jpg"
/// image4 : "95download4.jpg"
/// image5 : "119images5.jpg"
/// Wishlistid : "7"
/// stockid : "1"
/// userid : "28"

class Data {
  Data({
      required String productid,
      required String stockid,
      required String qtyStock,
      required String price,
      required String discount,
      required String discountedPrice,
      required String isOffer,
      required String offerPercent,
      required String isCashBack,
      required String makeDate,
      required String expeDate,
      required String id,
      required String subCatid,
      required String productName,
      required String description,
      required String isreturnable,
      dynamic make, 
      dynamic height, 
      dynamic width, 
      dynamic weight, 
      dynamic size, 
      dynamic thickness, 
      dynamic color, 
      dynamic sizedifference, 
      required String image1,
      required String image2,
      required String image3,
      required String image4,
      required String image5,
      required String wishlistid,
      required String stockids,
      required String userid,}){
    _productid = productid;
    _stockid = stockid;
    _qtyStock = qtyStock;
    _price = price;
    _discount = discount;
    _discountedPrice = discountedPrice;
    _isOffer = isOffer;
    _offerPercent = offerPercent;
    _isCashBack = isCashBack;
    _makeDate = makeDate;
    _expeDate = expeDate;
    _id = id;
    _subCatid = subCatid;
    _productName = productName;
    _description = description;
    _isreturnable = isreturnable;
    _make = make;
    _height = height;
    _width = width;
    _weight = weight;
    _size = size;
    _thickness = thickness;
    _color = color;
    _sizedifference = sizedifference;
    _image1 = image1;
    _image2 = image2;
    _image3 = image3;
    _image4 = image4;
    _image5 = image5;
    _wishlistid = wishlistid;
    _stockid = stockid;
    _userid = userid;
}

  Data.fromJson(dynamic json) {
    _productid = json['productid'];
    _stockid = json['Stockid'];
    _qtyStock = json['qty_stock'];
    _price = json['price'];
    _discount = json['discount'];
    _discountedPrice = json['discounted_price'];
    _isOffer = json['isOffer'];
    _offerPercent = json['offer_percent'];
    _isCashBack = json['isCashBack'];
    _makeDate = json['make_date'];
    _expeDate = json['expe_date'];
    _id = json['id'];
    _subCatid = json['sub_catid'];
    _productName = json['product_name'];
    _description = json['description'];
    _isreturnable = json['isreturnable'];
    _make = json['make'];
    _height = json['height'];
    _width = json['width'];
    _weight = json['weight'];
    _size = json['size'];
    _thickness = json['thickness'];
    _color = json['color'];
    _sizedifference = json['sizedifference'];
    _image1 = json['image1'];
    _image2 = json['image2'];
    _image3 = json['image3'];
    _image4 = json['image4'];
    _image5 = json['image5'];
    _wishlistid = json['Wishlistid'];
    _stockid = json['stockid'];
    _userid = json['userid'];
  }
  String _productid="";
  String _stockid="";
  String _qtyStock="";
  String _price="";
  String _discount="";
  String _discountedPrice="";
  String _isOffer="";
  String _offerPercent="";
  String _isCashBack="";
  String _makeDate="";
  String _expeDate="";
  String _id="";
  String _subCatid="";
  String _productName="";
  String _description="";
  String _isreturnable="";
  dynamic _make="";
  dynamic _height="";
  dynamic _width="";
  dynamic _weight="";
  dynamic _size="";
  dynamic _thickness="";
  dynamic _color="";
  dynamic _sizedifference="";
  String _image1="";
  String _image2="";
  String _image3="";
  String _image4="";
  String _image5="";
  String _wishlistid="";
  String _stockids="";
  String _userid="";

  String get productid => _productid;
  String get stockid => _stockid;
  String get qtyStock => _qtyStock;
  String get price => _price;
  String get discount => _discount;
  String get discountedPrice => _discountedPrice;
  String get isOffer => _isOffer;
  String get offerPercent => _offerPercent;
  String get isCashBack => _isCashBack;
  String get makeDate => _makeDate;
  String get expeDate => _expeDate;
  String get id => _id;
  String get subCatid => _subCatid;
  String get productName => _productName;
  String get description => _description;
  String get isreturnable => _isreturnable;
  dynamic get make => _make;
  dynamic get height => _height;
  dynamic get width => _width;
  dynamic get weight => _weight;
  dynamic get size => _size;
  dynamic get thickness => _thickness;
  dynamic get color => _color;
  dynamic get sizedifference => _sizedifference;
  String get image1 => _image1;
  String get image2 => _image2;
  String get image3 => _image3;
  String get image4 => _image4;
  String get image5 => _image5;
  String get wishlistid => _wishlistid;
  String get stockids => _stockid;
  String get userid => _userid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['productid'] = _productid;
    map['Stockid'] = _stockid;
    map['qty_stock'] = _qtyStock;
    map['price'] = _price;
    map['discount'] = _discount;
    map['discounted_price'] = _discountedPrice;
    map['isOffer'] = _isOffer;
    map['offer_percent'] = _offerPercent;
    map['isCashBack'] = _isCashBack;
    map['make_date'] = _makeDate;
    map['expe_date'] = _expeDate;
    map['id'] = _id;
    map['sub_catid'] = _subCatid;
    map['product_name'] = _productName;
    map['description'] = _description;
    map['isreturnable'] = _isreturnable;
    map['make'] = _make;
    map['height'] = _height;
    map['width'] = _width;
    map['weight'] = _weight;
    map['size'] = _size;
    map['thickness'] = _thickness;
    map['color'] = _color;
    map['sizedifference'] = _sizedifference;
    map['image1'] = _image1;
    map['image2'] = _image2;
    map['image3'] = _image3;
    map['image4'] = _image4;
    map['image5'] = _image5;
    map['Wishlistid'] = _wishlistid;
    map['stockid'] = _stockid;
    map['userid'] = _userid;
    return map;
  }

}