/// status : 1
/// message : "Success"
/// notification_data : [{"id":"1","title":"de Finibus Bonorum et Malorum","message":"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.","userid":"28","read_status":"0"},{"id":"2","title":"The standard Lorem Ipsum","message":"Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo","userid":"0","read_status":"1"}]

class NotificationServer {
  Notification({
      required int status,
      required String message,
      required List<NotificationData> notificationData,}){
    _status = status;
    _message = message;
    _notificationData = notificationData;
}

  NotificationServer.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['notification_data'] != null) {
      _notificationData = [];
      json['notification_data'].forEach((v) {
        _notificationData.add(NotificationData.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<NotificationData> _notificationData=[];

  int get status => _status;
  String get message => _message;
  List<NotificationData> get notificationData => _notificationData;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_notificationData != null) {
      map['notification_data'] = _notificationData.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// title : "de Finibus Bonorum et Malorum"
/// message : "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
/// userid : "28"
/// read_status : "0"

class NotificationData {
  NotificationData({
      required String id,
      required String title,
      required String message,
      required String userid,
      required String readStatus,}){
    _id = id;
    _title = title;
    _message = message;
    _userid = userid;
    _readStatus = readStatus;
}

  NotificationData.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _message = json['message'];
    _userid = json['userid'];
    _readStatus = json['read_status'];
  }
  String _id="";
  String _title="";
  String _message="";
  String _userid="";
  String _readStatus="";

  String get id => _id;
  String get title => _title;
  String get message => _message;
  String get userid => _userid;
  String get readStatus => _readStatus;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['message'] = _message;
    map['userid'] = _userid;
    map['read_status'] = _readStatus;
    return map;
  }

}