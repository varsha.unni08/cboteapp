/// status : 1
/// message : "Success."
/// data : [{"id":"3","user_id":"34","name":"kumaran ","houseno":"13A","area":"ayyappa nagar","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"ernakulam","phone":"9789456780","pincode":"123456","addresstype":"Home"}]

class AddressData {
  int _status=0;
  String _message="";
 late List<Data> _data;

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  AddressData({
      required int status,
      required String message,
      required List<Data> data}){
    _status = status;
    _message = message;
    _data = data;
}

  AddressData.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "3"
/// user_id : "34"
/// name : "kumaran "
/// houseno : "13A"
/// area : "ayyappa nagar"
/// landmark : "kacheripady"
/// latitude : "10.338325"
/// longitude : "76.2294367"
/// town : "ernakulam"
/// phone : "9789456780"
/// pincode : "123456"
/// addresstype : "Home"

class Data {
  String _id="";
  String _userId="";
  String _name="";
  String _houseno="";
  String _area="";
  String _landmark="";
  String _latitude="";
  String _longitude="";
  String _town="";
  String _phone="";
  String _pincode="";
  String _addresstype="";
  bool selected=false;

  String get id => _id;
  String get userId => _userId;
  String get name => _name;
  String get houseno => _houseno;
  String get area => _area;
  String get landmark => _landmark;
  String get latitude => _latitude;
  String get longitude => _longitude;
  String get town => _town;
  String get phone => _phone;
  String get pincode => _pincode;
  String get addresstype => _addresstype;

  Data({
      required String id,
      required String userId,
      required String name,
      required String houseno,
      required String area,
      required String landmark,
      required String latitude,
      required String longitude,
      required String town,
      required String phone,
      required String pincode,
      required String addresstype}){
    _id = id;
    _userId = userId;
    _name = name;
    _houseno = houseno;
    _area = area;
    _landmark = landmark;
    _latitude = latitude;
    _longitude = longitude;
    _town = town;
    _phone = phone;
    _pincode = pincode;
    _addresstype = addresstype;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _userId = json['user_id'];
    _name = json['name'];
    _houseno = json['houseno'];
    _area = json['area'];
    _landmark = json['landmark'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _town = json['town'];
    _phone = json['phone'];
    _pincode = json['pincode'];
    _addresstype = json['addresstype'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['id'] = _id;
    map['user_id'] = _userId;
    map['name'] = _name;
    map['houseno'] = _houseno;
    map['area'] = _area;
    map['landmark'] = _landmark;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['town'] = _town;
    map['phone'] = _phone;
    map['pincode'] = _pincode;
    map['addresstype'] = _addresstype;
    return map;
  }

}