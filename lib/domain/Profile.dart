

import 'package:json_annotation/json_annotation.dart';

part 'Profile.g.dart';


@JsonSerializable(explicitToJson: true)
class ProfileData{

  String id="";
  String customer_name="";
 String image="";
 String phone1="";
 String email="";
 String password="";
 String district="";
 String createdAt="";
 String updatedAt="";

  ProfileData();



  factory ProfileData.fromJson(Map<String,dynamic>data) => _$ProfileDataFromJson(data);


  Map<String,dynamic> toJson() => _$ProfileDataToJson(this);


}