// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Productwithstock.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ProductwithStock _$ProductwithStockFromJson(Map<String, dynamic> json) {
  return ProductwithStock()
    ..stockid = json['stockid'] as String
    ..productid = json['productid'] as String
    ..qtystock = json['qtystock'] as String
    ..price = json['price'] as String
    ..discountedprice = json['discountedprice'] as String
    ..discount = json['discount'] as String
    ..productname = json['productname'] as String
    ..subCatid = json['subCatid'] as String
    ..subcategoryid = json['subcategoryid'] as String
    ..subcatname = json['subcatname'] as String
    ..categoryid = json['categoryid'] as String
    ..catname = json['catname'] as String
    ..productimage = json['productimage'] as String;
}

Map<String, dynamic> _$ProductwithStockToJson(ProductwithStock instance) =>
    <String, dynamic>{
      'stockid': instance.stockid,
      'productid': instance.productid,
      'qtystock': instance.qtystock,
      'price': instance.price,
      'discountedprice': instance.discountedprice,
      'discount': instance.discount,
      'productname': instance.productname,
      'subCatid': instance.subCatid,
      'subcategoryid': instance.subcategoryid,
      'subcatname': instance.subcatname,
      'categoryid': instance.categoryid,
      'catname': instance.catname,
      'productimage': instance.productimage,
    };
