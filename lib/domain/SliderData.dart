import 'package:flutter_app_ecommerce/domain/SliderData.dart';
import 'package:json_annotation/json_annotation.dart';

part 'SliderData.g.dart';

@JsonSerializable(explicitToJson: true)
class SliderData{


   String id="";

   String image="";

   SliderData();

   factory SliderData.fromJson(Map<String,dynamic>data) => _$SliderDataFromJson(data);


   Map<String,dynamic> toJson() => _$SliderDataToJson(this);
}