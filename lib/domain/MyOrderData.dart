/// status : 1
/// message : "Success"
/// data : [{"Orderid":"17","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 07:03:19","item_count":"1","expected_deli_date":"2022-03-31","total_price":"0.00","delevery_charge":"0.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 07:35:19","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"},{"Orderid":"18","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 07:03:13","item_count":"1","expected_deli_date":"2022-03-31","total_price":"0.00","delevery_charge":"0.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 07:49:13","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"},{"Orderid":"19","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 08:03:49","item_count":"1","expected_deli_date":"2022-03-31","total_price":"0.00","delevery_charge":"0.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 08:22:49","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"},{"Orderid":"20","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 08:03:44","item_count":"1","expected_deli_date":"2022-03-31","total_price":"0.00","delevery_charge":"0.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 08:31:44","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"},{"Orderid":"21","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 08:03:36","item_count":"1","expected_deli_date":"2022-03-31","total_price":"164.00","delevery_charge":"0.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 08:45:36","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"},{"Orderid":"22","id":"5","userid":"28","addressid":"5","order_type":"online","order_date":"2022-03-30 11:03:17","item_count":"1","expected_deli_date":"2022-03-31","total_price":"164.00","delevery_charge":"50.00","discount":"0.00","discounted_total_price":"0.00","cancel_status":"0","delivery_completion_status":"0","comments":"","created_at":null,"updated_at":"2022-03-30 11:10:17","sale_status":"0","payment_methode":"1","transactionid":null,"user_id":"28","name":"antony","houseno":"12B","area":"oomapadi","landmark":"kacheripady","latitude":"10.338325","longitude":"76.2294367","town":"Ernakulam","phone":"9748566325","pincode":"235689","addresstype":"Office"}]

class MyOrderData {
  MyOrderData({
     required int status,
    required  String message,
    required  List<Data> data,}){
    _status = status;
    _message = message;
    _data = data;
}

  MyOrderData.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['data'] != null) {
      _data = [];
      json['data'].forEach((v) {
        _data.add(Data.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Data> _data=[];

  int get status => _status;
  String get message => _message;
  List<Data> get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// Orderid : "17"
/// id : "5"
/// userid : "28"
/// addressid : "5"
/// order_type : "online"
/// order_date : "2022-03-30 07:03:19"
/// item_count : "1"
/// expected_deli_date : "2022-03-31"
/// total_price : "0.00"
/// delevery_charge : "0.00"
/// discount : "0.00"
/// discounted_total_price : "0.00"
/// cancel_status : "0"
/// delivery_completion_status : "0"
/// comments : ""
/// created_at : null
/// updated_at : "2022-03-30 07:35:19"
/// sale_status : "0"
/// payment_methode : "1"
/// transactionid : null
/// user_id : "28"
/// name : "antony"
/// houseno : "12B"
/// area : "oomapadi"
/// landmark : "kacheripady"
/// latitude : "10.338325"
/// longitude : "76.2294367"
/// town : "Ernakulam"
/// phone : "9748566325"
/// pincode : "235689"
/// addresstype : "Office"

class Data {
  Data({
      required String orderid,
      required String id,
      required String userid,
      required String addressid,
      required String orderType,
      required String orderDate,
      required String itemCount,
      required String expectedDeliDate,
      required String totalPrice,
      required String deleveryCharge,
      required String discount,
      required String discountedTotalPrice,
      required String cancelStatus,
      required String deliveryCompletionStatus,
      required String comments,
      dynamic createdAt, 
      required String updatedAt,
      required String saleStatus,
      required String paymentMethode,
      dynamic transactionid, 
      required String userId,
      required String name,
      required String houseno,
      required String area,
      required String landmark,
      required String latitude,
      required String longitude,
      required String town,
      required String phone,
      required String pincode,
      required String addresstype,}){
    _orderid = orderid;
    _id = id;
    _userid = userid;
    _addressid = addressid;
    _orderType = orderType;
    _orderDate = orderDate;
    _itemCount = itemCount;
    _expectedDeliDate = expectedDeliDate;
    _totalPrice = totalPrice;
    _deleveryCharge = deleveryCharge;
    _discount = discount;
    _discountedTotalPrice = discountedTotalPrice;
    _cancelStatus = cancelStatus;
    _deliveryCompletionStatus = deliveryCompletionStatus;
    _comments = comments;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _saleStatus = saleStatus;
    _paymentMethode = paymentMethode;
    _transactionid = transactionid;
    _userId = userId;
    _name = name;
    _houseno = houseno;
    _area = area;
    _landmark = landmark;
    _latitude = latitude;
    _longitude = longitude;
    _town = town;
    _phone = phone;
    _pincode = pincode;
    _addresstype = addresstype;
}

  Data.fromJson(dynamic json) {
    _orderid = json['Orderid'];
    _id = json['id'];
    _userid = json['userid'];
    _addressid = json['addressid'];
    _orderType = json['order_type'];
    _orderDate = json['order_date'];
    _itemCount = json['item_count'];
    _expectedDeliDate = json['expected_deli_date'];
    _totalPrice = json['total_price'];
    _deleveryCharge = json['delevery_charge'];
    _discount = json['discount'];
    _discountedTotalPrice = json['discounted_total_price'];
    _cancelStatus = json['cancel_status'];
    _deliveryCompletionStatus = json['delivery_completion_status'];
    _comments = json['comments'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _saleStatus = json['sale_status'];
    _paymentMethode = json['payment_methode'];
    _transactionid = json['transactionid'];
    _userId = json['user_id'];
    _name = json['name'];
    _houseno = json['houseno'];
    _area = json['area'];
    _landmark = json['landmark'];
    _latitude = json['latitude'];
    _longitude = json['longitude'];
    _town = json['town'];
    _phone = json['phone'];
    _pincode = json['pincode'];
    _addresstype = json['addresstype'];
  }
  String _orderid="";
  String _id="";
  String _userid="";
  String _addressid="";
  String _orderType="";
  String _orderDate="";
  String _itemCount="";
  String _expectedDeliDate="";
  String _totalPrice="";
  String _deleveryCharge="";
  String _discount="";
  String _discountedTotalPrice="";
  String _cancelStatus="";
  String _deliveryCompletionStatus="";
  String _comments="";
  dynamic _createdAt="";
  String _updatedAt="";
  String _saleStatus="";
  String _paymentMethode="";
  dynamic _transactionid="";
  String _userId="";
  String _name="";
  String _houseno="";
  String _area="";
  String _landmark="";
  String _latitude="";
  String _longitude="";
  String _town="";
  String _phone="";
  String _pincode="";
  String _addresstype="";
  String totalwithdeliverycharge="";

  String get orderid => _orderid;
  String get id => _id;
  String get userid => _userid;
  String get addressid => _addressid;
  String get orderType => _orderType;
  String get orderDate => _orderDate;
  String get itemCount => _itemCount;
  String get expectedDeliDate => _expectedDeliDate;
  String get totalPrice => _totalPrice;
  String get deleveryCharge => _deleveryCharge;
  String get discount => _discount;
  String get discountedTotalPrice => _discountedTotalPrice;
  String get cancelStatus => _cancelStatus;
  String get deliveryCompletionStatus => _deliveryCompletionStatus;
  String get comments => _comments;
  dynamic get createdAt => _createdAt;
  String get updatedAt => _updatedAt;
  String get saleStatus => _saleStatus;
  String get paymentMethode => _paymentMethode;
  dynamic get transactionid => _transactionid;
  String get userId => _userId;
  String get name => _name;
  String get houseno => _houseno;
  String get area => _area;
  String get landmark => _landmark;
  String get latitude => _latitude;
  String get longitude => _longitude;
  String get town => _town;
  String get phone => _phone;
  String get pincode => _pincode;
  String get addresstype => _addresstype;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['Orderid'] = _orderid;
    map['id'] = _id;
    map['userid'] = _userid;
    map['addressid'] = _addressid;
    map['order_type'] = _orderType;
    map['order_date'] = _orderDate;
    map['item_count'] = _itemCount;
    map['expected_deli_date'] = _expectedDeliDate;
    map['total_price'] = _totalPrice;
    map['delevery_charge'] = _deleveryCharge;
    map['discount'] = _discount;
    map['discounted_total_price'] = _discountedTotalPrice;
    map['cancel_status'] = _cancelStatus;
    map['delivery_completion_status'] = _deliveryCompletionStatus;
    map['comments'] = _comments;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['sale_status'] = _saleStatus;
    map['payment_methode'] = _paymentMethode;
    map['transactionid'] = _transactionid;
    map['user_id'] = _userId;
    map['name'] = _name;
    map['houseno'] = _houseno;
    map['area'] = _area;
    map['landmark'] = _landmark;
    map['latitude'] = _latitude;
    map['longitude'] = _longitude;
    map['town'] = _town;
    map['phone'] = _phone;
    map['pincode'] = _pincode;
    map['addresstype'] = _addresstype;
    return map;
  }

}