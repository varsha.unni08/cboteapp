/// UPI : true
/// IMPS : true
/// DISTRICT : "ERNAKULAM"
/// NEFT : true
/// RTGS : true
/// SWIFT : ""
/// CITY : "KOCHI"
/// CONTACT : "+914842344840"
/// MICR : "682002907"
/// BRANCH : "PALARIVATTOM"
/// CENTRE : "ERNAKULAM"
/// STATE : "KERALA"
/// ADDRESS : "PBNO2298,PREETHIBUILDINGS,PALARIVATTOMPO682025PALARIVATTOMATSBTCOIN"
/// BANK : "State Bank of India"
/// BANKCODE : "SBIN"
/// IFSC : "SBIN0070403"

class BranchIFSC {
  bool _upi=false;
  bool _imps=false;
  String _district="";
  bool _neft=false;
  bool _rtgs=false;
  String _swift="";
  String _city="";
  String _contact="";
  String _micr="";
  String _branch="";
  String _centre="";
  String _state="";
  String _address="";
  String _bank="";
  String _bankcode="";
  String _ifsc="";

  bool get upi => _upi;
  bool get imps => _imps;
  String get district => _district;
  bool get neft => _neft;
  bool get rtgs => _rtgs;
  String get swift => _swift;
  String get city => _city;
  String get contact => _contact;
  String get micr => _micr;
  String get branch => _branch;
  String get centre => _centre;
  String get state => _state;
  String get address => _address;
  String get bank => _bank;
  String get bankcode => _bankcode;
  String get ifsc => _ifsc;

  BranchIFSC({
      required bool upi,
      required bool imps,
      required String district,
      required bool neft,
      required bool rtgs,
      required String swift,
      required String city,
      required String contact,
      required String micr,
      required String branch,
      required String centre,
      required String state,
      required String address,
      required String bank,
      required String bankcode,
      required String ifsc}){
    _upi = upi;
    _imps = imps;
    _district = district;
    _neft = neft;
    _rtgs = rtgs;
    _swift = swift;
    _city = city;
    _contact = contact;
    _micr = micr;
    _branch = branch;
    _centre = centre;
    _state = state;
    _address = address;
    _bank = bank;
    _bankcode = bankcode;
    _ifsc = ifsc;
}

  BranchIFSC.fromJson(dynamic json) {
    _upi = json['UPI'];
    _imps = json['IMPS'];
    _district = json['DISTRICT'];
    _neft = json['NEFT'];
    _rtgs = json['RTGS'];
    _swift = json['SWIFT'];
    _city = json['CITY'];
    _contact = json['CONTACT'];
    _micr = json['MICR'];
    _branch = json['BRANCH'];
    _centre = json['CENTRE'];
    _state = json['STATE'];
    _address = json['ADDRESS'];
    _bank = json['BANK'];
    _bankcode = json['BANKCODE'];
    _ifsc = json['IFSC'];
  }

  Map<String, dynamic> toJson() {
    var map = <String, dynamic>{};
    map['UPI'] = _upi;
    map['IMPS'] = _imps;
    map['DISTRICT'] = _district;
    map['NEFT'] = _neft;
    map['RTGS'] = _rtgs;
    map['SWIFT'] = _swift;
    map['CITY'] = _city;
    map['CONTACT'] = _contact;
    map['MICR'] = _micr;
    map['BRANCH'] = _branch;
    map['CENTRE'] = _centre;
    map['STATE'] = _state;
    map['ADDRESS'] = _address;
    map['BANK'] = _bank;
    map['BANKCODE'] = _bankcode;
    map['IFSC'] = _ifsc;
    return map;
  }

}