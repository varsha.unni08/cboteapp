/// status : 1
/// message : "Success"
/// requestdata : [{"id":"1","userid":"28","requesteddate":"2022-05-04 05:55:01","amount":"10","accountnumber":"123456789","ifsc":"igghgh","upi_id":"","transactionid":"0","transfereddate":"0000-00-00 00:00:00","status":"0"}]

class WithdrawalRequests {
  WithdrawalRequests({
    required  int status,
    required  String message,
     required List<Requestdata> requestdata,}){
    _status = status;
    _message = message;
    _requestdata = requestdata;
}

  WithdrawalRequests.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['requestdata'] != null) {
      _requestdata = [];
      json['requestdata'].forEach((v) {
        _requestdata.add(Requestdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Requestdata> _requestdata=[];

  int get status => _status;
  String get message => _message;
  List<Requestdata> get requestdata => _requestdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_requestdata != null) {
      map['requestdata'] = _requestdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// userid : "28"
/// requesteddate : "2022-05-04 05:55:01"
/// amount : "10"
/// accountnumber : "123456789"
/// ifsc : "igghgh"
/// upi_id : ""
/// transactionid : "0"
/// transfereddate : "0000-00-00 00:00:00"
/// status : "0"

class Requestdata {
  Requestdata({
      required String id,
      required String userid,
      required String requesteddate,
      required String amount,
      required String accountnumber,
      required String ifsc,
      required String upiId,
      required String transactionid,
      required String transfereddate,
      required String status,}){
    _id = id;
    _userid = userid;
    _requesteddate = requesteddate;
    _amount = amount;
    _accountnumber = accountnumber;
    _ifsc = ifsc;
    _upiId = upiId;
    _transactionid = transactionid;
    _transfereddate = transfereddate;
    _status = status;
}

  Requestdata.fromJson(dynamic json) {
    _id = json['id'];
    _userid = json['userid'];
    _requesteddate = json['requesteddate'];
    _amount = json['amount'];
    _accountnumber = json['accountnumber'];
    _ifsc = json['ifsc'];
    _upiId = json['upi_id'];
    _transactionid = json['transactionid'];
    _transfereddate = json['transfereddate'];
    _status = json['status'];
  }
  String _id="";
  String _userid="";
  String _requesteddate="";
  String _amount="";
  String _accountnumber="";
  String _ifsc="";
  String _upiId="";
  String _transactionid="";
  String _transfereddate="";
  String _status="";

  String get id => _id;
  String get userid => _userid;
  String get requesteddate => _requesteddate;
  String get amount => _amount;
  String get accountnumber => _accountnumber;
  String get ifsc => _ifsc;
  String get upiId => _upiId;
  String get transactionid => _transactionid;
  String get transfereddate => _transfereddate;
  String get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['userid'] = _userid;
    map['requesteddate'] = _requesteddate;
    map['amount'] = _amount;
    map['accountnumber'] = _accountnumber;
    map['ifsc'] = _ifsc;
    map['upi_id'] = _upiId;
    map['transactionid'] = _transactionid;
    map['transfereddate'] = _transfereddate;
    map['status'] = _status;
    return map;
  }

}