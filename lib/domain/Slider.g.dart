// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Slider.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Sliders _$SlidersFromJson(Map<String, dynamic> json) {
  return Sliders()
    ..status = json['status'] as int
    ..message = json['message'] as String
    ..data = (json['data'] as List<dynamic>)
        .map((e) => SliderData.fromJson(e as Map<String, dynamic>))
        .toList();
}

Map<String, dynamic> _$SlidersToJson(Sliders instance) => <String, dynamic>{
      'status': instance.status,
      'message': instance.message,
      'data': instance.data.map((e) => e.toJson()).toList(),
    };
