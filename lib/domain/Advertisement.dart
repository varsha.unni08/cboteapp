/// status : 1
/// message : "Success"
/// advertisedata : [{"id":"1","title":"14% Offer ","description":"14% Offer","photo":"melam.jpeg","type":"2","parameter":"1","stockid":"0"},{"id":"2","title":"Offer in Oil category","description":"Offer in Oil category","photo":"cookingoilad.jpg","type":"1","parameter":"2","stockid":"0"},{"id":"3","title":"Cashback Offer","description":"Cashback Offer","photo":"melam.jpeg","type":"4","parameter":"2","stockid":"0"},{"id":"4","title":"Price between ₹100-₹250","description":"Price between ₹100-₹250","photo":"ii.jpeg","type":"3","parameter":"0","stockid":"0"}]

class Advertisement {
  Advertisement({
      required int status,
      required String message,
      required List<Advertisedata> advertisedata,}){
    _status = status;
    _message = message;
    _advertisedata = advertisedata;
}

  Advertisement.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['advertisedata'] != null) {
      _advertisedata = [];
      json['advertisedata'].forEach((v) {
        _advertisedata.add(Advertisedata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Advertisedata> _advertisedata=[];

  int get status => _status;
  String get message => _message;
  List<Advertisedata> get advertisedata => _advertisedata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_advertisedata != null) {
      map['advertisedata'] = _advertisedata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// id : "1"
/// title : "14% Offer "
/// description : "14% Offer"
/// photo : "melam.jpeg"
/// type : "2"
/// parameter : "1"
/// stockid : "0"

class Advertisedata {
  Advertisedata({
      required String id,
      required String title,
      required String description,
      required String photo,
      required String type,
      required String parameter,
      required String stockid,}){
    _id = id;
    _title = title;
    _description = description;
    _photo = photo;
    _type = type;
    _parameter = parameter;
    _stockid = stockid;
}

  Advertisedata.fromJson(dynamic json) {
    _id = json['id'];
    _title = json['title'];
    _description = json['description'];
    _photo = json['photo'];
    _type = json['type'];
    _parameter = json['parameter'];
    _stockid = json['stockid'];
  }
  String _id="";
  String _title="";
  String _description="";
  String _photo="";
  String _type="";
  String _parameter="";
  String _stockid="";

  String get id => _id;
  String get title => _title;
  String get description => _description;
  String get photo => _photo;
  String get type => _type;
  String get parameter => _parameter;
  String get stockid => _stockid;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['title'] = _title;
    map['description'] = _description;
    map['photo'] = _photo;
    map['type'] = _type;
    map['parameter'] = _parameter;
    map['stockid'] = _stockid;
    return map;
  }

}