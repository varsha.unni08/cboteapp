// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Product.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Product _$ProductFromJson(Map<String, dynamic> json) {
  return Product()
    ..id = json['id'] as String
    ..subCatid = json['subCatid'] as String
    ..productName = json['productName'] as String
    ..description = json['description'] as String
    ..image = json['image'] as String;
}

Map<String, dynamic> _$ProductToJson(Product instance) => <String, dynamic>{
      'id': instance.id,
      'subCatid': instance.subCatid,
      'productName': instance.productName,
      'description': instance.description,
      'image': instance.image,
    };
