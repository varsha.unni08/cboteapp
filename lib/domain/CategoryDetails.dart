/// status : 1
/// message : "Success"
/// categorydata : "Tea and coffee"

class CategoryDetails {
  CategoryDetails({
      required int status,
      required String message,
      required String categorydata,}){
    _status = status;
    _message = message;
    _categorydata = categorydata;
}

  CategoryDetails.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _categorydata = json['categorydata'];
  }
  int _status=0;
  String _message="";
  String _categorydata="";

  int get status => _status;
  String get message => _message;
  String get categorydata => _categorydata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    map['categorydata'] = _categorydata;
    return map;
  }

}