/// status : 1
/// message : "Success"
/// detailsdata : [{"sales_details_id":"4","id":"96","orderid":"4","shopid":"1","stockid":"96","qty":"1","qty_to_deliver":"1","deliverydate":"0000-00-00","delivery_status":"0","cancel_status":"0","return_date":null,"return_qty":"0","return_status":"0","return_request_status":"0","price":"175.00","discount":"25.00","cgst":"3.50","sgst":"3.50","igst":"7.00","total_price":"164.00","cash_back":"3.28","feedback":null,"comment":null,"invoice_status":"0","cartid":"28","productid":"62","sub_catid":"8","product_name":"Genuine Coffee gold 200gm","description":"Brand name : Genuine coffee gold \r\nWeight : 200gm\r\n\r\nCoffee is more than a tool to make it until the end of the day, it is a highly bioactive plant filled with antioxidants, fatty acids, diterpene esters, melanoidins, proteins, sugars, etc. all of which are beneficial for human health, skin and well-being.  Therefore, we look at it as a sustainable health elixir, rather than a simple beverage.In fact, when coffee is brewed to produce a coffee beverage, less than 1% of its compounds are extracted. The used coffee grounds contain all the bioactive compounds mentioned above","isreturnable":"0","make":null,"height":"10.00","width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"coffee.jpeg","image2":"coffee.jpeg","image3":"coffee.jpeg","image4":"coffee.jpeg","image5":"coffee.jpeg","status":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","discounted_price":"150.00","isOffer":"1","offer_percent":"0","isCashBack":"0","cashback_amount":"6","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null,"purchase_without_gst":"0.00","purchase_with_tax":"0.00","MRP":"200.00","discount_percent":"15.00","cashback_percent":"2.00","salespri_without_gst":"160.00","salespri_with_gst":"164.00","reff_income_percent":"0.00","reff_gen_status":"0","cashback_gen_status":"0"},{"sales_details_id":"5","id":"98","orderid":"4","shopid":"1","stockid":"98","qty":"1","qty_to_deliver":"1","deliverydate":"0000-00-00","delivery_status":"0","cancel_status":"0","return_date":null,"return_qty":"0","return_status":"0","return_request_status":"0","price":"175.00","discount":"25.00","cgst":"3.50","sgst":"3.50","igst":"7.00","total_price":"164.00","cash_back":"3.28","feedback":null,"comment":null,"invoice_status":"0","cartid":"29","productid":"64","sub_catid":"19","product_name":"Genuine leaf bud Premium quality tea 1kg ","description":"Brand name : Genuine leaf bud Premium quality tea\r\nWeight : 1kg\r\nNo matter what the season, tea can be a tasty beverage since it can be served iced or hot.\r\nBut its benefits go far beyond refreshment. There is plenty of research showing that drinking tea can actually improve your health.\r\n\r\nA recent study, published in European Journal of Preventive Cardiology, involved more than 100,000 adults in China and found those who regularly drank tea were less likely to develop atherosclerotic cardiovascular disease or die prematurely from any cause — particularly stroke — compared to others during a seven-year follow-up.","isreturnable":"0","make":null,"height":"10.00","width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"gt1kg.jpeg","image2":"gt1kg.jpeg","image3":"gt1kg.jpeg","image4":"gt1kg.jpeg","image5":"gt1kg.jpeg","status":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","discounted_price":"150.00","isOffer":"1","offer_percent":"0","isCashBack":"0","cashback_amount":"6","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null,"purchase_without_gst":"0.00","purchase_with_tax":"0.00","MRP":"200.00","discount_percent":"15.00","cashback_percent":"2.00","salespri_without_gst":"160.00","salespri_with_gst":"164.00","reff_income_percent":"0.00","reff_gen_status":"0","cashback_gen_status":"0"},{"sales_details_id":"6","id":"100","orderid":"4","shopid":"1","stockid":"100","qty":"1","qty_to_deliver":"1","deliverydate":"0000-00-00","delivery_status":"0","cancel_status":"0","return_date":null,"return_qty":"0","return_status":"0","return_request_status":"0","price":"175.00","discount":"25.00","cgst":"3.50","sgst":"3.50","igst":"7.00","total_price":"164.00","cash_back":"3.28","feedback":null,"comment":null,"invoice_status":"0","cartid":"30","productid":"66","sub_catid":"19","product_name":"Genuine Strong Premium quality tea 5kg","description":"Brand name : Genuine Strong Premium quality tea\r\nWeight : 5kg\r\nNo matter what the season, tea can be a tasty beverage since it can be served iced or hot.\r\nBut its benefits go far beyond refreshment. There is plenty of research showing that drinking tea can actually improve your health.\r\n\r\nA recent study, published in European Journal of Preventive Cardiology, involved more than 100,000 adults in China and found those who regularly drank tea were less likely to develop atherosclerotic cardiovascular disease or die prematurely from any cause — particularly stroke — compared to others during a seven-year follow-up.","isreturnable":"0","make":null,"height":"10.00","width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"gt5kg.jpeg","image2":"gt5kg.jpeg","image3":"gt5kg.jpeg","image4":"gt5kg.jpeg","image5":"gt5kg.jpeg","status":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","discounted_price":"150.00","isOffer":"1","offer_percent":"0","isCashBack":"0","cashback_amount":"6","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null,"purchase_without_gst":"0.00","purchase_with_tax":"0.00","MRP":"200.00","discount_percent":"15.00","cashback_percent":"2.00","salespri_without_gst":"160.00","salespri_with_gst":"164.00","reff_income_percent":"0.00","reff_gen_status":"0","cashback_gen_status":"0"},{"sales_details_id":"7","id":"103","orderid":"4","shopid":"1","stockid":"103","qty":"1","qty_to_deliver":"1","deliverydate":"0000-00-00","delivery_status":"0","cancel_status":"1","return_date":null,"return_qty":"0","return_status":"0","return_request_status":"0","price":"175.00","discount":"25.00","cgst":"3.50","sgst":"3.50","igst":"7.00","total_price":"164.00","cash_back":"3.28","feedback":null,"comment":null,"invoice_status":"0","cartid":"31","productid":"69","sub_catid":"19","product_name":"Lemount Garden fresh Premium tea 1kg","description":"Brand name : Lemount Garden fresh Premium tea \r\nWeight : 1kg\r\nNo matter what the season, tea can be a tasty beverage since it can be served iced or hot.\r\nBut its benefits go far beyond refreshment. There is plenty of research showing that drinking tea can actually improve your health.\r\n\r\nA recent study, published in European Journal of Preventive Cardiology, involved more than 100,000 adults in China and found those who regularly drank tea were less likely to develop atherosclerotic cardiovascular disease or die prematurely from any cause — particularly stroke — compared to others during a seven-year follow-up.\r\nTea's ability to lower blood pressure may be why it could reduce a person's risk of dying of a stroke, the editorial noted.\r\nTea, especially green tea, is a rich source of flavonoids, bioactive compounds that can lessen oxidative stress, relieve inflammation, and provide other health benefits, the study authors noted. They cautioned more research was needed to see whether the results in Chinese adults would also apply to people outside east Asia.","isreturnable":"0","make":null,"height":"10.00","width":null,"weight":null,"size":null,"thickness":null,"color":null,"sizedifference":null,"image1":"lemount1kg.jpeg","image2":"lemount1kg.jpeg","image3":"lemount1kg.jpeg","image4":"lemount1kg.jpeg","image5":"lemount1kg.jpeg","status":"1","batchno":"AAA","shop_id":"1","warehouse_id":"1","distributor_id":"1","make_date":"2022-02-20","expe_date":"2022-04-30","qty_stock":"10","qty_damage":"0","qty_rol":"25","discounted_price":"150.00","isOffer":"1","offer_percent":"0","isCashBack":"0","cashback_amount":"6","total_with_gst":"0.00","stock_status":"0","iswide":"0","stock_update_date":"2021-05-19","commition_agent_id":null,"added_user_id":"1","barcode_no":"sdgsdgdsfhdfh","created_at":null,"updated_at":null,"purchase_without_gst":"0.00","purchase_with_tax":"0.00","MRP":"200.00","discount_percent":"15.00","cashback_percent":"2.00","salespri_without_gst":"160.00","salespri_with_gst":"164.00","reff_income_percent":"0.00","reff_gen_status":"0","cashback_gen_status":"0"}]

class OrderDetailsData {
  OrderDetailsData({
     required int status,
    required String message,
    required List<Detailsdata> detailsdata,}){
    _status = status;
    _message = message;
    _detailsdata = detailsdata;
}

  OrderDetailsData.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    if (json['detailsdata'] != null) {
      _detailsdata = [];
      json['detailsdata'].forEach((v) {
        _detailsdata.add(Detailsdata.fromJson(v));
      });
    }
  }
  int _status=0;
  String _message="";
  List<Detailsdata> _detailsdata=[];

  int get status => _status;
  String get message => _message;
  List<Detailsdata> get detailsdata => _detailsdata;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_detailsdata != null) {
      map['detailsdata'] = _detailsdata.map((v) => v.toJson()).toList();
    }
    return map;
  }

}

/// sales_details_id : "4"
/// id : "96"
/// orderid : "4"
/// shopid : "1"
/// stockid : "96"
/// qty : "1"
/// qty_to_deliver : "1"
/// deliverydate : "0000-00-00"
/// delivery_status : "0"
/// cancel_status : "0"
/// return_date : null
/// return_qty : "0"
/// return_status : "0"
/// return_request_status : "0"
/// price : "175.00"
/// discount : "25.00"
/// cgst : "3.50"
/// sgst : "3.50"
/// igst : "7.00"
/// total_price : "164.00"
/// cash_back : "3.28"
/// feedback : null
/// comment : null
/// invoice_status : "0"
/// cartid : "28"
/// productid : "62"
/// sub_catid : "8"
/// product_name : "Genuine Coffee gold 200gm"
/// description : "Brand name : Genuine coffee gold \r\nWeight : 200gm\r\n\r\nCoffee is more than a tool to make it until the end of the day, it is a highly bioactive plant filled with antioxidants, fatty acids, diterpene esters, melanoidins, proteins, sugars, etc. all of which are beneficial for human health, skin and well-being.  Therefore, we look at it as a sustainable health elixir, rather than a simple beverage.In fact, when coffee is brewed to produce a coffee beverage, less than 1% of its compounds are extracted. The used coffee grounds contain all the bioactive compounds mentioned above"
/// isreturnable : "0"
/// make : null
/// height : "10.00"
/// width : null
/// weight : null
/// size : null
/// thickness : null
/// color : null
/// sizedifference : null
/// image1 : "coffee.jpeg"
/// image2 : "coffee.jpeg"
/// image3 : "coffee.jpeg"
/// image4 : "coffee.jpeg"
/// image5 : "coffee.jpeg"
/// status : "1"
/// batchno : "AAA"
/// shop_id : "1"
/// warehouse_id : "1"
/// distributor_id : "1"
/// make_date : "2022-02-20"
/// expe_date : "2022-04-30"
/// qty_stock : "10"
/// qty_damage : "0"
/// qty_rol : "25"
/// discounted_price : "150.00"
/// isOffer : "1"
/// offer_percent : "0"
/// isCashBack : "0"
/// cashback_amount : "6"
/// total_with_gst : "0.00"
/// stock_status : "0"
/// iswide : "0"
/// stock_update_date : "2021-05-19"
/// commition_agent_id : null
/// added_user_id : "1"
/// barcode_no : "sdgsdgdsfhdfh"
/// created_at : null
/// updated_at : null
/// purchase_without_gst : "0.00"
/// purchase_with_tax : "0.00"
/// MRP : "200.00"
/// discount_percent : "15.00"
/// cashback_percent : "2.00"
/// salespri_without_gst : "160.00"
/// salespri_with_gst : "164.00"
/// reff_income_percent : "0.00"
/// reff_gen_status : "0"
/// cashback_gen_status : "0"

class Detailsdata {
  Detailsdata({
     required String salesDetailsId,
      required String id,
      required String orderid,
      required String shopid,
      required String stockid,
      required String qty,
      required String qtyToDeliver,
      required String deliverydate,
      required String deliveryStatus,
      required String cancelStatus,
      dynamic returnDate, 
      required String returnQty,
      required String returnStatus,
      required String returnRequestStatus,
      required String price,
      required String discount,
      required String cgst,
      required String sgst,
      required String igst,
      required String totalPrice,
      required String cashBack,
      dynamic feedback, 
      dynamic comment, 
      required String invoiceStatus,
      required String cartid,
      required String productid,
      required String subCatid,
      required String productName,
      required String description,
      required String isreturnable,
      dynamic make, 
      required String height,
      dynamic width, 
      dynamic weight, 
      dynamic size, 
      dynamic thickness, 
      dynamic color, 
      dynamic sizedifference, 
      required String image1,
      required String image2,
      required String image3,
      required String image4,
      required String image5,
      required String status,
      required String batchno,
      required String shopId,
      required String warehouseId,
      required String distributorId,
      required String makeDate,
      required String expeDate,
      required String qtyStock,
      required String qtyDamage,
      required String qtyRol,
      required String discountedPrice,
      required String isOffer,
      required String offerPercent,
      required String isCashBack,
      required String cashbackAmount,
      required String totalWithGst,
      required String stockStatus,
      required String iswide,
      required String stockUpdateDate,
      dynamic commitionAgentId, 
      required String addedUserId,
      required String barcodeNo,
      dynamic createdAt, 
      dynamic updatedAt, 
      required String purchaseWithoutGst,
      required String purchaseWithTax,
      required String mrp,
      required String discountPercent,
      required String cashbackPercent,
      required String salespriWithoutGst,
      required String salespriWithGst,
      required String reffIncomePercent,
      required String reffGenStatus,
      required String cashbackGenStatus,}){
    _salesDetailsId = salesDetailsId;
    _id = id;
    _orderid = orderid;
    _shopid = shopid;
    _stockid = stockid;
    _qty = qty;
    _qtyToDeliver = qtyToDeliver;
    _deliverydate = deliverydate;
    _deliveryStatus = deliveryStatus;
    _cancelStatus = cancelStatus;
    _returnDate = returnDate;
    _returnQty = returnQty;
    _returnStatus = returnStatus;
    _returnRequestStatus = returnRequestStatus;
    _price = price;
    _discount = discount;
    _cgst = cgst;
    _sgst = sgst;
    _igst = igst;
    _totalPrice = totalPrice;
    _cashBack = cashBack;
    _feedback = feedback;
    _comment = comment;
    _invoiceStatus = invoiceStatus;
    _cartid = cartid;
    _productid = productid;
    _subCatid = subCatid;
    _productName = productName;
    _description = description;
    _isreturnable = isreturnable;
    _make = make;
    _height = height;
    _width = width;
    _weight = weight;
    _size = size;
    _thickness = thickness;
    _color = color;
    _sizedifference = sizedifference;
    _image1 = image1;
    _image2 = image2;
    _image3 = image3;
    _image4 = image4;
    _image5 = image5;
    _status = status;
    _batchno = batchno;
    _shopId = shopId;
    _warehouseId = warehouseId;
    _distributorId = distributorId;
    _makeDate = makeDate;
    _expeDate = expeDate;
    _qtyStock = qtyStock;
    _qtyDamage = qtyDamage;
    _qtyRol = qtyRol;
    _discountedPrice = discountedPrice;
    _isOffer = isOffer;
    _offerPercent = offerPercent;
    _isCashBack = isCashBack;
    _cashbackAmount = cashbackAmount;
    _totalWithGst = totalWithGst;
    _stockStatus = stockStatus;
    _iswide = iswide;
    _stockUpdateDate = stockUpdateDate;
    _commitionAgentId = commitionAgentId;
    _addedUserId = addedUserId;
    _barcodeNo = barcodeNo;
    _createdAt = createdAt;
    _updatedAt = updatedAt;
    _purchaseWithoutGst = purchaseWithoutGst;
    _purchaseWithTax = purchaseWithTax;
    _mrp = mrp;
    _discountPercent = discountPercent;
    _cashbackPercent = cashbackPercent;
    _salespriWithoutGst = salespriWithoutGst;
    _salespriWithGst = salespriWithGst;
    _reffIncomePercent = reffIncomePercent;
    _reffGenStatus = reffGenStatus;
    _cashbackGenStatus = cashbackGenStatus;
}

  Detailsdata.fromJson(dynamic json) {
    _salesDetailsId = json['sales_details_id'];
    _id = json['id'];
    _orderid = json['orderid'];
    _shopid = json['shopid'];
    _stockid = json['stockid'];
    _qty = json['qty'];
    _qtyToDeliver = json['qty_to_deliver'];
    _deliverydate = json['deliverydate'];
    _deliveryStatus = json['delivery_status'];
    _cancelStatus = json['cancel_status'];
    _returnDate = json['return_date'];
    _returnQty = json['return_qty'];
    _returnStatus = json['return_status'];
    _returnRequestStatus = json['return_request_status'];
    _price = json['price'];
    _discount = json['discount'];
    _cgst = json['cgst'];
    _sgst = json['sgst'];
    _igst = json['igst'];
    _totalPrice = json['total_price'];
    _cashBack = json['cash_back'];
    _feedback = json['feedback'];
    _comment = json['comment'];
    _invoiceStatus = json['invoice_status'];
    _cartid = json['cartid'];
    _productid = json['productid'];
    _subCatid = json['sub_catid'];
    _productName = json['product_name'];
    _description = json['description'];
    _isreturnable = json['isreturnable'];
    _make = json['make'];
    _height = json['height'];
    _width = json['width'];
    _weight = json['weight'];
    _size = json['size'];
    _thickness = json['thickness'];
    _color = json['color'];
    _sizedifference = json['sizedifference'];
    _image1 = json['image1'];
    _image2 = json['image2'];
    _image3 = json['image3'];
    _image4 = json['image4'];
    _image5 = json['image5'];
    _status = json['status'];
    _batchno = json['batchno'];
    _shopId = json['shop_id'];
    _warehouseId = json['warehouse_id'];
    _distributorId = json['distributor_id'];
    _makeDate = json['make_date'];
    _expeDate = json['expe_date'];
    _qtyStock = json['qty_stock'];
    _qtyDamage = json['qty_damage'];
    _qtyRol = json['qty_rol'];
    _discountedPrice = json['discounted_price'];
    _isOffer = json['isOffer'];
    _offerPercent = json['offer_percent'];
    _isCashBack = json['isCashBack'];
    _cashbackAmount = json['cashback_amount'];
    _totalWithGst = json['total_with_gst'];
    _stockStatus = json['stock_status'];
    _iswide = json['iswide'];
    _stockUpdateDate = json['stock_update_date'];
    _commitionAgentId = json['commition_agent_id'];
    _addedUserId = json['added_user_id'];
    _barcodeNo = json['barcode_no'];
    _createdAt = json['created_at'];
    _updatedAt = json['updated_at'];
    _purchaseWithoutGst = json['purchase_without_gst'];
    _purchaseWithTax = json['purchase_with_tax'];
    _mrp = json['MRP'];
    _discountPercent = json['discount_percent'];
    _cashbackPercent = json['cashback_percent'];
    _salespriWithoutGst = json['salespri_without_gst'];
    _salespriWithGst = json['salespri_with_gst'];
    _reffIncomePercent = json['reff_income_percent'];
    _reffGenStatus = json['reff_gen_status'];
    _cashbackGenStatus = json['cashback_gen_status'];
  }
  String _salesDetailsId="";
  String _id="";
  String _orderid="";
  String _shopid="";
  String _stockid="";
  String _qty="";
  String _qtyToDeliver="";
  String _deliverydate="";
  String _deliveryStatus="";
  String _cancelStatus="";
  dynamic _returnDate="";
  String _returnQty="";
  String _returnStatus="";
  String _returnRequestStatus="";
  String _price="";
  String _discount="";
  String _cgst="";
  String _sgst="";
  String _igst="";
  String _totalPrice="";
  String _cashBack="";
  dynamic _feedback="";
  dynamic _comment="";
  String _invoiceStatus="";
  String _cartid="";
  String _productid="";
  String _subCatid="";
  String _productName="";
  String _description="";
  String _isreturnable="";
  dynamic _make="";
  String _height="";
  dynamic _width="";
  dynamic _weight="";
  dynamic _size="";
  dynamic _thickness="";
  dynamic _color="";
  dynamic _sizedifference="";
  String _image1="";
  String _image2="";
  String _image3="";
  String _image4="";
  String _image5="";
  String _status="";
  String _batchno="";
  String _shopId="";
  String _warehouseId="";
  String _distributorId="";
  String _makeDate="";
  String _expeDate="";
  String _qtyStock="";
  String _qtyDamage="";
  String _qtyRol="";
  String _discountedPrice="";
  String _isOffer="";
  String _offerPercent="";
  String _isCashBack="";
  String _cashbackAmount="";
  String _totalWithGst="";
  String _stockStatus="";
  String _iswide="";
  String _stockUpdateDate="";
  dynamic _commitionAgentId="";
  String _addedUserId="";
  String _barcodeNo="";
  dynamic _createdAt="";
  dynamic _updatedAt="";
  String _purchaseWithoutGst="";
  String _purchaseWithTax="";
  String _mrp="";
  String _discountPercent="";
  String _cashbackPercent="";
  String _salespriWithoutGst="";
  String _salespriWithGst="";
  String _reffIncomePercent="";
  String _reffGenStatus="";
  String _cashbackGenStatus="";
  String TotalPricebyqty="0";

  String get salesDetailsId => _salesDetailsId;
  String get id => _id;
  String get orderid => _orderid;
  String get shopid => _shopid;
  String get stockid => _stockid;
  String get qty => _qty;
  String get qtyToDeliver => _qtyToDeliver;
  String get deliverydate => _deliverydate;
  String get deliveryStatus => _deliveryStatus;
  String get cancelStatus => _cancelStatus;
  dynamic get returnDate => _returnDate;
  String get returnQty => _returnQty;
  String get returnStatus => _returnStatus;
  String get returnRequestStatus => _returnRequestStatus;
  String get price => _price;
  String get discount => _discount;
  String get cgst => _cgst;
  String get sgst => _sgst;
  String get igst => _igst;
  String get totalPrice => _totalPrice;
  String get cashBack => _cashBack;
  dynamic get feedback => _feedback;
  dynamic get comment => _comment;
  String get invoiceStatus => _invoiceStatus;
  String get cartid => _cartid;
  String get productid => _productid;
  String get subCatid => _subCatid;
  String get productName => _productName;
  String get description => _description;
  String get isreturnable => _isreturnable;
  dynamic get make => _make;
  String get height => _height;
  dynamic get width => _width;
  dynamic get weight => _weight;
  dynamic get size => _size;
  dynamic get thickness => _thickness;
  dynamic get color => _color;
  dynamic get sizedifference => _sizedifference;
  String get image1 => _image1;
  String get image2 => _image2;
  String get image3 => _image3;
  String get image4 => _image4;
  String get image5 => _image5;
  String get status => _status;
  String get batchno => _batchno;
  String get shopId => _shopId;
  String get warehouseId => _warehouseId;
  String get distributorId => _distributorId;
  String get makeDate => _makeDate;
  String get expeDate => _expeDate;
  String get qtyStock => _qtyStock;
  String get qtyDamage => _qtyDamage;
  String get qtyRol => _qtyRol;
  String get discountedPrice => _discountedPrice;
  String get isOffer => _isOffer;
  String get offerPercent => _offerPercent;
  String get isCashBack => _isCashBack;
  String get cashbackAmount => _cashbackAmount;
  String get totalWithGst => _totalWithGst;
  String get stockStatus => _stockStatus;
  String get iswide => _iswide;
  String get stockUpdateDate => _stockUpdateDate;
  dynamic get commitionAgentId => _commitionAgentId;
  String get addedUserId => _addedUserId;
  String get barcodeNo => _barcodeNo;
  dynamic get createdAt => _createdAt;
  dynamic get updatedAt => _updatedAt;
  String get purchaseWithoutGst => _purchaseWithoutGst;
  String get purchaseWithTax => _purchaseWithTax;
  String get mrp => _mrp;
  String get discountPercent => _discountPercent;
  String get cashbackPercent => _cashbackPercent;
  String get salespriWithoutGst => _salespriWithoutGst;
  String get salespriWithGst => _salespriWithGst;
  String get reffIncomePercent => _reffIncomePercent;
  String get reffGenStatus => _reffGenStatus;
  String get cashbackGenStatus => _cashbackGenStatus;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['sales_details_id'] = _salesDetailsId;
    map['id'] = _id;
    map['orderid'] = _orderid;
    map['shopid'] = _shopid;
    map['stockid'] = _stockid;
    map['qty'] = _qty;
    map['qty_to_deliver'] = _qtyToDeliver;
    map['deliverydate'] = _deliverydate;
    map['delivery_status'] = _deliveryStatus;
    map['cancel_status'] = _cancelStatus;
    map['return_date'] = _returnDate;
    map['return_qty'] = _returnQty;
    map['return_status'] = _returnStatus;
    map['return_request_status'] = _returnRequestStatus;
    map['price'] = _price;
    map['discount'] = _discount;
    map['cgst'] = _cgst;
    map['sgst'] = _sgst;
    map['igst'] = _igst;
    map['total_price'] = _totalPrice;
    map['cash_back'] = _cashBack;
    map['feedback'] = _feedback;
    map['comment'] = _comment;
    map['invoice_status'] = _invoiceStatus;
    map['cartid'] = _cartid;
    map['productid'] = _productid;
    map['sub_catid'] = _subCatid;
    map['product_name'] = _productName;
    map['description'] = _description;
    map['isreturnable'] = _isreturnable;
    map['make'] = _make;
    map['height'] = _height;
    map['width'] = _width;
    map['weight'] = _weight;
    map['size'] = _size;
    map['thickness'] = _thickness;
    map['color'] = _color;
    map['sizedifference'] = _sizedifference;
    map['image1'] = _image1;
    map['image2'] = _image2;
    map['image3'] = _image3;
    map['image4'] = _image4;
    map['image5'] = _image5;
    map['status'] = _status;
    map['batchno'] = _batchno;
    map['shop_id'] = _shopId;
    map['warehouse_id'] = _warehouseId;
    map['distributor_id'] = _distributorId;
    map['make_date'] = _makeDate;
    map['expe_date'] = _expeDate;
    map['qty_stock'] = _qtyStock;
    map['qty_damage'] = _qtyDamage;
    map['qty_rol'] = _qtyRol;
    map['discounted_price'] = _discountedPrice;
    map['isOffer'] = _isOffer;
    map['offer_percent'] = _offerPercent;
    map['isCashBack'] = _isCashBack;
    map['cashback_amount'] = _cashbackAmount;
    map['total_with_gst'] = _totalWithGst;
    map['stock_status'] = _stockStatus;
    map['iswide'] = _iswide;
    map['stock_update_date'] = _stockUpdateDate;
    map['commition_agent_id'] = _commitionAgentId;
    map['added_user_id'] = _addedUserId;
    map['barcode_no'] = _barcodeNo;
    map['created_at'] = _createdAt;
    map['updated_at'] = _updatedAt;
    map['purchase_without_gst'] = _purchaseWithoutGst;
    map['purchase_with_tax'] = _purchaseWithTax;
    map['MRP'] = _mrp;
    map['discount_percent'] = _discountPercent;
    map['cashback_percent'] = _cashbackPercent;
    map['salespri_without_gst'] = _salespriWithoutGst;
    map['salespri_with_gst'] = _salespriWithGst;
    map['reff_income_percent'] = _reffIncomePercent;
    map['reff_gen_status'] = _reffGenStatus;
    map['cashback_gen_status'] = _cashbackGenStatus;
    return map;
  }

}