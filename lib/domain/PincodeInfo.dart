/// status : 1
/// message : "Success."
/// data : {"id":"2","pincode_data":"680121","place":"Irinjalakuda","shopid":"1","status":"1"}

class PincodeInfo {
  PincodeInfo({
     required int status,
    required String message,
    required  Data data,}){
    _status = status;
    _message = message;
    _data = data;
}

  PincodeInfo.fromJson(dynamic json) {
    _status = json['status'];
    _message = json['message'];
    _data = json['data'] ;
  }
  int _status=0;
  String _message="";
 late Data _data;

  int get status => _status;
  String get message => _message;
  Data get data => _data;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['status'] = _status;
    map['message'] = _message;
    if (_data != null) {
      map['data'] = _data.toJson();
    }
    return map;
  }

}

/// id : "2"
/// pincode_data : "680121"
/// place : "Irinjalakuda"
/// shopid : "1"
/// status : "1"

class Data {
  Data({
      required String id,
      required String pincodeData,
      required String place,
      required String shopid,
      required String status,}){
    _id = id;
    _pincodeData = pincodeData;
    _place = place;
    _shopid = shopid;
    _status = status;
}

  Data.fromJson(dynamic json) {
    _id = json['id'];
    _pincodeData = json['pincode_data'];
    _place = json['place'];
    _shopid = json['shopid'];
    _status = json['status'];
  }
  String _id="";
  String _pincodeData="";
  String _place="";
  String _shopid="";
  String _status="";

  String get id => _id;
  String get pincodeData => _pincodeData;
  String get place => _place;
  String get shopid => _shopid;
  String get status => _status;

  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = _id;
    map['pincode_data'] = _pincodeData;
    map['place'] = _place;
    map['shopid'] = _shopid;
    map['status'] = _status;
    return map;
  }

}