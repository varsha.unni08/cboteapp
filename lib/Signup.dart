import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Otp.dart';
import 'package:flutter_app_ecommerce/domain/PincodeInfo.dart';
import 'package:flutter_app_ecommerce/domain/ProfileStatus.dart';
import 'package:flutter_app_ecommerce/login.dart';


import 'constants/DataConstants.dart';
import 'constants/UrlConstants.dart';
import 'design/ResponsiveInfo.dart';
import 'homepage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:url_launcher/url_launcher.dart';


class MySignupPage extends StatefulWidget {
  MySignupPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MySignupPageState createState() => _MySignupPageState();
}

class _MySignupPageState extends State<MySignupPage> {
  TextEditingController mobilenumbercontroller = new TextEditingController();
  TextEditingController passwordcontroller = new TextEditingController();
  TextEditingController emailcontroller = new TextEditingController();
  TextEditingController confirmpasswordmobilecontroller = new TextEditingController();
  TextEditingController namecontroller = new TextEditingController();

  TextEditingController pincodecontroller = new TextEditingController();

  bool checked=false;
  bool ispincodeVerified=false;

  String pincode_id="0";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    Size screenSize = MediaQuery.of(context).size;
    double width=screenSize.width;
    double height=screenSize.height;


    return Scaffold(
      resizeToAvoidBottomInset : false,
      body: Stack(

          children: <Widget>[

            Container(
              width: width,
              height: height/2.2,

              child: Image.asset("images/loginbg.png",width: width,height: height/1.9,fit: BoxFit.fill,),

            ),



            Align(
                alignment: Alignment.center,

                child:  Container(
                    width: width,
                    height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?height:height/1.1:height/1.05,
                    child:

                    Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(15, 0, 15, 0):EdgeInsets.fromLTRB(30, 0, 30, 0),

                        child:Card(

                          shape: RoundedRectangleBorder(
                            borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(15):BorderRadius.circular(30),
                          ),

                          elevation: 10,

                          child: Column(




                            children: [

                              Image.asset("images/kolica.png",width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:80:130,height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:80:130,fit: BoxFit.fill,),

                              Container(



                                child: Padding(
                                  padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15) ,

                                  child:Container(

                                      decoration: BoxDecoration(
                                          border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                                      ),

                                      child: Row(
                                        textDirection: TextDirection.ltr,
                                        children: <Widget>[

                                          Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                                              child: ResponsiveInfo.isMobile(context)? Image.asset("images/idcard.png",width: 25,height: 25,

                                              ):Image.asset("images/idcard.png",width: 45,height: 45,

                                              )
                                          )
                                          ,
                                          Expanded(child: new Theme(data: new ThemeData(
                                              hintColor: Colors.black45
                                          ), child: TextField(


                                            controller: namecontroller,
                                            style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                            decoration: InputDecoration(
                                              border: InputBorder.none,

                                              hintText: 'Name',
                                            ),


                                          )))
                                        ],
                                      )

                                  )

                                  //padding: EdgeInsets.symmetric(horizontal: 15),
                                  ,
                                ),







                              ),


                      Container(



                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15)  ,

                                    child:Container(

                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                                        ),

                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[

                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                                                child: ResponsiveInfo.isMobile(context)? Image.asset("images/pc.png",width: 25,height: 25,

                                                ):Image.asset("images/pc.png",width: 45,height: 45,

                                                )
                                            )
                                            ,
                                            Expanded(child: new Theme(data: new ThemeData(
                                                hintColor: Colors.black45
                                            ), child: TextField(

                                              keyboardType: TextInputType.number,
                                              controller: mobilenumbercontroller,
                                              style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                              decoration: InputDecoration(
                                                border: InputBorder.none,

                                                hintText: 'Phone number',
                                              ),


                                            )))
                                          ],
                                        )

                                    )

                                    //padding: EdgeInsets.symmetric(horizontal: 15),
                                    ,
                                  ),







                                )
,
                                Container(


                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15)  ,

                                    child:Container(

                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                                        ),

                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[

                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                                                child: ResponsiveInfo.isMobile(context)? Image.asset("images/mail.png",width: 25,height: 25,

                                                ):Image.asset("images/mail.png",width: 45,height: 45,

                                                )
                                            )
                                            ,
                                            Expanded(child: new Theme(data: new ThemeData(
                                                hintColor: Colors.black45
                                            ), child: TextField(
                                              controller:emailcontroller ,

                                              keyboardType: TextInputType.emailAddress,
                                              style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                              decoration: InputDecoration(
                                                border: InputBorder.none,

                                                hintText: 'Email(optional)',
                                              ),


                                            )))
                                          ],
                                        )

                                    )

                                    //padding: EdgeInsets.symmetric(horizontal: 15),
                                    ,
                                  ),







                                )
,

                            Container(

                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15)  ,

                                    child:Container(

                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black45,width: ResponsiveInfo.isMobile(context)? 0.5:1.0)
                                        ),

                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[

                                            Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                                                child:

                                                ResponsiveInfo.isMobile(context)? Image.asset("images/padlock.png",width: 25,height: 25,

                                                ):Image.asset("images/padlock.png",width: 45,height: 45,

                                                )
                                            )
                                            ,
                                            Expanded(child: new Theme(data: new ThemeData(
                                                hintColor: Colors.black45
                                            ), child: TextField(
                                              obscureText: true,
                                              controller:passwordcontroller ,
                                              style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),

                                              decoration: InputDecoration(
                                                border: InputBorder.none,

                                                hintText: 'Password',
                                              ),


                                            )))
                                          ],
                                        )

                                    )

                                    //padding: EdgeInsets.symmetric(horizontal: 15),
                                    ,
                                  ),



                                )



                                ,

                          Container(

                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15)  ,

                                    child:Container(

                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black45,width: ResponsiveInfo.isMobile(context)? 0.5:1.0)
                                        ),

                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[

                                            Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                                                child:

                                                ResponsiveInfo.isMobile(context)? Image.asset("images/padlock.png",width: 25,height: 25,

                                                ):Image.asset("images/padlock.png",width: 45,height: 45,

                                                )
                                            )
                                            ,
                                            Expanded(child: new Theme(data: new ThemeData(
                                                hintColor: Colors.black45
                                            ), child: TextField(
                                              obscureText: true,
                                              controller:confirmpasswordmobilecontroller ,
                                              style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),

                                              decoration: InputDecoration(
                                                border: InputBorder.none,

                                                hintText: 'Confirm password',
                                              ),


                                            )))
                                          ],
                                        )

                                    )

                                    //padding: EdgeInsets.symmetric(horizontal: 15),
                                    ,
                                  ),



                                ),

                            Container(



                              child: Row(

                                children: [

                                  Expanded(child:
                                  Padding(
                                    padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 5, 10, 5)    : EdgeInsets.fromLTRB(20, 10, 20, 10) :EdgeInsets.fromLTRB(30, 15, 30, 15)  ,

                                    child:Container(

                                        decoration: BoxDecoration(
                                            border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                                        ),

                                        child: Row(
                                          textDirection: TextDirection.ltr,
                                          children: <Widget>[

                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                                                child:  Icon(Icons.person_pin_circle_outlined, color: Color(0xfff55245),size: ResponsiveInfo.isMobile(context)?25:45,)
                                            )
                                            ,
                                            Expanded(child: new Theme(data: new ThemeData(
                                                hintColor: Colors.black45
                                            ), child: TextField(
    onChanged: (text){

      setState(() {

        ispincodeVerified=false;
      });


    },

                                              keyboardType: TextInputType.number,
                                              controller: pincodecontroller,
                                              style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                              decoration: InputDecoration(
                                                border: InputBorder.none,
                                                hintText: 'Pincode',

                                              ),


                                            )))
                                          ],
                                        )

                                    )

                                    //padding: EdgeInsets.symmetric(horizontal: 15),
                                    ,
                                  ),

                                    flex: 2,

                                  ),
                                  Expanded(child:    Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 8, 10, 10):EdgeInsets.fromLTRB(0, 20, 20, 20),
                                      child:  Container(

                                        width:ResponsiveInfo.isMobile(context)? 120:200,
                                        height:ResponsiveInfo.isMobile(context)? 50:90,
                                        decoration: BoxDecoration(
                                            color: Color(0xfff55245),
                                            border: Border.all(
                                              color: Color(0xfff55245),
                                            ),
                                            borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                                        ),
                                        child: TextButton(onPressed: () {


                                          if(!pincodecontroller.text.toString().isEmpty)
                                            {

                                              confirmPincode(pincodecontroller.text.toString(), 0);


                                            }
                                          else{

                                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                              content: Text("Enter pincode"),
                                            ));


                                          }




                                        },
                                          child: Text("Verify",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                                        ),




                                      )) ,

                                    flex:1

                                  )



                                ],



                              )











                            ),


                              (ispincodeVerified)?   Container(



                              child:  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.only(top: 8.0,left:0.0,right: 20.0,bottom: 0)      : EdgeInsets.only(top: 20.0,left:0.0,right: 40.0,bottom: 0),

                                child: Text("Pincode verified successfully",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?12:16,color: Colors.redAccent),)


                              )

                            ):Container(),



                                


                              Container(



                                  child:  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.only(top: 8.0,left:0.0,right: 20.0,bottom: 0)      : EdgeInsets.only(top: 20.0,left:0.0,right: 40.0,bottom: 0),

                                    child: Align(
                                      alignment: Alignment.center,

                                      child: Row(
                                        mainAxisAlignment: MainAxisAlignment.center ,
                                        crossAxisAlignment: CrossAxisAlignment.center ,

                                        children: [

                                          Checkbox(
                                            value: checked,
                                            onChanged: (bool? value) {
                                              setState(() {
                                                checked = value!;
                                              });
                                            },
                                          ),


                                          Text("I agree ",style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 14,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),),

                                          TextButton( onPressed: ()async {

                                            String url=UrlData.termsurl;

                                            if (await canLaunch(url)) {
                                            await launch(url, forceWebView: true);
                                            } else {
                                            throw 'Could not launch $url';
                                            }


                                          },
                                            child: Text("terms and conditions",style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 14,color: Colors.blueAccent):TextStyle(fontSize: 22,color: Colors.blueAccent)


                                            ),)

                                        ]






                                      ),
                                    ),)),



                              Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 8, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                                  child:  Container(

                                    width:ResponsiveInfo.isMobile(context)? 120:200,
                                    height:ResponsiveInfo.isMobile(context)? 50:90,
                                    decoration: BoxDecoration(
                                        color: Color(0xfff55245),
                                        border: Border.all(
                                          color: Color(0xfff55245),
                                        ),
                                        borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                                    ),
                                    child: TextButton(onPressed: () {

                                      if(checked) {
                                        checkMobileNumber(
                                            mobilenumbercontroller.text);
                                      }
                                      else{

                                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                          content: Text("Please check terms and conditions "),
                                        ));

                                      }




                                    },
                                      child: Text("Sign up",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                                    ),




                                  )),


                              Row(

                                mainAxisAlignment: MainAxisAlignment.center ,
                                crossAxisAlignment: CrossAxisAlignment.center ,

                                children: [

                                  Text("Already a member ? ",style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 14,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),),

                                  TextButton( onPressed: () {

                                    Navigator.pushReplacement(context, MaterialPageRoute(
                                        builder: (context) => MyLoginPage(title: "Sign in",)
                                    )
                                    );


                                  },
                                    child: Text("Sign in",style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 14,color: Colors.blueAccent):TextStyle(fontSize: 22,color: Colors.blueAccent)


                                    ),)


                                ],



                              )







                            ],



                          ),





                        )


                    )



                ))











            ,
          ] ),







    );



  }

  confirmPincode(String pincode,int code) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
      Uri.parse(UrlData.baseurl+UrlData.checkUserPincode+"/"+pincode),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',


      },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);



    int status = jsondata['status'];

   // String responsedata=jsondata['data'];

    dynamic pincodejsondata=jsondata['data'];

    if(status==1)
    {

    //  dynamic pincodejsondata = jsonDecode(responsedata);
       pincode_id=pincodejsondata['id'];



      setState(() {
        ispincodeVerified=true;
      });

      //checkData();

      if(code==1) {
        checkData();
      }
      else{


      }


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("pincode verification is success"),
      ));



    }
    else{

      setState(() {
        ispincodeVerified=false;
      });


      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {

          Navigator.pop(context);

        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Kolica"),
        content: Text("pincode verification is failed , we cannot deliver to this area"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("pincode verification is failed , we cannot deliver to this area"),
      ));
    }
  }


  checkData()
  {

    if(namecontroller.text.isNotEmpty) {
      if (mobilenumbercontroller.text.isNotEmpty) {
        if (passwordcontroller.text.isNotEmpty) {
          if (confirmpasswordmobilecontroller.text.isNotEmpty) {
            if (passwordcontroller.text.toString().compareTo(
                confirmpasswordmobilecontroller.text.toString()) == 0) {
              if (passwordcontroller.text
                  .toString()
                  .length >= 8) {
                if (checked) {
                  Navigator.pushReplacement(context, MaterialPageRoute(
                      builder: (context) =>
                          Otppage(title: "Otp",
                            email: emailcontroller.text.toString(),
                            mobile: mobilenumbercontroller.text,
                            password: passwordcontroller.text, name: namecontroller.text, pincode_id: pincode_id,)
                  )
                  );
                }
                else {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Please agree our terms and conditions"),
                  ));
                }
              }
              else {
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Your password must have atleast 8 characters"),
                ));
              }
            }

            else {
              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("Password confirmation failed"),
              ));
            }
          }
          else {
            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Confirm your password"),
            ));
          }
        }
        else {
          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter password"),
          ));
        }
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Please enter mobile number"),
        ));
      }
    }
    else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Please enter name"),
      ));
    }



  }


  void checkMobileNumber(String mobile) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");


    var dataasync = await http.get(
        Uri.parse(
            UrlData.baseurl + UrlData.mobilenumberexists + "/" + mobile),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }
    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    int status = jsondata['status'];

    String message = jsondata['message'];

    if(status==1)
    {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Your mobile number is already exists"),
      ));

    }
    else{

      confirmPincode(pincodecontroller.text.toString(), 1);




    }




  }
}