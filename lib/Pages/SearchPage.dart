import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Welcomepage.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/SearchedByWordData.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:share/share.dart';

import 'ProductDetails.dart';





class SearchPage extends StatefulWidget {
  SearchPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  String link = "";

  List<Searchdata>srd=[];


  @override
  void initState() {
    // TODO: implement initState



    super.initState();
  }


  @override
  Widget build(BuildContext context) {

    return Scaffold(

      body: Stack(

        children: [





          Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,30,10,10):EdgeInsets.fromLTRB(35,45,15,15):EdgeInsets.fromLTRB(50,70,20,20),

            child:Card(

              elevation: 8,
              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 0),
                borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
              ),

              child:  Container(




                child:Container(






                      child:  Row(
                        textDirection: TextDirection.ltr,
                        children: <Widget>[

                          Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                              child:

                              ResponsiveInfo.isMobile(context)? Image.asset("images/search.png",width: 25,height: 25,

                              ):Image.asset("images/search.png",width: 45,height: 45,

                              )
                          )
                          ,
                          Expanded(child: new Theme(data: new ThemeData(
                              hintColor: Colors.black45
                          ), child:TextField(
                            
                            onChanged: (text){
                              
                              getSearchDataByWord(text);
                            },

                            decoration: InputDecoration(
                              border: InputBorder.none,
                              focusedBorder: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              errorBorder: InputBorder.none,
                              disabledBorder: InputBorder.none,

                              hintText: 'Search ...',

                            ),


                          )))
                        ],
                      )



                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  ,




                )



                ,),


            ) ,



          ),

    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(25,80,10,10):EdgeInsets.fromLTRB(35,120,15,15):EdgeInsets.fromLTRB(50,170,20,20),

    child:ListView.builder(
        padding: const EdgeInsets.all(8),
        itemCount: srd.length,
        itemBuilder: (BuildContext context, int index) {
          return InkWell(child : Container(
            height:ResponsiveInfo.isMobile(context)? 50:80,
           
            child: Container(child: Text(srd[index].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?14:19),)),
          ),

          onTap: (){

            Navigator.push(
                context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: srd[index].productids, stockid: srd[index].stockid,)));


          },



          );
        }
    ))

        ],




      )










    );


  }


  getSearchDataByWord(String word) async
  {

    if(word.isEmpty)
      {
        setState(() {

          srd.clear();


        });

      }
    else {
      final datacount = await SharedPreferences.getInstance();
      var date = new DateTime.now().toIso8601String();
      var dataasync = await http.post(
        Uri.parse(UrlData.baseurl + UrlData.searchProducts + "/" + word),
        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
        },
      );
      String response = dataasync.body;
      dynamic jsondata = jsonDecode(response);
      SearchedByWordData swd = SearchedByWordData.fromJson(jsondata);
      if (swd.status == 1) {
        srd.clear();
        if (swd.searchdata.length > 0) {
          setState(() {

            srd.addAll(swd.searchdata);
          });


        }
        else{

          setState(() {
            srd.clear();
          });
        }
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Failed"),
        ));
      }
    }
    return srd;
  }

}
