import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter_app_ecommerce/Pages/FilterPage.dart';
import 'package:flutter_app_ecommerce/Pages/OrderDetails.dart';
import 'package:flutter_app_ecommerce/Pages/ProductDetails.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/CategoryDetails.dart';
import 'package:flutter_app_ecommerce/domain/ProductDatabyCategory.dart';
import 'package:flutter_app_ecommerce/domain/SubcategoryList.dart';

import 'package:geocoding_platform_interface/src/models/placemark.dart' as Placemark;

import 'package:geolocator/geolocator.dart';
// or whatever name you want

import 'package:geocoding/geocoding.dart';

import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

import 'SearchPage.dart';

class ProductlistPage extends StatefulWidget {
  final String title;
  final String categoryid;



  const ProductlistPage({Key? key, required this.title, required this.categoryid}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProductlistPage(categoryid);
}

class _ProductlistPage extends State<ProductlistPage> {

  bool _switchValue=false;

  String categoryid="";

  List<Data>prdata=[];
  String lat="0";
  String longi="0";
  bool _fromTop = true;

  List<Subcategorydata> subcategorydata=[];

  String mylocation="Select location to see product availability";

  _ProductlistPage(this.categoryid);

  int filtercount=0;

 String  cddata="";

  @override
  void initState() {
    // TODO: implement initState
    getCategoryData(categoryid);

getProductList(categoryid);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;
    double height = screenSize.height;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:
        PreferredSize(
        preferredSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?Size.fromHeight(60.0):Size.fromHeight(80.0): Size.fromHeight(100.0),
    child:


        AppBar(

          backgroundColor: Color(0xffff5545),
   flexibleSpace: Container(

     color: Color(0xffff5545),
      child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?EdgeInsets.fromLTRB(60, 35, 50, 10) : EdgeInsets.fromLTRB(60, 40, 50, 10): EdgeInsets.fromLTRB(60, 60, 50, 10),

         child:Row(
           crossAxisAlignment: CrossAxisAlignment.center,
           mainAxisAlignment: MainAxisAlignment.center,
           
           children: [
             
             Expanded(child: Container(


                 // decoration: BoxDecoration(
                 //     border: Border.all(color: Colors.black45,width: ResponsiveInfo.isMobile(context)? 0.5:1.0)
                 // ),
                 height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?40:50:80,
                 decoration: BoxDecoration(
                   borderRadius: BorderRadius.circular(10),
                   color: Colors.white,
                   boxShadow: [
                     BoxShadow(color: Colors.white, spreadRadius: 3),
                   ],
                 ),

                 child:
                 GestureDetector(
                     onTap: (){

                       Navigator.push(
                           context, MaterialPageRoute(builder: (_) => SearchPage(title: "Cart",)));


                     },

                     child:

                     Row(
                       textDirection: TextDirection.ltr,
                       crossAxisAlignment: CrossAxisAlignment.center,
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[

                         Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                             child:

                             ResponsiveInfo.isMobile(context)? Image.asset("images/search.png",width: 25,height: 25,

                             ):Image.asset("images/search.png",width: 45,height: 45,

                             )
                         )
                         ,
                         Expanded(child: new Theme(data: new ThemeData(
                             hintColor: Colors.black45
                         ), child: Text("Search ...",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?14:19 ))))
                       ],
                     )
                 )




             ),flex: 3,),
             
             Expanded(child:    GestureDetector(

               child: Stack(children: [

                 Center(child:

                 Icon(Icons.filter_list,color: Colors.white,size:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:28: 28)
                 ),

                 (filtercount>0)?    Align(

                     alignment: Alignment.topRight,

                     child: Padding(
                         padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                         child:Container(
                           width: ResponsiveInfo.isMobile(context)?20:40,
                           height: ResponsiveInfo.isMobile(context)?20:40,

                           decoration: BoxDecoration(
                               color: Color(0xfff55245),
                               shape: BoxShape.circle
                           ),

                           child: Center(

                             child:

                             (filtercount>0)? Text(filtercount.toString()+"",style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 12,color: Colors.white):TextStyle(fontSize: 19,color: Colors.white)):Container(),



                           ),
                         ))

                 ):Container()





               ],),
               onTap: () async{

              List<Subcategorydata>lsb=await   getSubcategories();

              if(lsb.length>0)
                {

                  showGeneralDialog(
                    barrierLabel: "Label",
                    barrierDismissible: true,
                    barrierColor: Colors.black.withOpacity(0.5),
                    transitionDuration: Duration(milliseconds: 700),
                    context: context,
                    pageBuilder: (context, anim1, anim2) {
                      return Align(
                        alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
                        child: Container(
                          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?200:250:300,
                          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?350:390:450,

                          child: ListView.builder(
                              itemCount: lsb.length,
                              itemBuilder: (BuildContext context,int index){
                                return



                                   Card(child:InkWell(
                                onTap: (){

                                  Navigator.pop(context);

                                 getFilteredProducts(lsb[index].id);
                                },child: Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),

                                      child:   Text(lsb[index].subCatname,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?14:18,color: Colors.black),),

                                    ) ,
                                  )



                              ,);
                              }
                          ),
                          margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(10),
                          ),
                        ),
                      );
                    },
                    transitionBuilder: (context, anim1, anim2, child) {
                      return SlideTransition(
                        position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                        child: child,
                      );
                    },
                  );



                }
              else{
                ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                  content: Text("Error"),
                ));

              }

                 // Map results = await Navigator.of(context)
                 //     .push(new MaterialPageRoute<dynamic>(
                 //   builder: (BuildContext context) {
                 //     return new FilterPage(
                 //       title: "Filter", categoryid: categoryid,
                 //     );
                 //   },
                 // ));
                 //
                 // if (results != null &&
                 //     results.containsKey('filterparameter')) {
                 //
                 //
                 //   var acc_selected =
                 //   results['filterparameter'];
                 //
                 //   var fcount =
                 //   results['fcount'];
                 //
                 //   if(fcount>0)
                 //   {
                 //     setState(() {
                 //
                 //       filtercount=fcount;
                 //     });
                 //   }
                 //
                 //
                 //   print(acc_selected);
                 //
                 //   getFilteredProducts(acc_selected);

                // }


                 // Navigator.push(
                 //     context, MaterialPageRoute(builder: (_) => FilterPage(title: "Filter",)));

               },
             ),flex: 1,)
           ],
           
         )
         
         
         
         ,


       )



   ),
          elevation: 0,
        )),
        body: Stack(
          children: [
          Container(
          width: double.infinity,
            height: double.infinity,
            color: Color(0xffEFECEC),

          ),
            



          Container(
            width: double.infinity,
            height: ResponsiveInfo.isMobile(context)? 100:180,

            alignment: Alignment.topCenter, color: Color(0xffefebeb),child:  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(20) : EdgeInsets.all(40),
              child:Text(cddata,style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 15,color: Colors.black):TextStyle(fontSize: 22,color: Colors.black))),





    ),




        Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 40, 10, 10):EdgeInsets.fromLTRB(20, 60, 20, 20):EdgeInsets.fromLTRB(30, 90, 30, 30),

          child: ListView.builder(

              itemCount: prdata.length,
              itemBuilder: (BuildContext context, int index) {
                return GestureDetector(

                  child:Card(

                      elevation: 5,

                      shape: RoundedRectangleBorder(
                        side: BorderSide(color: Colors.white70, width: 0),
                        borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                      ),

                      child:    Container(
                        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?160:190:230,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [



                            Row(

                              children: [

                                (prdata[index].image1!=null)?  Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(18),

                                  child: Container(child:Image.network(UrlData.productimageurl+prdata[index].image1,
                                    width:ResponsiveInfo.isMobile(context)?70:90 ,height: ResponsiveInfo.isMobile(context)?70:90,
                                    errorBuilder:
                                        (BuildContext context, Object exception, StackTrace? stackTrace) {

                                      return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?70:90 ,height: ResponsiveInfo.isMobile(context)?70:90,
                                      );
                                    },
                                  )),

                                ):Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(18),

                                  child: Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?70:90 ,height: ResponsiveInfo.isMobile(context)?70:90,
                                  ),
                                )
                            ,

                                Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 0):EdgeInsets.fromLTRB(0, 16, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0),

                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.center,
                                      mainAxisAlignment: MainAxisAlignment.center,

                                      children: [

                                        SizedBox(


                                          child:  Text(prdata[index].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),
                                            width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?130:170:200 ,
                                        ),


                                        (double.parse(prdata[index].discountPercent)>0)?  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                            child:Row(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [

                                               Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),

                                                  child: Text("₹ "+prdata[index].mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),

                                                  )
                                                  ,)
                                                ,
                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),

                                                  child: Text("₹ "+prdata[index].salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                                  )
                                                  ,)
                                              ],


                                            )
                                        ):Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),

                                          child: Text("₹ "+prdata[index].mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                          )
                                          ,),




                                      ],


                                    )


                                )






                              ],


                            ),







                          ],


                        ),




                      )

                  ) ,

                  onTap: (){
                    Navigator.push(
                        context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: prdata[index].productid, stockid: prdata[index].stockid,)));
                  },
                )







                ;



              }
          ),



        )









          ],







        ));

  }

  getCategoryData(String catid) async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getCategoryDetails+"/"+catid),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;
    dynamic jsondata = jsonDecode(response);

    CategoryDetails cds=CategoryDetails.fromJson(jsondata);
    if(cds.status==1)
      {

        if(cds.categorydata.length>0)
          {

            cddata=cds.categorydata;
          }


      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }



  }


getProductList(String catid) async
{
  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

  final datacount = await SharedPreferences.getInstance();
  var date = new DateTime.now().toIso8601String();
  var dataasync = await http.post(
    Uri.parse(UrlData.baseurl + UrlData.productbyCategory+"/"+catid),
    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
    },
  );
  _progressDialog.dismissProgressDialog(context);
  String response = dataasync.body;

  print(response);
  dynamic jsondata = jsonDecode(response);
  ProductDatabyCategory prd=ProductDatabyCategory.fromJson(jsondata);

  if(prd.status==1)
    {

      if(prd.data.length>0)
        {


          prdata.clear();

          setState(() {

            prdata.addAll(prd.data);


          });


        }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("No data found"),
        ));

      }



    }
  else{

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Failed"),
    ));


  }




}

  fetchLocation() async
  {

    bool  serviceEnabled = await  Geolocator().isLocationServiceEnabled();





    if (!serviceEnabled) {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Location disabled"),
      ));

    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Location enabled"),
      ));

      var  loc = await  Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      print(loc);

      lat=loc.latitude.toString();
      longi=loc.longitude.toString();

      List<Placemark.Placemark> placemarks = await placemarkFromCoordinates(double.parse(lat), double.parse(longi)) as List<Placemark.Placemark>;


      if(placemarks.length>0)
      {

        Placemark.Placemark placeMark  = placemarks[0];

        String name = placeMark.name!;
        String subLocality = placeMark.subLocality!;
        String locality = placeMark.locality!;
        String administrativeArea = placeMark.administrativeArea!;
        String postalCode = placeMark.postalCode!;
        String country = placeMark.country!;

        String data="";
        if(name!=null||name.isNotEmpty)
        {
          data=data+name+",";
        }
        if(subLocality!=null||subLocality.isNotEmpty)
        {
          data=data+subLocality+",";
        }
        if(locality!=null||locality.isNotEmpty)
        {
          data=data+locality+",";
        }




        setState(() {
          mylocation=data;
        });

        getProductbylocation();

        // print(name+"\n"+subLocality+"\n"+locality+"\n"+administrativeArea+"\n"+postalCode+"\n"+country);
      }






    }
  }

  getFilteredProducts(String filterdata) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getFilterProducts+"/"+filterdata),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },
        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'param':filterdata


        }


    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(jsondata);

    ProductDatabyCategory prd=ProductDatabyCategory.fromJson(jsondata);

    if(prd.status==1)
    {

      if(prd.data.length>0)
      {

        prdata.clear();

        setState(() {

          prdata.addAll(prd.data);
        });


      }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("No data found"),
        ));

        // setState(() {
        //
        //   prdata.clear();
        // });
      }



    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));

    }

  }




  getProductbylocation() async
  {

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getProductAccordingToLocation+"/"+lat+"/"+longi),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },


    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    ProductDatabyCategory prd=ProductDatabyCategory.fromJson(jsondata);

    if(prd.status==1)
      {

        if(prd.data.length>0)
          {

            prdata.clear();

            setState(() {

              prdata.addAll(prd.data);
            });


          }
        else{

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Failed"),
          ));

          setState(() {

            prdata.clear();
          });
        }



      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));

    }
  }


  Future<List<Subcategorydata>> getSubcategories() async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(UrlData.baseurl + UrlData.getSubcategories+"/"+categoryid),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    SubcategoryList sub=SubcategoryList.fromJson(jsonDecode(response));

    if(sub.status==1)
    {

      subcategorydata.clear();
      subcategorydata.addAll(sub.subcategorydata);


    }


    return subcategorydata;

  }

}


