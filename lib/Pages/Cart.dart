import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/MyAddress.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/CartProduct.dart';
import 'package:flutter_app_ecommerce/domain/TotalAmount.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class CartPage extends StatefulWidget {
  final String title;

  const CartPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _CartPage();
}

class _CartPage extends State<CartPage> {
  bool _switchValue = false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;

  double amounttopay=0;

  double deliverycharge=0;

  double totalamount=0;


  List<Data>lcart=[];

  @override
  void initState() {
    // TODO: implement initState

    getCart();
    calculateAmount();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      resizeToAvoidBottomInset: true,
      appBar: AppBar(
        backgroundColor: Color(0xfff55245),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text(
          "Cart",
          style: TextStyle(
              fontSize: ResponsiveInfo.isMobile(context)
                  ? ResponsiveInfo.isSmallMobile(context)
                  ? 12
                  : 16
                  : 20,
              color: Colors.white),
        ),
        centerTitle: false,
      ),
        body:

        Column(

          children: [

            Flexible(child:  Stack(
              children: [
                Container(
                  width: double.infinity,
                  height: double.infinity,
                  color: Color(0xffEFECEC),

                ),

                (lcart.length>0)?  ListView.builder(
                    itemCount: lcart.length,
                    itemBuilder: (BuildContext context,int i){

                      double d=0;
                      double total=0;
                      // if(double.parse(lcart[i].offerPercent)>0)
                      //   {
                          d=double.parse(lcart[i].salespriWithGst);

                      //   }
                      // else{
                      //   d=double.parse(lcart[i].mrp);
                      //
                      // }
                      int q=int.parse(lcart[i].quantity);
                      total=d*q;
                      total=total.roundToDouble();




                      return Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5): EdgeInsets.all(8): EdgeInsets.all(15),
                          child:  Card(
                              elevation: 5,
                              shape: RoundedRectangleBorder(
                                side: BorderSide(color: Colors.white70, width: 0),
                                borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                              ),
                              child:    Container(
                                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?240:280:340,
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Row(
                                      children: [
                                        Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(10):EdgeInsets.all(16):EdgeInsets.all(22),



                                          child: (lcart[i].image1!=null&&lcart[i].image1.isNotEmpty)?



                                          ClipOval(child:Image.network(UrlData.productimageurl+lcart[i].image1,
                                            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,fit: BoxFit.fill,
                                            errorBuilder:
                                                (BuildContext context, Object exception, StackTrace? stackTrace) {
                                              return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                                              );
                                            },
                                          )):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                                          ),),
                                        Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 0):EdgeInsets.fromLTRB(0, 16, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.center,
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [

                                                SizedBox(


                                                  child:  Text(lcart[i].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),
                                                  width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?130:170:200 ,
                                                ),

                                                // ( double.parse(lcart[i].discountPercent)>0)? Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                //   child: Text("Price : ₹ "+lcart[i].salespriWithGst,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black54),),
                                                // ):
                                                Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                  child: Text("Price : ₹ "+lcart[i].salespriWithGst,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black54),),
                                                ),
                                                Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                    child:Row(
                                                      crossAxisAlignment: CrossAxisAlignment.center,
                                                      mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        Padding(padding: EdgeInsets.all(5),
                                                          child: Text("Total Amount : ₹ "+total.toString(),style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),
                                                          )
                                                          ,)
                                                      ],
                                                    )
                                                ),
                                                Container(
                                                  decoration: BoxDecoration(
                                                      border: Border.all(
                                                        color: Color(0xff3d3b3a),
                                                      ),
                                                      borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?15:30))
                                                  ),
                                                  child: Row(
                                                    children: [
                                                      TextButton(onPressed: () {

                                                        int q=int.parse(lcart[i].quantity);

                                                        if(q>1) {
                                                          int a = q - 1;

                                                          setState(() {
                                                            lcart[i].setquantity=a.toString();
                                                          });


                                                          // changeQty(i,a);
                                                          checkStockAvailability(int.parse(lcart[i].stockid), a, i,int.parse(lcart[i].cartid),lcart[i].shopId,lcart[i].productid);
                                                        }


                                                      },
                                                        child: Text("-",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),
                                                      ),
                                                      Text(lcart[i].quantity.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Color(0xfff55245)),),
                                                      TextButton(onPressed: () {
                                                        int q=int.parse(lcart[i].quantity);
                                                        int a = q + 1;
                                                        setState(() {
                                                          lcart[i].setquantity=a.toString();
                                                        });
                                                       // changeQty(i,a);
                                                        checkStockAvailability(int.parse(lcart[i].stockid), a, i,int.parse(lcart[i].cartid),lcart[i].shopId,lcart[i].productid);

                                                      },
                                                        child: Text("+",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),
                                                      ),
                                                    ],
                                                  ),
                                                )
                                              ],
                                            )
                                        )
                                      ],
                                    ),
                                    Align(
                                      alignment: Alignment.bottomRight,
                                      child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 0, 10, 0):EdgeInsets.fromLTRB(0, 0, 20, 0):EdgeInsets.fromLTRB(0, 0, 25, 0),
                                          child:TextButton(onPressed: () {

                                            Widget okButton = TextButton(
                                              child: Text("Yes"),
                                              onPressed: () {

                                                Navigator.pop(context);
                                                removeFromCart(lcart[i].cartid, i);

                                              },
                                            );

                                            Widget nopButton = TextButton(
                                              child: Text("No"),
                                              onPressed: () {

                                                Navigator.pop(context);
                                              },
                                            );

                                            // set up the AlertDialog
                                            AlertDialog alert = AlertDialog(
                                              title: Text("Kolica"),
                                              content: Text("Do you want to delete this product from cart ?"),
                                              actions: [
                                                okButton,
                                                nopButton
                                              ],
                                            );

                                            // show the dialog
                                            showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return alert;
                                              },
                                            );





                                          }, child: Text("Delete", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),
                                          )
                                      )
                                      ,
                                    )
                                  ],
                                ),
                              )
                          ));
                    }
                )
                :
                Align(
                  alignment: Alignment.center,

                  child: Text("No data found", style: TextStyle(color: Colors.black,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),
                ),












              ],







            ),flex: 4,),

            Flexible(child:(lcart.length>0)?Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: [

                Column(
                  children: [
                    Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),


                      child:Text("Total Amount : ₹ "+totalamount.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:19,color: Colors.black26),)





                      ,


                    ),
                    Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),


                      child:Text("Delivery Charge : ₹ "+deliverycharge.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:19,color: Colors.black26),)





                      ,


                    ),
                    Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),


                      child:Text("Amount to pay : ₹ "+amounttopay.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:18:22,color: Colors.black),)





                      ,


                    ),

                  ],

                )

              ,



                Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 25, 5, 20):EdgeInsets.fromLTRB(15, 35, 10, 30):EdgeInsets.fromLTRB(20, 50, 15, 40),

                    child:GestureDetector(

    child:  Container(
      height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 50:70:90,
      width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 100:130:170,
      decoration: BoxDecoration(
          color: Color(0xfff55245),

          border: Border.all(
            color: Color(0xfff55245),
          ),
          borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?15:30))


      ),
      child: Center(child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,

        children: [
          Text("Checkout" , style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?15:20,color: Colors.white ),),
          Container(
            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?25:35:50,
            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?25:35:50,

            decoration: BoxDecoration(
                color: Color(0xffffffff),
                shape: BoxShape.circle
            ),

            child: Center(

              child: Text(lcart.length.toString(),style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 12,color: Colors.black):TextStyle(fontSize: 19,color: Colors.black)),



            ),
          )


        ],


      )



      ),


    ),
                      onTap: (){
                        Navigator.push(
                            context, MaterialPageRoute(builder: (_) => MyAddressPage(title: "Cart", cartid: '0', code: '0',)));

                      },



    )





                )





              ],
            ):Container()



                ,flex: 1),




          ],

        )



    );

  }

  removeFromCart(String cartid,int index)async{

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");

    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.deleteFromCart),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
       UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!
      },
      body: {
        'cartid':cartid


      }



    );


    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);
    if(jsondata['status']==1)
    {

calculateAmount();
      setState(() {

        lcart.removeAt(index);


      });




    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }

  }






  getCart() async {
    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl + UrlData.getCart),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
        },



    );


   // _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    CartProduct crp=CartProduct.fromJson(jsondata);

    if(crp.status==1)
      {

        List<Data>ld=crp.data;

        setState(() {
          lcart.clear();

          lcart.addAll(crp.data);

        });






      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }





  }

  calculateAmount()
  async {

    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");
    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.calculateAmount),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },



    );

  //  _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    TotalAmount  tm=TotalAmount.fromJson(jsondata);

    if(tm.status==1)
    {


      setState(() {

        totalamount=double.parse(tm.totalamount);
        deliverycharge=double.parse(tm.deliverycharge);

        double am=totalamount+deliverycharge;
        amounttopay=am.roundToDouble();


      });






    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }





  }

  changeQty(int index,int qty,int stockid,int cartid) async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");

    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.updateCart),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },
      body: {

        'stockid':stockid.toString(),
        'quantity':qty.toString(),
        'cartid':cartid.toString()


      }


    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {



getCart();

calculateAmount();


    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }

  }


  checkStockAvailability(int stockid,int qty,int index,int cartid,String shopid,String productid) async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.checkStockAvailability + "/" + stockid.toString() + "/" +
          qty.toString()+"/"+shopid+"/"+productid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );

    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {

    changeQty(index, qty,stockid,cartid);

    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }


  }


}