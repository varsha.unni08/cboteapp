import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/placeOrder.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/Pincode.dart';
import 'package:flutter_app_ecommerce/domain/address_by_id.dart';
import 'package:flutter_app_ecommerce/domain/user_address.dart';
import 'package:flutter_xlider/flutter_xlider.dart';
import 'package:geocoding_platform_interface/src/models/placemark.dart' as Placemark;

import 'package:geolocator/geolocator.dart';
// or whatever name you want

import 'package:geocoding/geocoding.dart';


import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class MyNewAddressPage extends StatefulWidget {
  final String title;
  final String id;

  const MyNewAddressPage({Key? key, required this.title, required this.id}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyNewAddressPage(id);
}

class _MyNewAddressPage extends State<MyNewAddressPage> {

  String id="";


  _MyNewAddressPage(this.id);

  bool _switchValue = false;
  bool ispincodeVerified=false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;

  bool _value = false;
  int val = -1;

  String languagedropdown='Select your address type';

  String mylocation="Select my location (optional )";

  static final List<String> addresstype = ['Select your address type','Office','Home'];


  TextEditingController namecontroller=new TextEditingController();
  TextEditingController mobilecontroller=new TextEditingController();
  TextEditingController pincodecontroller=new TextEditingController();

  TextEditingController flatcontroller=new TextEditingController();
  TextEditingController areacontroller=new TextEditingController();
  TextEditingController landmarkcontroller=new TextEditingController();



  TextEditingController towncontroller=new TextEditingController();

  String lat="0";
  String longi="0";

  String buttontext="Add";




  @override
  void initState() {
    // TODO: implement initState

    if(id.compareTo("0")!=0)
      {
        getAddressById(id);
      }



    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "Add Address",
            style: TextStyle(
                fontSize: ResponsiveInfo.isMobile(context)
                    ? ResponsiveInfo.isSmallMobile(context)
                    ? 12
                    : 16
                    : 20,
                color: Colors.white),
          ),
          centerTitle: false,
        ),
        body:Column(

            children: [

              Flexible(child:  Stack(
                  children: [

                    SingleChildScrollView(

                      child: Column(

                        children: [

                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(




                              controller: namecontroller,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: 'Full name',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(


                              controller: mobilecontroller,

                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: '10 digit mobile number without prefix',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),




                               Padding(
                                padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                                // padding: EdgeInsets.all(15),
                                child: new Theme(data: new ThemeData(
                                  hintColor: Colors.black54,

                                ), child: TextField(



                                  controller: pincodecontroller,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                    ),
                                    hintText: '6 digits [0-9] pin code',



                                  ),

                                  onChanged: (text) {


                                  },


                                )),
                              )


                           ,


                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(



                              controller: flatcontroller,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: 'Flat no./House no./Building',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(



                              controller: areacontroller,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: 'Area/Street',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(



                              controller: landmarkcontroller,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: 'Landmark',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),

                          Padding(
                            padding: const EdgeInsets.only(left:15.0,right: 15.0,top:10,bottom: 0),
                            // padding: EdgeInsets.all(15),
                            child: new Theme(data: new ThemeData(
                              hintColor: Colors.black54,

                            ), child: TextField(




                              controller: towncontroller,
                              decoration: InputDecoration(
                                focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                enabledBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: Colors.black54, width: 0.5),
                                ),
                                hintText: 'Town/city',



                              ),

                              onChanged: (text) {


                              },


                            )),
                          ),


                          Padding(
                            padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10)    : EdgeInsets.all(15) :EdgeInsets.all(30) ,

                            child:Container(

                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black45,width: ResponsiveInfo.isMobile(context)? 0.5:1.0)
                                ),

                                child: Row(
                                  textDirection: TextDirection.rtl,
                                  children: <Widget>[

                                    GestureDetector(


                                      child: Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                                          child:

                                          ResponsiveInfo.isMobile(context)? Image.asset("images/locationpick.png",width: 25,height: 25,

                                          ):Image.asset("images/locationpick.png",width: 45,height: 45,

                                          )
                                      ),

                                      onTap: (){

                                        fetchLocation();

                                      },
                                    )


                                    ,
                                    Expanded(child: new Theme(data: new ThemeData(
                                        hintColor: Colors.black45
                                    ), child: TextButton(onPressed: () {

                                      fetchLocation();


                                    },
                                    child: Text(mylocation,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.blue)),)))
                                  ],
                                )

                            )

                            //padding: EdgeInsets.symmetric(horizontal: 15),
                            ,
                          ),

                          Padding(
                              padding: const EdgeInsets.all(15),
                              child:Container(
                                width: double.infinity,
                                height: 60.0,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                    color: Colors.white,
                                    // red as border color
                                  ),
                                ),
                                child: DropdownButtonHideUnderline(
                                  child: ButtonTheme(
                                    alignedDropdown: true,
                                    child: InputDecorator(
                                      decoration: const InputDecoration(border: OutlineInputBorder()),
                                      child: DropdownButtonHideUnderline(
                                        child: DropdownButton(

                                          value: languagedropdown,
                                          items: addresstype
                                              .map<DropdownMenuItem<String>>((String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                          onChanged: (String? newValue) {
                                            setState(() {
                                              languagedropdown = newValue!;


                                            });
                                          },
                                          style: Theme.of(context).textTheme.bodyText1,

                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )),

                        ],


                      ),



                    )




                  ]

              ),flex: 4,),

              Flexible(child: Container(

                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 120.0:150:200,

                child:   Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                    child:  Container(

                      width:ResponsiveInfo.isMobile(context)? 150.0:180,
                      height:ResponsiveInfo.isMobile(context)? 50.0:100,
                      decoration: BoxDecoration(
                          color: Color(0xfff55245),
                          border: Border.all(
                            color: Color(0xfff55245),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                      ),
                      child: TextButton(onPressed: () {



                        checkPincode(pincodecontroller.text.toString().trim());



                        //checkData();




                      },
                        child: Text(buttontext,style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                      ),




                    )) ,





              ),flex: 1,)





            ])





    );

  }

  checkPincode(String pincode) async{


    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
        Uri.parse(UrlData.baseurl+UrlData.checkPincode+"/"+pincode),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token: datastorage.getString(DataConstants.tokenkey)!

        },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    int status = jsondata['status'];

    if(status==1)
      {

       checkData();

ispincodeVerified=true;
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("pincode verification is failed , we cannot deliver to this area"),
      ));
    }



  }



  getAddressById(String id)async
  {

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getAddressById),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
         UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'addressid':id

        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    UserAddress addressById=UserAddress.fromJson(jsondata);


    if(addressById.status==1)
      {



        setState(() async{

          namecontroller.text=addressById.address.name;
          mobilecontroller.text=addressById.address.phone;
          pincodecontroller.text=addressById.address.pincode;
          flatcontroller.text=addressById.address.houseno;
          landmarkcontroller.text=addressById.address.landmark;
          areacontroller.text=addressById.address.area;
          towncontroller.text=addressById.address.town;
          lat=addressById.address.latitude;
          longi=addressById.address.longitude;

          languagedropdown=addressById.address.addresstype;

          buttontext="Edit";

          if(lat.compareTo("0")!=0&&longi.compareTo("0")!=0)
            {

              List<Placemark.Placemark> placemarks = await placemarkFromCoordinates(double.parse(lat), double.parse(longi)) as List<Placemark.Placemark>;


              if(placemarks.length>0)
              {

                Placemark.Placemark placeMark  = placemarks[0];

                String name = placeMark.name!;
                String subLocality = placeMark.subLocality!;
                String locality = placeMark.locality!;
                String administrativeArea = placeMark.administrativeArea!;
                String postalCode = placeMark.postalCode!;
                String country = placeMark.country!;

                String data="";
                if(name!=null||name.isNotEmpty)
                {
                  data=data+name+",";
                }
                if(subLocality!=null||subLocality.isNotEmpty)
                {
                  data=data+subLocality+",";
                }
                if(locality!=null||locality.isNotEmpty)
                {
                  data=data+locality+",";
                }
                if(administrativeArea!=null||administrativeArea.isNotEmpty)
                {
                  data=data+administrativeArea+",";
                }

                if(postalCode!=null||postalCode.isNotEmpty)
                {
                  data=data+postalCode+",";
                }

                if(country!=null||country.isNotEmpty)
                {
                  data=data+country+"";
                }

                setState(() {
                  mylocation=data;
                });

                // print(name+"\n"+subLocality+"\n"+locality+"\n"+administrativeArea+"\n"+postalCode+"\n"+country);
              }

            }






        });








      }


  }





  checkData() async
  {
    if(namecontroller.text.isNotEmpty)
      {
        if(mobilecontroller.text.isNotEmpty)
        {
          if(mobilecontroller.text.length==10)
          {

            if(pincodecontroller.text.isNotEmpty)
            {



                if (flatcontroller.text.isNotEmpty) {
                  if (areacontroller.text.isNotEmpty) {
                    if (landmarkcontroller.text.isNotEmpty) {
                      if (towncontroller.text.isNotEmpty) {
                        if (languagedropdown.compareTo(
                            "Select your address type") != 0) {
                          if (id.compareTo("0") != 0) {
                            final datastorage = await SharedPreferences
                                .getInstance();
                            ProgressDialog _progressDialog = ProgressDialog();
                            _progressDialog.showProgressDialog(context,
                                textToBeDisplayed: "Please wait for a moment......");
                            var date = new DateTime.now().toIso8601String();

                            var dataasync = await http.post(
                                Uri.parse(UrlData.baseurl + UrlData
                                    .updateAddress),

                                headers: <String, String>{
                                  'Content-Type': 'application/x-www-form-urlencoded',
                                 UrlData.header_token :datastorage.getString(DataConstants
                                      .tokenkey)!
                                },

                                body: <String, String>{
                                  'Content-Type': 'application/x-www-form-urlencoded',

                                  'name': namecontroller.text.toString(),
                                  'mobile': mobilecontroller.text.toString(),
                                  'pincode': pincodecontroller.text.toString(),
                                  'flatno': flatcontroller.text,

                                  'area': areacontroller.text.toString(),
                                  'landmark': landmarkcontroller.text,
                                  'town': towncontroller.text.toString(),
                                  'latitude': lat.toString(),
                                  'longitude': longi.toString(),
                                  'addresstype': languagedropdown.toString(),
                                  'addressid': id
                                }

                            );


                            _progressDialog.dismissProgressDialog(context);

                            String response = dataasync.body;


                            dynamic jsondata = jsonDecode(response);

                            int status = jsondata['status'];

                            String message = jsondata['message'];

                            if (status == 1) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text(
                                        "Address edited successfully"),
                                  ));


                              Navigator.pop(context, {"added": 1});
                            }
                            else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text("Failed"),
                                  ));
                            }
                          }
                          else {
                            final datastorage = await SharedPreferences
                                .getInstance();
                            ProgressDialog _progressDialog = ProgressDialog();
                            _progressDialog.showProgressDialog(context,
                                textToBeDisplayed: "Please wait for a moment......");
                            var date = new DateTime.now().toIso8601String();

                            var dataasync = await http.post(
                                Uri.parse(UrlData.baseurl + UrlData.addAddress),

                                headers: <String, String>{
                                  'Content-Type': 'application/x-www-form-urlencoded',
                                 UrlData.header_token :datastorage.getString(DataConstants
                                      .tokenkey)!
                                },

                                body: <String, String>{
                                  'Content-Type': 'application/x-www-form-urlencoded',

                                  'name': namecontroller.text.toString(),
                                  'mobile': mobilecontroller.text.toString(),
                                  'pincode': pincodecontroller.text.toString(),
                                  'flatno': flatcontroller.text,

                                  'area': areacontroller.text.toString(),
                                  'landmark': landmarkcontroller.text,
                                  'town': towncontroller.text.toString(),
                                  'latitude': lat.toString(),
                                  'longitude': longi.toString(),
                                  'addresstype': languagedropdown.toString(),

                                }

                            );


                            _progressDialog.dismissProgressDialog(context);

                            String response = dataasync.body;


                            dynamic jsondata = jsonDecode(response);

                            int status = jsondata['status'];

                            String message = jsondata['message'];

                            if (status == 1) {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text("Address added successfully"),
                                  ));


                              Navigator.pop(context, {"added": 1});
                            }
                            else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text("Failed"),
                                  ));
                            }
                          }
                        }
                        else {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Select your address type"),
                          ));
                        }
                      }
                      else {
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Enter town"),
                        ));
                      }
                    }
                    else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Enter land mark"),
                      ));
                    }
                  }
                  else {
                    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text("Enter area"),
                    ));
                  }
                }
                else {
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                    content: Text("Enter flat no."),
                  ));
                }
              // }
              // else{
              //
              //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              //     content: Text("Pincode verification is failed.we cannot deliver to this area"),
              //   ));
              //
              // }

            }
            else{

              ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                content: Text("Enter pincode"),
              ));

            }



          }
          else{

            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
              content: Text("Enter proper mobile number"),
            ));

          }

        }
        else{

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text("Enter mobile number"),
          ));

        }
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Enter name"),
      ));

    }


  }







  fetchLocation() async
  {

    bool  serviceEnabled = await  Geolocator().isLocationServiceEnabled();





    if (!serviceEnabled) {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Location disabled"),
      ));

    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Location enabled"),
      ));

      var  loc = await  Geolocator().getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
      print(loc);

      lat=loc.latitude.toString();
      longi=loc.longitude.toString();

      List<Placemark.Placemark> placemarks = await placemarkFromCoordinates(double.parse(lat), double.parse(longi)) as List<Placemark.Placemark>;


      if(placemarks.length>0)
        {

          Placemark.Placemark placeMark  = placemarks[0];

          String name = placeMark.name!;
          String subLocality = placeMark.subLocality!;
          String locality = placeMark.locality!;
          String administrativeArea = placeMark.administrativeArea!;
          String postalCode = placeMark.postalCode!;
          String country = placeMark.country!;

          String data="";
          if(name!=null||name.isNotEmpty)
            {
              data=data+name+",";
            }
          if(subLocality!=null||subLocality.isNotEmpty)
          {
            data=data+subLocality+",";
          }
          if(locality!=null||locality.isNotEmpty)
          {
            data=data+locality+",";
          }
          if(administrativeArea!=null||administrativeArea.isNotEmpty)
          {
            data=data+administrativeArea+",";
          }

          if(postalCode!=null||postalCode.isNotEmpty)
          {
            data=data+postalCode+",";
          }

          if(country!=null||country.isNotEmpty)
          {
            data=data+country+"";
          }

setState(() {
  mylocation=data;
});

          // print(name+"\n"+subLocality+"\n"+locality+"\n"+administrativeArea+"\n"+postalCode+"\n"+country);
        }






    }
    }






}