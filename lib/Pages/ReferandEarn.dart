import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Welcomepage.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/services.dart';
import 'package:share/share.dart';

void main() {
  runApp(MyApp());
}



class ReferEarnPage extends StatefulWidget {
  ReferEarnPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _ReferEarnPageState createState() => _ReferEarnPageState();
}

class _ReferEarnPageState extends State<ReferEarnPage> {

String link="";



  @override
  void initState() {
    // TODO: implement initState

    getReferalLink();

    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;

    return Scaffold(
      resizeToAvoidBottomInset : false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor:Color(0xfff55245),
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.white, ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Refer and earn",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.white),),
        centerTitle: true,
      ),
      body: Container(


        width: double.infinity,
        height: double.infinity,
        color:Color(0xffEBEBEB),

        child: Column(

          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          
          children: [
            
            Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),
            
            child: Image.asset("images/referbg.png",width:ResponsiveInfo.isMobile(context)? 150:243,height:ResponsiveInfo.isMobile(context)? 176:283,
            fit: BoxFit.fill,)
              
              ,
            ),

    Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19),

      child: Card(
        elevation: 8,

        child: Container(

          width: double.infinity,
          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?40:60:80,
          
          child: Row(
            
            children: [
              
              Expanded(child: Padding(padding: EdgeInsets.fromLTRB(5, 0, 0, 0),

          child:

              Text(link,maxLines: 1,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),)),flex: 2,),
              
              Expanded(child: Container(width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:80:100,height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:80:100,color: Color(0xfff55245),child: Align(
    alignment: Alignment.center,


           child: TextButton(onPressed: () {

             Clipboard.setData(ClipboardData(text:link));

             ScaffoldMessenger.of(context).showSnackBar(SnackBar(
               content: Text("Copied to clipboard"),
             ));

           },
           child: Text("Copy link",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.white)),),)  ),flex: 1,)
              
              
            ],
            
            
          ),


        ),


      ),


    ),


            Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                child:  Container(

                  width:ResponsiveInfo.isMobile(context)? 120:200,
                  height:ResponsiveInfo.isMobile(context)? 50:90,
                  decoration: BoxDecoration(
                      color: Color(0xfff55245),
                      border: Border.all(
                        color: Color(0xfff55245),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                  ),
                  child: TextButton(onPressed: () {



                    Share.share(link);




                  },
                    child: Text("Share",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 13:22),),





                  ),




                )),



            
            
            
          ],


          


        ),
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.

      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }




  getReferalLink() async
  {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getEncryptedUserData),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
      {

        setState(() {

          link=UrlData.baseurl+UrlData.referal+jsondata['data'];
        });

      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to generate link"),
      ));

    }


  }


}
