import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/MyAddress.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/NotificationServer.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class NotificationPage extends StatefulWidget {
  final String title;

  const NotificationPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _NotificationPage();
}

class _NotificationPage extends State<NotificationPage> {


  List<NotificationData>notificationData=[];

  bool _fromTop = true;
  @override
  void initState() {
    // TODO: implement initState
    getNotifications();

    super.initState();


  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:
        AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Notification",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.white),),
          centerTitle: false,
        ),

      body: Stack(

        children: [
          ListView.builder(
            physics: BouncingScrollPhysics(),

            shrinkWrap: true,

            itemCount: notificationData.length,
            itemBuilder: (context, index) {
              return  Card(
                  elevation: ResponsiveInfo.isMobile(context)?5:10,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child:InkWell(

                    onTap: (){

                      updateReadStatus(notificationData[index].id);


                      showGeneralDialog(
                        barrierLabel: "Label",
                        barrierDismissible: true,
                        barrierColor: Colors.black.withOpacity(0.5),
                        transitionDuration: Duration(milliseconds: 700),
                        context: context,
                        pageBuilder: (context, anim1, anim2) {
                          return Align(
                            alignment: _fromTop ? Alignment.topCenter : Alignment.bottomCenter,
                            child: Container(
                              width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?200:250:300,
                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?260:290:360,

                              child: SingleChildScrollView(


                                  child: Column(

                                    children: [


                                      Container(child: Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                          child: Column(

                                                                                       children: [
                                              Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                                child:
                                                Text(notificationData[index].title,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 17,color: Colors.black,)     :TextStyle(fontSize: 26,color: Colors.black),
                                                ),),

                                              Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                                child:
                                                Text(notificationData[index].message,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,)     :TextStyle(fontSize: 20,color: Colors.black54),
                                                ),)


                                            ],


                                          )






                                      )),








                                    ],


                                  )),
                              margin: EdgeInsets.only(top: 60, left: 12, right: 12, bottom: 60),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                              ),
                            ),
                          );
                        },
                        transitionBuilder: (context, anim1, anim2, child) {
                          return SlideTransition(
                            position: Tween(begin: Offset(0, _fromTop ? -1 : 1), end: Offset(0, 0)).animate(anim1),
                            child: child,
                          );
                        },
                      );


                    },

                    child:Container(




                        child:Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                            child: Column(

                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                  child:
                                  Text(notificationData[index].title,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 17,color: Colors.black,)     :TextStyle(fontSize: 26,color: Colors.black),
                                  ),),

                                Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20),
                                  child:
                                  Text(notificationData[index].message,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,)     :TextStyle(fontSize: 20,color: Colors.black54),
                                  ),)


                              ],


                            )






                        )


                    )



                  )






              )



              ;


            },
          )

        ],


      ),


    );
  }

  getNotifications() async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getNotifications),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    NotificationServer ntf=NotificationServer.fromJson(jsondata);

    if(ntf.status==1)
      {

        if(ntf.notificationData.length>0)
          {

            setState(() {

              notificationData.clear();

              notificationData.addAll(ntf.notificationData);


            });
          }



      }
    else{




    }



  }


  updateReadStatus(String notificationid) async
  {
    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.updateReadStatus+"/"+notificationid.toString()),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

  }


}