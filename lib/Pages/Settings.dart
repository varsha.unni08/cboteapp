import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/AppUpdate.dart';
import 'package:flutter_app_ecommerce/login.dart';
import 'package:package_info_plus/package_info_plus.dart';



import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';


import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class SettingsPage extends StatefulWidget {
  final String title;



  const SettingsPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SettingsPage();
}

class _SettingsPage extends State<SettingsPage> {

  bool _switchValue=false;


  @override
  void initState() {
    // TODO: implement initState



    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;
    double height=screenSize.height;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Settings",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.white),),
          centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[

            // Align(
            Container(
              margin: ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(10, 10, 10, 5):EdgeInsets.fromLTRB(20, 40, 20, 15),
              child:  SingleChildScrollView(

                  child: Column(

                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [

                        // Card(
                        //     child:  InkWell(
                        //
                        //       onTap: (){
                        //
                        //
                        //
                        //       },
                        //
                        //
                        //
                        //       child: Container(
                        //           width: double.infinity,
                        //           height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                        //           child: Padding(
                        //             padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                        //             child: Align(
                        //               alignment: Alignment.centerLeft,
                        //               child:Row(
                        //                 children: [
                        //
                        //                   Container(
                        //                     width:width/1.7 ,
                        //                     child: Text("App lock",
                        //                         style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                        //                   )
                        //                   ,
                        //
                        //                   CupertinoSwitch(
                        //                     value: _switchValue,
                        //                     onChanged: (value) async{
                        //
                        //
                        //
                        //
                        //
                        //                     },
                        //                   )
                        //                 ],
                        //
                        //
                        //               ) ,
                        //             ),
                        //           )),
                        //     )
                        // ),
                        Card(

                            child:   InkWell(

                              onTap: () async{

                                // ProgressDialog _progressDialog = ProgressDialog();
                                // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

                                String url=UrlData.privacyurl;

                                if (await canLaunch(url)) {
                                await launch(url, forceWebView: true);
                                } else {
                                throw 'Could not launch $url';
                                }

                              },



                              child: Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Privacy policy",
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                                    ),
                                  )),


                            )
                        ),
                        Card(
                            child:   InkWell(

                              onTap: () async{

                                String url=UrlData.termsurl;

                                if (await canLaunch(url)) {
                                await launch(url, forceWebView: true);
                                } else {
                                throw 'Could not launch $url';
                                }

                                // ProgressDialog _progressDialog = ProgressDialog();
                                // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");


                              },



                              child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Terms and conditions",
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                                    ),
                                  )),)
                        ),

                        Card(
                            child:   InkWell(

                              onTap: () async{





                                PackageInfo packageInfo = await PackageInfo.fromPlatform();
                                String version = packageInfo.version;
                                String code = packageInfo.buildNumber;

                            //    print(version);

                                double v=double.parse(code);

                                ProgressDialog _progressDialog = ProgressDialog();
                                _progressDialog.showProgressDialog(
                                    context, textToBeDisplayed: "Please wait for a moment......");

                                final datacount = await SharedPreferences.getInstance();
                                var date = new DateTime.now().toIso8601String();
                                var dataasync = await http.post(
                                  Uri.parse(UrlData.baseurl + UrlData.getVerion),
                                  headers: <String, String>{
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
                                  },
                                );
                                _progressDialog.dismissProgressDialog(context);
                                String response = dataasync.body;

                                dynamic jsondata = jsonDecode(response);

                                print(response);

                                AppUpdate apu=AppUpdate.fromJson(jsondata);

                                if(apu.status==1)
                                  {

                                    double updatedversion=0;

                                    if(apu.data.length>0)
                                      {

                                        updatedversion=double.parse(apu.data[0].version);
                                      }


                                    Dialog forgotpasswordDialog = Dialog(
                                      shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0): BorderRadius.circular(24.0)), //this right here
                                      child: Container(
                                        height: height/2,
                                        width: width/1.5,

                                        child: Column(
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: <Widget>[



                                            Padding(
                                              padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0)  : EdgeInsets.all(30.0),
                                               child:  Image.asset("images/kolica.png",width: ResponsiveInfo.isMobile(context)?130:260,height:ResponsiveInfo.isMobile(context)?130: 260,fit: BoxFit.fill,),
                                              ),


                                          Padding(
                                              padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0)  : EdgeInsets.all(30.0),
                                              child:  Text("Version : "+v.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?14:19 ),),
                                            ),

                                            (updatedversion>v)?    Padding(
                                              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(30.0),

                                              child: Container(

                                                width:ResponsiveInfo.isMobile(context)?120: 200,
                                                height: ResponsiveInfo.isMobile(context)?50:90,
                                                decoration: BoxDecoration(

                                                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(10):BorderRadius.circular(20)),
                                                child:Align(
                                                  alignment: Alignment.center,
                                                  child: TextButton(

                                                    onPressed:() async{


                                                      String url=UrlData.playstoreurl;

                                                      if (await canLaunch(url )) {
                                                      await launch(url);
                                                      } else {
                                                      throw 'Could not launch $url';
                                                      }




                                                    },

                                                    child: Text('Update', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13)   : TextStyle(color: Colors.white,fontSize: 23) ,) ,),
                                                ),



                                                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                              ),


                                              // ,
                                            ):Container(),
                                            // Padding(padding: EdgeInsets.only(top: 50.0)),
                                            // TextButton(onPressed: () {
                                            //   Navigator.of(context).pop();
                                            // },
                                            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                                          ],
                                        ),
                                      ),
                                    );



                                    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);





                                  }
                                else{
                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    content: Text("Cannot fetch version data"),
                                  ));



                                }








                              },



                              child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("App update",
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                                    ),
                                  )),)
                        ),

                        Card(
                            child:   InkWell(

                              onTap: () async{

                                ProgressDialog _progressDialog = ProgressDialog();
                                _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

                                final datacount = await SharedPreferences.getInstance();
                                var date = new DateTime.now().toIso8601String();
                                var dataasync = await http.post(
                                  Uri.parse(UrlData.baseurl + UrlData.getCustomerCareNumber),
                                  headers: <String, String>{
                                    'Content-Type': 'application/x-www-form-urlencoded',
                                    UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
                                  },
                                );

                                _progressDialog.dismissProgressDialog(context);

                                String response = dataasync.body;

                                dynamic jsondata = jsonDecode(response);


                                if(jsondata['status']==1) {

                                  String phone=jsondata['customercare'];


                                  Dialog forgotpasswordDialog = Dialog(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: ResponsiveInfo.isMobile(
                                            context) ? BorderRadius.circular(
                                            12.0) : BorderRadius.circular(
                                            24.0)), //this right here
                                    child: Container(
                                      height: ResponsiveInfo.isMobile(context)
                                          ? 250
                                          : 400,
                                      width: width / 1.5,

                                      child: Column(
                                        mainAxisAlignment: MainAxisAlignment
                                            .center,
                                        children: <Widget>[

                                          Padding(
                                            padding: ResponsiveInfo
                                                .isMobile(context)
                                                ? EdgeInsets.fromLTRB(
                                                10, 0, 0, 0)
                                                : EdgeInsets.fromLTRB(
                                                20, 0, 0, 0),
                                            child: Text("Contact with",
                                              style: TextStyle(
                                                  fontSize: ResponsiveInfo
                                                      .isMobile(context)
                                                      ? 13
                                                      : 19,
                                                  color: Colors.black),)
                                            ,


                                          ),
                                          Padding(
                                            padding: ResponsiveInfo.isMobile(
                                                context)
                                                ? EdgeInsets.all(15.0)
                                                : EdgeInsets.all(30.0),
                                            child: InkWell(

                                              onTap: () async{

                                                String url="https://wa.me/91"+phone;

                                                if (await canLaunch(url)) {
                                                await launch(url);
                                                } else {
                                                throw 'Could not launch $url';
                                                }




                                              },

                                              child: Row(
                                                children: [

                                                  Image.asset(
                                                    "images/whatsapp.png",
                                                    width: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? 35
                                                        : 55,
                                                    height: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? 35
                                                        : 55,
                                                    fit: BoxFit.fill,),

                                                  Padding(
                                                    padding: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? EdgeInsets.fromLTRB(
                                                        10, 0, 0, 0)
                                                        : EdgeInsets.fromLTRB(
                                                        20, 0, 0, 0),
                                                    child: Text("Whatsapp",
                                                      style: TextStyle(
                                                          fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                              ? 13
                                                              : 19,
                                                          color: Colors.black),)
                                                    ,


                                                  )


                                                ],


                                              ),

                                            )


                                            ,
                                          ),

                                          Padding(
                                            padding: ResponsiveInfo.isMobile(
                                                context)
                                                ? EdgeInsets.all(15.0)
                                                : EdgeInsets.all(30.0),
                                            child: InkWell(

                                              onTap: () async{
                                                String url="tel:"+phone;

                                                if (await canLaunch(url)) {
                                                await launch(url);
                                                } else {
                                                throw 'Could not launch $url';
                                                }
                                              },

                                              child: Row(
                                                children: [

                                                  Image.asset(
                                                    "images/telephonecall.png",
                                                    width: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? 35
                                                        : 55,
                                                    height: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? 35
                                                        : 55,
                                                    fit: BoxFit.fill,),

                                                  Padding(
                                                    padding: ResponsiveInfo
                                                        .isMobile(context)
                                                        ? EdgeInsets.fromLTRB(
                                                        10, 0, 0, 0)
                                                        : EdgeInsets.fromLTRB(
                                                        20, 0, 0, 0),
                                                    child: Text("Call",
                                                      style: TextStyle(
                                                          fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                              ? 13
                                                              : 19,
                                                          color: Colors.black),)
                                                    ,


                                                  )


                                                ],


                                              ),

                                            )


                                            ,
                                          ),

                                          // Padding(padding: EdgeInsets.only(top: 50.0)),
                                          // TextButton(onPressed: () {
                                          //   Navigator.of(context).pop();
                                          // },
                                          //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                                        ],
                                      ),
                                    ),
                                  );


                                  showDialog(context: context,
                                      builder: (
                                          BuildContext context) => forgotpasswordDialog);
                                }
                                else{

                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    content: Text("Cannot fetch customer care number"),
                                  ));

                                }




                              },



                              child:Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                                  child: Padding(
                                    padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Customer care",
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                                    ),
                                  )),)
                        ),


                        Card(
                            child:
                            InkWell(

                              onTap: ()async{

                                Widget yesButton = TextButton(
                                    child: Text("Yes"),
                                    onPressed: () async {

                                      Navigator.pop(context);

                                      final datastorage = await SharedPreferences.getInstance();
                          datastorage.setString(DataConstants.tokenkey,"");

                                      Navigator.pushAndRemoveUntil(
                                        context,
                                        MaterialPageRoute(
                                          builder: (BuildContext context) => MyLoginPage(title: "login"),
                                        ),
                                            (route) => false,
                                      );


                                    });



                                Widget noButton = TextButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                );

                                // set up the AlertDialog
                                AlertDialog alert = AlertDialog(
                                  title: Text("Kolica"),
                                  content: Text("Do you want to log out now ?"),
                                  actions: [yesButton, noButton],
                                );

                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return alert;
                                  },
                                );

                              },




                              child: Container(
                                  width: double.infinity,
                                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90,
                                  child: Padding(
                                 padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):EdgeInsets.all(20):EdgeInsets.all(30),
                                    child: Align(
                                      alignment: Alignment.centerLeft,
                                      child: Text("Sign out",
                                          style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20, color: Colors.black)),
                                    ),
                                  )),)
                        ),

                      ])),
            )
            //)
          ]),
        ));
  }






}


