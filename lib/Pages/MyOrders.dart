import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/OrderDetails.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/MyOrderData.dart';
import 'package:intl/intl.dart';



import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class MyOrderPage extends StatefulWidget {
  final String title;



  const MyOrderPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyOrderPage();
}

class _MyOrderPage extends State<MyOrderPage> {

  bool _switchValue=false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  List<Data>myorderdata=[];


  @override
  void initState() {
    // TODO: implement initState

    getOrders();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("My Orders",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.white),),
          centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[


            ListView.builder(

                itemCount: myorderdata.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8):EdgeInsets.all(13):EdgeInsets.all(24),

                      child:Card(

                          elevation: 5,

                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 0),
                            borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                          ),

                          child:    Container(height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?180:220:270,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [

                                Row(
                                  children: [

                                    Expanded(child:  Align(

                                      alignment: Alignment.topLeft,
                                      child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0):EdgeInsets.fromLTRB(15, 0, 20, 0):EdgeInsets.fromLTRB(20, 0, 25, 0),

                                          child:TextButton(onPressed: () {


                                          }, child: Text("Order Id : #"+myorderdata[index].orderid.toString(), style: TextStyle(color: Colors.black,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19 ),),



                                          )

                                      )



                                      ,

                                    ),flex: 2,)

                                   ,
                        Expanded(child:   Align(

                                      alignment: Alignment.topRight,
                                      child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 0, 10, 0):EdgeInsets.fromLTRB(15, 0, 20, 0):EdgeInsets.fromLTRB(20, 0, 25, 0),

                                          child:TextButton(onPressed: () {


                                          }, child: Text(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(myorderdata[index].orderDate).day.toString()+"-" +new DateFormat("yyyy-MM-dd hh:mm:ss").parse(myorderdata[index].orderDate).month.toString()+"-"+new DateFormat("yyyy-MM-dd hh:mm:ss").parse(myorderdata[index].orderDate).year.toString()     , style: TextStyle(color: Colors.black38,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:19 ),),



                                          )

                                      )



                                      ,

                                    ),flex: 2),
                                  ],


                                ),

                                Align(

                                  alignment: Alignment.topLeft,

                                  child:Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 0, 10, 0):EdgeInsets.fromLTRB(25, 0, 20, 0):EdgeInsets.fromLTRB(35, 0, 25, 0),

                                      child: Text("Items : "+myorderdata[index].itemCount.toString(), style: TextStyle(color: Colors.black38,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?11:13:17 ,),



                                      )

                                  ) ,

                                )

                                ,

                                Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 10, 10, 0):EdgeInsets.fromLTRB(25, 15, 20, 0):EdgeInsets.fromLTRB(35, 25, 25, 0),

                                    child: Text("Total amount : ₹ "+myorderdata[index].totalwithdeliverycharge.toString(), style: TextStyle(color: Colors.black,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:18:22 ,),



                                    )

                                ),

                                Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,

                                  children: [
                                     Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 10, 10, 0):EdgeInsets.fromLTRB(20, 20, 20, 0):EdgeInsets.fromLTRB(25, 25, 25, 0),

                                          child:Container(


                                              decoration: BoxDecoration(
                                                border: Border.all(color: Colors.blueAccent),
                                                borderRadius:ResponsiveInfo.isMobile(context)?BorderRadius.circular(5):BorderRadius.circular(8) ,
                                              ),

                                              child:  TextButton(onPressed: () async {


                                                // Navigator.push(
                                                //     context, MaterialPageRoute(builder: (_) => OrderDetailsPage(title: "Order details", orderdata: myorderdata[index],)));



                                                final result = await Navigator.push(
                                                  context,
                                                  MaterialPageRoute(builder: (context) => OrderDetailsPage(title: "Order details", orderdata: myorderdata[index], index: index,)),
                                                );

                                                if(result!=null)
                                                  {

                                                    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                                    //   content: Text('Full cancell count reached '+result.toString()),
                                                    // ));

                                                  //  await Future.delayed(Duration(seconds: 3));


                                                    getOrders();

                                                   // cancelOrder(result.toString());

                                                  }
                                                else{

                                                  getOrders();

                                                  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                                  //   content: Text('Full cancell count not reached'+result.toString()),
                                                  // ));
                                                }



                                              }, child: Text("View details", style: TextStyle(color: Colors.blue,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),



                                              ))

                                      )



                                    ,

                                    (myorderdata[index].deliveryCompletionStatus.compareTo("1")==0 )?Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                                        child:Container(

                                          child:   Text("Delivery completed", style: TextStyle(color: Colors.green,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:20 ),),



                                        )

                                    ):Container(),


                                    (myorderdata[index].saleStatus.compareTo("0")==0 )?(myorderdata[index].cancelStatus.compareTo("1")==0)?  Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                                        child:Container(

                                            child:   Text("Order cancelled", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:20 ),),



                                            )

                                    ) : Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                                          child:Container(


                                              decoration: BoxDecoration(
                                                border: Border.all(color: Colors.redAccent),
                                                borderRadius:ResponsiveInfo.isMobile(context)?BorderRadius.circular(5):BorderRadius.circular(8) ,
                                              ),

                                              child:  TextButton(onPressed: () {


cancelOrder(myorderdata[index].orderid);

                                              }, child: Text("Cancel", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),



                                              ))

                                      ):Container()





                                  ],
                                )














                              ],


                            ),




                          )

                      )

                  )


                  ;



                }
            )





          ]),
        ));
  }





cancelOrder(String id) async{


  final datastorage = await SharedPreferences.getInstance();
  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
  var date = new DateTime.now().toIso8601String();

  var dataasync = await http.post(
    Uri.parse(UrlData.baseurl+UrlData.cancelMyOrder+"/"+id),

    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

    },



  );



  _progressDialog.dismissProgressDialog(context);

  String response = dataasync.body;

  print(response);

  dynamic jsondata = jsonDecode(response);
  if(jsondata['status']==1)
    {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Success'),
      ));
      getOrders();


    }
  else   if(jsondata['status']==2){

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Your order already dispatched , you cannot cancel the order'),
    ));
  }
  else{

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text('Failed'),
    ));

  }

}


  cancelFullOrder(String id) async{


    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.cancelFullOrder+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);
    if(jsondata['status']==1)
    {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Success'),
      ));
      getOrders();


    }
    else   if(jsondata['status']==2){

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Your order already dispatched , you cannot cancel the order'),
      ));
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }

  }




  getOrders() async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getSalesOrder),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);
    dynamic jsondata = jsonDecode(response);
    MyOrderData mod=MyOrderData.fromJson(jsondata);

    if(mod.status==1)
      {
        myorderdata.clear();

        if(mod.data.length>0) {

          for(int i=0;i<mod.data.length;i++)
            {
              double deliverycharg=double.parse(mod.data[i].deleveryCharge);

              double total=double.parse(mod.data[i].totalPrice);

              if(mod.data[i].cancelStatus.compareTo("1")!=0) {
                double t = total + deliverycharg;

                mod.data[i].totalwithdeliverycharge = t.toString();
              }
              else{
                mod.data[i].totalwithdeliverycharge="0";

              }


            }





          setState(() {

            myorderdata.addAll(mod.data.reversed.toList());
          });
        }
        else{

          setState(() {

            myorderdata.clear();
          });

          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            content: Text('No data found'),
          ));

        }

      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));
    }




  }



}


