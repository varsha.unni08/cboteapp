import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/FinalPage.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/TotalAmount.dart';
import 'package:flutter_app_ecommerce/domain/user_address.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class SingleOrderPage extends StatefulWidget {
  final String title;
  final String addressid;
  final String deliveryinstructions;
  final String cartid;


  const SingleOrderPage({Key? key, required this.title, required this.addressid, required this.deliveryinstructions, required this.cartid}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _SingleOrderPage(addressid,deliveryinstructions,cartid);
}

class _SingleOrderPage extends State<SingleOrderPage> {
  bool _switchValue = false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;

  bool _value = false;
  int val1 = 1;
  int val2=-1;
  bool isChecked=false;

  String addressid="";
  String deliveryinstructions="";
  double amounttopay=0;

  double deliverycharge=0;
  double freedeliveryamount=0;

  double totalamount=0;

  double cashback=0;

  double walletbalance=0;

  String address="";
  String cartid="";

  _SingleOrderPage(this.addressid, this.deliveryinstructions,this.cartid);

  @override
  void initState() {
    // TODO: implement initState
    calculateCashbackAmount();
    calculateAmount();
    getWalletBalance();
    getAddressById(addressid);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {




    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "Place Order",
            style: TextStyle(
                fontSize: ResponsiveInfo.isMobile(context)
                    ? ResponsiveInfo.isSmallMobile(context)
                    ? 12
                    : 16
                    : 20,
                color: Colors.white),
          ),
          centerTitle: false,
        ),
        body: Column(

          children: [

            Flexible(child:  Stack(


                children: <Widget>[

                  Container(
                    width: double.infinity,
                    height:double.infinity,

                    color: Color(0xffffffff),

                  ),

                  SingleChildScrollView(
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,

                          children: [

                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),

                              child: Card(
                                elevation: ResponsiveInfo.isMobile(context)?10:15,

                                child: Container(
                                  width: double.infinity,

                                  child: Column(

                                    children: [

                                      ListTile(
                                        title: (walletbalance==0)?Text("From Wallet",  style: TextStyle(
                                            fontSize: ResponsiveInfo.isMobile(context)
                                                ? ResponsiveInfo.isSmallMobile(context)
                                                ? 15
                                                : 18
                                                : 23,
                                            color: Colors.black)):

                                        Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,

                                          children: [
                                            Text("From Wallet ",  style: TextStyle(
                                                fontSize: ResponsiveInfo.isMobile(context)
                                                    ? ResponsiveInfo.isSmallMobile(context)
                                                    ? 15
                                                    : 18
                                                    : 23,
                                                color: Colors.black)),
                                            Text("₹"+walletbalance.toString(),  style: TextStyle(
                                                fontSize: ResponsiveInfo.isMobile(context)
                                                    ? ResponsiveInfo.isSmallMobile(context)
                                                    ? 12
                                                    : 14
                                                    : 18,
                                                color: Colors.black))
                                          ],)
                                        ,
                                        leading: Radio(
                                          value: 1,
                                          groupValue: val1,
                                          onChanged: (value) {
                                            setState(() {
                                              val1 = value as int;
                                              val2=-1;
                                            });
                                          },
                                          activeColor: Colors.green,
                                        ),
                                      ),

                                      ListTile(
                                        title: Text("Cash on delivery",  style: TextStyle(
                                            fontSize: ResponsiveInfo.isMobile(context)
                                                ? ResponsiveInfo.isSmallMobile(context)
                                                ? 15
                                                : 18
                                                : 23,
                                            color: Colors.black)),
                                        leading: Radio(
                                          value: 2,
                                          groupValue: val2,
                                          onChanged: (value) {
                                            setState(() {
                                              val2 = value as int;
                                              val1=-1;
                                            });
                                          },
                                          activeColor: Colors.green,
                                        ),
                                      ),




                                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 5, 20, 5):EdgeInsets.fromLTRB(30, 10, 30, 10):EdgeInsets.fromLTRB(50, 20, 50, 20)

                                        ,
                                        child:  Row(

                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,

                                          children: [



                                            Text("Amount : ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black54),),



                                            Text("₹ "+totalamount.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                                          ],),


                                      ),
                                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 5, 20, 5):EdgeInsets.fromLTRB(30, 10, 30, 10):EdgeInsets.fromLTRB(50, 20, 50, 20)

                                        ,
                                        child:  Row(

                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,

                                          children: [



                                            Text("Delivery : ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black54),),



                                            Text("₹ "+deliverycharge.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                                          ],),


                                      ),
                                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 5, 20, 5):EdgeInsets.fromLTRB(30, 10, 30, 10):EdgeInsets.fromLTRB(50, 20, 50, 20)

                                        ,
                                        child:  Row(

                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment: CrossAxisAlignment.center,

                                          children: [



                                            Text("Order Total : ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),



                                            Text("₹ "+amounttopay.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                                          ],),


                                      ),

                                      (totalamount<freedeliveryamount)?  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 5, 20, 5):EdgeInsets.fromLTRB(30, 10, 30, 10):EdgeInsets.fromLTRB(50, 20, 50, 20)

                                        ,
                                        child: Text("Free delivery charge for an amount greater than ₹ "+freedeliveryamount.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                                      ):Container(),
                                      (cashback>0)? Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(20, 5, 20, 5):EdgeInsets.fromLTRB(30, 10, 30, 10):EdgeInsets.fromLTRB(50, 20, 50, 20)

                                        ,
                                        child: Text("You are going to receive ₹ "+cashback.toString()+" cashback",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Color(
                                            0xff456110)),),


                                      ):Container(),






                                    ],



                                  ),





                                ),




                              ),

                            ),

                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),











                                child:


                                Card(


                                    elevation: ResponsiveInfo.isMobile(context)?10:15,




                                    child: Container(

                                      width: double.infinity,

                                      child:Column(
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [

                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),child:  Text("Shipping Address",style: TextStyle(color: Colors.black,fontSize:ResponsiveInfo.isMobile(context)? 17:26))),
                                            Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),child: Text(address,maxLines:5,style: TextStyle(color: Colors.black,fontSize:ResponsiveInfo.isMobile(context)? 13:18)))



                                          ]


                                      ) ,

                                    )





                                )




                            ),



                            (val1!=1) ? Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(20, 10, 20, 10):EdgeInsets.fromLTRB(30,20, 30, 20),

                                child:Text(
                                  "You can use UPI apps at the time of delivery for online payment\n"+"( ഉത്പന്നങ്ങൾ ഡെലിവറി  ചെയ്യുന്ന സമയത്തു നിങ്ങള്ക്ക്  താഴെ കാണുന്ന മൊബൈൽ അപ്പ്ലിക്കേഷനുകൾ വഴി ഓൺലൈൻ പേയ്മെന്റ്  ചെയ്യാവുന്നതാണ്)",
                                  style: TextStyle(
                                      fontSize: ResponsiveInfo.isMobile(context)
                                          ? ResponsiveInfo.isSmallMobile(context)
                                          ? 12
                                          : 16
                                          : 20,
                                      color: Colors.black),
                                )

                            ) : Container(),


                            (val1!=1) ? Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(20, 10, 20, 10):EdgeInsets.fromLTRB(30,20, 30, 20),

                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,

                                children: [

                                  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),


                                    child: Image.asset("images/gapps/gpay.jpeg",width: ResponsiveInfo.isMobile(context)?50:70,height:ResponsiveInfo.isMobile(context)?50:70 ,) ,

                                  ),

                                  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),


                                    child: Image.asset("images/gapps/amazonpay.png",width: ResponsiveInfo.isMobile(context)?50:70,height:ResponsiveInfo.isMobile(context)?50:70 ,) ,

                                  )
                                  ,
                                  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),


                                    child: Image.asset("images/gapps/paytm.png",width: ResponsiveInfo.isMobile(context)?50:70,height:ResponsiveInfo.isMobile(context)?50:70 ,) ,

                                  )
                                  ,

                                  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(15),


                                    child: Image.asset("images/gapps/phonepay.png",width: ResponsiveInfo.isMobile(context)?50:70,height:ResponsiveInfo.isMobile(context)?50:70 ,) ,

                                  )
                                  ,






                                ],

                              ),
                            ) : Container(),














                          ]

                      )
                  )

                ]
            ),flex: 5,),

            Flexible(child: Container(

              child: Center(

                child: Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(25, 8, 25, 10):EdgeInsets.fromLTRB(50, 20, 50, 20),
                    child:  Container(

                      width:double.infinity,
                      height:ResponsiveInfo.isMobile(context)? 50:90,
                      decoration: BoxDecoration(
                          color: Color(0xfff55245),
                          border: Border.all(
                            color: Color(0xfff55245),
                          ),
                          borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                      ),
                      child: TextButton(onPressed: () {


                        if(val1==1)
                        {

//getWalletBalance();

                          if(walletbalance > totalamount)
                          {

                            placeOrder();

                          }
                          else{
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text('Insufficient balance in wallet'),
                            ));

                          }


                        }
                        else {
                          placeOrder();
                        }


                      },
                        child: Text("Place order",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                      ),




                    )),


              ),






            ),flex: 1,),

          ],

        )




    );
  }

  getWalletBalance() async
  {

    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    //  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getWalletBalance),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
         UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }

    );



    // _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    int status = jsondata['status'];

    String message=jsondata['message'];

    String amount=jsondata['balance'].toString();

    if(status==1) {

      double d=double.parse(amount);

      setState(() {

        walletbalance=d;
      });







      // setState(() {
      //   balance = "₹ " + amount;
      //
      //   balanceamount=balance;
      // });
    }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));
    }

  }

  placeOrder() async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();
    String r=  new Random().nextInt(150000).toString();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl + UrlData.placeSingleOrder+"/"+cartid),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
         UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!,

        },
        body: {

          'addressid': addressid,
          'payment_methode':(val1==1)?val1.toString():val2.toString(),
          'comments':deliveryinstructions,
          'randomstring':r


        }



    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);
    if(jsondata['status']==1)
    {

      if(val1==1)
      {


        reduceAmountinWallet();



      }
      else {
        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => FinalPage(title: "Final page",)
        )
        );
      }


    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }

  }



  reduceAmountinWallet()async
  {
    double d=amounttopay;


    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.subtractBalance),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
         UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'amount':d.toString(),
          'transactionid':date,
          'description':"Amount reduced from wallet after purchase "

        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    int status = jsondata['status'];

    String message=jsondata['message'];


    if(status==1)
    {
      Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context) => FinalPage(title: "Final page",)
      )
      );

    }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }

  }






  calculateAmount()
  async {


    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.calculateSingleItemTotalAmount+"/"+cartid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
       UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!
      },



    );



    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    TotalAmount  tm=TotalAmount.fromJson(jsondata);

    if(tm.status==1)
    {


      setState(() {

        totalamount=double.parse(tm.totalamount);
        deliverycharge=double.parse(tm.deliverycharge);
        freedeliveryamount=double.parse(tm.free_deliveryamount);

        double am=totalamount+deliverycharge;
        amounttopay=am;


      });






    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }





  }




  calculateCashbackAmount()async{


    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.calculateSingleCashbackAmount+"/"+cartid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
       UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!
      },

    );



    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {

      String totalcashbackamount=jsondata['totalcashbackamount'];

      setState(() {

        double d=double.parse(totalcashbackamount);

        cashback=d;
      });




    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }


  }


  getAddressById(String id) async{


    final datastorage = await SharedPreferences.getInstance();

    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl + UrlData.getAddressById),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
         UrlData.header_token :datastorage.getString(DataConstants.tokenkey)!
        },
        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'addressid':id

        }
    );



    String response = dataasync.body;
    dynamic jsondata = jsonDecode(response);
    print(response);

    UserAddress addressById=UserAddress.fromJson(jsondata);


    if(addressById.status==1)
    {

      setState(() {

        address=addressById.address.name+","+addressById.address.phone+"\n"+
            addressById.address.pincode+","+addressById.address.houseno+"\n"+
            addressById.address.landmark+","+addressById.address.area+"\n"+
            addressById.address.town;


      });





    }
    else{



    }

  }

}