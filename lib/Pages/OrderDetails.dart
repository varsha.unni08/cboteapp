import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/MyOrderData.dart';
import 'package:flutter_app_ecommerce/domain/OrderDetailsData.dart';
import 'package:flutter_app_ecommerce/domain/Orderinfo.dart';



import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class OrderDetailsPage extends StatefulWidget {
  final String title;
  final Data orderdata;
  final int index;



  const OrderDetailsPage({Key? key, required this.title, required this.orderdata, required this.index}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _OrderDetailsPage(orderdata,index);
}

class _OrderDetailsPage extends State<OrderDetailsPage> {

  bool _switchValue=false;

  late Data orderdata;
  double totalwithdeliverycharge=0;

  double totalwithoutdeliverycharge=0;
  double deliverycharge=0;

  String quantitytoupdate="";

  List<Detailsdata>orderdetaildata=[];

  int orderdetailscount=0;


  TextEditingController qtycontroller=new TextEditingController();

  int index=0;

  _OrderDetailsPage(this.orderdata,this.index);

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  @override
  void initState() {
    // TODO: implement initState

    getOrderDetails(orderdata.orderid);
    getOrderInfo(orderdata.orderid);
    //getCancelledDataCount(orderdata.orderid);

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;
    double height=screenSize.height;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xffEBEBEB),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Order Id : #"+orderdata.orderid.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.black),),
          centerTitle: true,
          elevation: 0,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[



            Card(
              child:
              Container(
              width: double.infinity,
            height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120:150:190,
            child:    Align(
                alignment: Alignment.topLeft,

                  child: Container(


                    child: Column(
                      children: [
                        Padding(
                          padding: EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text("Payment type",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 1,
                              ),
                              (orderdata.paymentMethode.compareTo("1")==0)? Expanded(
                                child: Text(
                                  "Wallet",
                                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),
                                ),
                                flex: 2,
                              ):
                              Expanded(
                                child: Text(
                                  "Cash on delivery",
                                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text("Amount",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text(
                                  "₹ "+totalwithoutdeliverycharge.toString(),
                                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text("Delivery charge",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text(
                                  "₹ "+deliverycharge.toString(),
                                  style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                        ),
                        Padding(
                          padding: EdgeInsets.all(6),
                          child: Row(
                            children: [
                              Expanded(
                                child: Text("Total Amount ",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 2,
                              ),
                              Expanded(
                                child: Text(":",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)),
                                flex: 1,
                              ),
                              Expanded(
                                child: Text(
                                    "₹ "+totalwithdeliverycharge.roundToDouble().toString(),
                                    style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black)
                                ),
                                flex: 2,
                              )
                            ],
                          ),
                        ),


                      ],
                    ),
                  ),
                ),

          ),




              shape: RoundedRectangleBorder(
                side: BorderSide(color: Colors.white70, width: 0),
                borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
              ),

            ),
        Padding(

            padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(10, 130, 10, 10):EdgeInsets.fromLTRB(10, 160, 10, 10):EdgeInsets.fromLTRB(10, 200, 10, 10),
            child:Container(


              child:ListView.builder(

                  itemCount: orderdetaildata.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(

                            elevation: 5,

                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.white70, width: 0),
                              borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                            ),

                            child:    Container(
                              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?250:300:380,
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [

                                  Row(

                                    children: [

                                      Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(18),

                                        child: Container(

                                            child:



                                            (orderdetaildata[index].image1!=null)?ClipRect(
                                              child:Image.network(UrlData.productimageurl+orderdetaildata[index].image1,
                                                width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:120,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?70:90:120,
                                                errorBuilder:
                                                    (BuildContext context, Object exception, StackTrace? stackTrace) {
                                                  return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120,
                                                  );
                                                },
                                              ) ,
                                            ):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 70:90:120,
                                            )











                                        ),

                                      ),

                                      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 0):EdgeInsets.fromLTRB(0, 16, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0),

                                          child: Column(
                                            crossAxisAlignment: CrossAxisAlignment.center,
                                            mainAxisAlignment: MainAxisAlignment.center,

                                            children: [



                                              SizedBox(


                                                child:  Text(orderdetaildata[index].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),
                                                width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?130:170:200 ,
                                              ),

                                              Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                child: Text("Quantity : "+orderdetaildata[index].qty.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black54),),

                                              ),
                                              Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                  child:Column(
                                                    crossAxisAlignment: CrossAxisAlignment.center,
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [


                                                      Padding(padding: EdgeInsets.all(5),

                                                        child: Text("Price : ₹ "+orderdetaildata[index].salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                                        )
                                                        ,),

                                                      Padding(padding: EdgeInsets.all(5),

                                                        child: Text("Total Price : ₹ "+orderdetaildata[index].TotalPricebyqty,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                                        )
                                                        ,)
                                                    ],


                                                  )
                                              ),

                                              (orderdetaildata[index].cancelStatus.compareTo("1")==0)?  Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                                child: Text("You cancelled this product",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.redAccent),),

                                              ):Container()

                                            ],


                                          )


                                      )






                                    ],


                                  ),



                                  (orderdata.cancelStatus.compareTo("0")==0&&orderdata.saleStatus.compareTo("0")==0&&orderdetaildata[index].cancelStatus.compareTo("0")==0&&orderdata.paymentMethode.compareTo("2")==0)?


                                  Align(

                                    alignment: Alignment.bottomRight,
                                    child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 0, 10, 0):EdgeInsets.fromLTRB(0, 0, 20, 0):EdgeInsets.fromLTRB(0, 0, 25, 0),

                                        child:TextButton(onPressed: () {
                                          if(orderdetaildata[index].qty.compareTo("1")==0) {
                                            Widget yesButton = TextButton(
                                                child: Text("Yes"),
                                                onPressed: () async {
                                                  Navigator.pop(context);

                                                  cancelItem(
                                                      orderdetaildata[index]
                                                          .salesDetailsId,
                                                      index);
                                                });


                                            Widget noButton = TextButton(
                                              child: Text("No"),
                                              onPressed: () {
                                                Navigator.pop(context);
                                              },
                                            );

                                            // set up the AlertDialog
                                            AlertDialog alert = AlertDialog(
                                              title: Text("Kolica"),
                                              content: Text(
                                                  "Do you want to cancel product now ?"),
                                              actions: [yesButton, noButton],
                                            );

                                            showDialog(
                                              context: context,
                                              builder: (BuildContext context) {
                                                return alert;
                                              },
                                            );
                                          }

                                          else{

                                            showDialog(
                                                context: context,
                                                builder: (_) {
                                                  return QuantityConfirmDialog(orderdetaildata[index].qty);
                                                }).then((value) {

                                              quantitytoupdate=value["quantitytoupdate"].toString();

                                              if(quantitytoupdate.compareTo("0")!=0) {
                                                cancelOrderItemQty(
                                                    orderdetaildata[index]
                                                        .salesDetailsId,
                                                    quantitytoupdate);
                                              }
                                              else{


                                                cancelItem(
                                                    orderdetaildata[index]
                                                        .salesDetailsId,
                                                    index);
                                              }



                                            });


                                          //  showQuantityConfirmation(orderdetaildata[index].qty);

                                          }

                                        }, child: Text("Cancel", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),



                                        )

                                    )



                                    ,

                                  )


                                      :Container(),
                                  (orderdata.cancelStatus.compareTo("0")==0&&orderdetaildata[index].cancelStatus.compareTo("0")==0&&orderdata.deliveryCompletionStatus.compareTo("1")==0&&orderdetaildata[index].isreturnable.compareTo("1")==0&&orderdetaildata[index].returnRequestStatus.compareTo("0")==0)?

                                  Align(

                                    alignment: Alignment.bottomRight,
                                    child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 0, 10, 0):EdgeInsets.fromLTRB(0, 0, 20, 0):EdgeInsets.fromLTRB(0, 0, 25, 0),

                                        child:TextButton(onPressed: () {

                                          Widget yesButton = TextButton(
                                              child: Text("Yes"),
                                              onPressed: () async {

                                                Navigator.pop(context);

                                                returnItem(orderdetaildata[index].salesDetailsId,index);



                                              });



                                          Widget noButton = TextButton(
                                            child: Text("No"),
                                            onPressed: () {
                                              Navigator.pop(context);
                                            },
                                          );

                                          // set up the AlertDialog
                                          AlertDialog alert = AlertDialog(
                                            title: Text("Kolica"),
                                            content: Text("Do you want to retun product now ?"),
                                            actions: [yesButton, noButton],
                                          );

                                          showDialog(
                                            context: context,
                                            builder: (BuildContext context) {
                                              return alert;
                                            },
                                          );

                                        }, child: Text("Return", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),



                                        )

                                    )



                                    ,

                                  )

                                      :Container()







                                ],


                              ),




                            )

                        )




                    ;



                  }
              ),width: double.infinity,height: height/1.3, )


        ) ,










          ]),
        ));
  }

  cancelOrderItemQty(String orderdetailid,String qty) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.cancelOrderQty+"/"+orderdetailid+"/"+qty ),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );


    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {

      getOrderDetails(orderdata.orderid);
      getOrderInfo(orderdata.orderid);
    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }


  }

  cancelItem(String orderdetailid,int index) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.cancelOrderDetails+"/"+orderdetailid ),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );


    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {
      getCancelledDataCount(orderdata.orderid);

getOrderDetails(orderdata.orderid);
      getOrderInfo(orderdata.orderid);

    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }


  }

  returnItem(String orderdetailid,int index) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.updateReturnOrderStatus+"/"+orderdetailid ),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );


    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {

      getOrderDetails(orderdata.orderid);



      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Return request submitted. Our delivery boy will contact you"),
      ));

    }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }


  }

  getCancelledDataCount(String id) async{


    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getCancelledSalesOrderItems+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },



    );



   // _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

     print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
      {

        int cancelledcount=jsondata['cancelledcount'];


        if(cancelledcount==orderdetailscount)
          {


            cancelOrder(orderdata.orderid);


            // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
            //   content: Text('Full cancell count reached'),
            // ));

          }




      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }


  }

  cancelOrder(String id) async{


    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.cancelMyOrder+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);
    if(jsondata['status']==1)
    {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Success'),
      ));


      Navigator.pop(context, orderdata.orderid);


    }
    else   if(jsondata['status']==2){

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Your order already dispatched , you cannot cancel the order'),
      ));
    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }

  }


  getOrderInfo(String id)async{

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getOrderInfo+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    print(response);

    Orderinfo of=Orderinfo.fromJson(jsondata);

    if(of.status==1)
      {

        double total=double.parse(of.infodata.totalPrice);

        double deliv=double.parse(of.infodata.deleveryCharge);

        setState(() {
          if(total>0) {

            totalwithdeliverycharge = total + deliv;
            totalwithoutdeliverycharge=total;
            deliverycharge=deliv;
          }
          else{

            totalwithdeliverycharge=0;
            totalwithoutdeliverycharge=0;
            deliverycharge=0;
          }
        });

      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }


  }





  getOrderDetails(String id) async
  {

    // double total=double.parse(orderdata.totalPrice);
    //
    // double deliverycharge=double.parse(orderdata.deleveryCharge);
    //
    // setState(() {
    //   if(total>0) {
    //     totalwithdeliverycharge = total + deliverycharge;
    //   }
    //   else{
    //
    //     totalwithdeliverycharge=0;
    //   }
    // });




    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getSalesOrderItems+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },



    );



  //  _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

   // print(response);

    dynamic jsondata = jsonDecode(response);

    OrderDetailsData orderDetails=OrderDetailsData.fromJson(jsondata);

    if(orderDetails.status==1)
      {

        orderdetaildata.clear();

        orderdetailscount=orderDetails.detailsdata.length;

        for(int i=0;i<orderDetails.detailsdata.length;i++)
          {

            double p=double.parse(orderDetails.detailsdata[i].salespriWithGst);

            int qty=int.parse(orderDetails.detailsdata[i].qty);

            double tp=p*qty;
            orderDetails.detailsdata[i].TotalPricebyqty=tp.roundToDouble().toString();





          }



        setState(() {

          orderdetaildata.addAll(orderDetails.detailsdata);
        });




      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));
    }

  }

  showQuantityConfirmation(String qty)
  {
    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0):BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                child:Container(

                  child:   Text("Ordered quantity : "+qty, style: TextStyle(color: Colors.green,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:20 ),),



                )

            ),




            Padding(
              padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(15.0):EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: qtycontroller ,
                keyboardType: TextInputType.number,
                onChanged: (text){

                },



                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter quantity to cancel',


                ),
              )),
            ),
            Padding(
              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(25.0),

              child: Container(

                width:ResponsiveInfo.isMobile(context)?120: 200,
                height: ResponsiveInfo.isMobile(context)?50:90,
                decoration: BoxDecoration(

                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      if(qtycontroller.text.toString().isNotEmpty) {

                        // if(qtycontroller.text.toString().compareTo(otpcode.toString())==0)
                        // {
                        //   // Navigator.of(
                        //   //     context, rootNavigator: true)
                        //   //     .pop();
                        //
                        //   qtycontroller.text="";
                        //
                        //   Navigator.pop(context);
                        //
                        //   Navigator.push(
                        //       context, MaterialPageRoute(builder: (_) => ForgotpasswordPage(title: "Product", mobile: mobilenumber,)));
                        //
                        // }
                        // else{
                        //
                        //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     content: Text("incorrect otp code"),
                        //   ));
                        // }


                        //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter the quantity to cancel"),
                        ));
                      }


                    },

                    child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),










            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);
  }



}


class QuantityConfirmDialog extends StatefulWidget {

  String qty="0";


  QuantityConfirmDialog(this.qty);

  @override
  _MyDialogState createState() => new _MyDialogState(qty);
}

class _MyDialogState extends State<QuantityConfirmDialog> {

  String quantity="0";


  _MyDialogState(this.quantity);

  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";

  int neededqty=0;


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";
  TextEditingController qtycontroller=new TextEditingController();

  String data="";

  int updatedqty=0;


  int a=0;

  int orderedqty=0;

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();





    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                child:Container(

                  child:   Text("Ordered quantity : "+quantity, style: TextStyle(color: Colors.green,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:20 ),),



                )

            ),




            Padding(
              padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(15.0):EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: qtycontroller ,
                keyboardType: TextInputType.number,
                onChanged: (text){

                  if(text.isNotEmpty) {
                    a = int.parse(text);

                    orderedqty = int.parse(quantity);


                    if (a < orderedqty) {
                      updatedqty = orderedqty - a;
                      setState(() {
                        data = "You need only " + updatedqty.toString() +
                            " quantity";
                      });
                    }
                    else if (a == orderedqty) {
                      updatedqty = orderedqty - a;
                      setState(() {
                        data = "You are going to cancel whole ordered quantity";
                      });
                    }
                    else {
                      setState(() {
                        data =
                        "Cancelling quantity must be less than ordered quantity";
                      });
                    }
                  }
                  else{

           setState(() {


             data="";
           });
                  }





                },



                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter quantity to cancel',


                ),
              )),
            ),


            Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 0):EdgeInsets.fromLTRB(0, 20, 20, 0):EdgeInsets.fromLTRB(0, 25, 25, 0),

                child:Container(

                  child:   Text(data, style: TextStyle(color: Colors.green,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:20 ),),



                )

            ),






            Padding(
              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(25.0),

              child: Container(

                width:ResponsiveInfo.isMobile(context)?120: 200,
                height: ResponsiveInfo.isMobile(context)?50:90,
                decoration: BoxDecoration(

                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      if(qtycontroller.text.toString().isNotEmpty) {


                        if(a<orderedqty)
                        {

                          updatedqty=orderedqty-a;

                          Navigator.of(context).pop({'quantitytoupdate':updatedqty});

                        }
                        else if(a==orderedqty)
                        {

                          updatedqty=orderedqty-a;

                          Navigator.of(context).pop({'quantitytoupdate':updatedqty});
                        }
                        else{


                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Cancelling quantity must be less than ordered quantity"),
                          ));
                        }

                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter the quantity to cancel"),
                        ));
                      }


                    },

                    child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),


            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}