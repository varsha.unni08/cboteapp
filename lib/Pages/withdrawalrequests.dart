import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/WishlistProduct.dart';
import 'package:flutter_app_ecommerce/domain/WithdrawalRequests.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class WithdrawalPage extends StatefulWidget {
  final String title;

  const WithdrawalPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WithdrawalPage();
}

class _WithdrawalPage extends State<WithdrawalPage> {
  bool _switchValue = false;
  List<Requestdata> wpdata = [];

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  @override
  void initState() {
    // TODO: implement initState

    getWalletRequest();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "Withdrawal requests",
            style: TextStyle(
                fontSize: ResponsiveInfo.isMobile(context)
                    ? ResponsiveInfo.isSmallMobile(context)
                        ? 12
                        : 16
                    : 20,
                color: Colors.white),
          ),
          centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color: Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[
            ListView.builder(
                itemCount: wpdata.length,
                itemBuilder: (BuildContext context, int index) {
                  return Padding(
                      padding: ResponsiveInfo.isMobile(context)
                          ? ResponsiveInfo.isSmallMobile(context)
                              ? EdgeInsets.all(8)
                              : EdgeInsets.all(13)
                          : EdgeInsets.all(24),
                      child: Card(
                          elevation: 5,
                          shape: RoundedRectangleBorder(
                            side: BorderSide(color: Colors.white70, width: 0),
                            borderRadius: ResponsiveInfo.isMobile(context)
                                ? ResponsiveInfo.isSmallMobile(context)
                                    ? BorderRadius.circular(8)
                                    : BorderRadius.circular(12)
                                : BorderRadius.circular(15),
                          ),
                          child: Container(
                            height: ResponsiveInfo.isMobile(context)
                                ? ResponsiveInfo.isSmallMobile(context)
                                    ? 140
                                    : 170
                                : 220,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Padding(
                                        padding:
                                            ResponsiveInfo.isMobile(context)
                                                ? ResponsiveInfo.isSmallMobile(
                                                        context)
                                                    ? EdgeInsets.fromLTRB(
                                                        10, 10, 10, 0)
                                                    : EdgeInsets.fromLTRB(
                                                        16, 16, 16, 0)
                                                : EdgeInsets.fromLTRB(
                                                    22, 22, 22, 0),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          mainAxisAlignment: MainAxisAlignment.start,
                                          children: [
                                            Row(
                                              children: [
                                                Text(
                                                  "Amount : ",
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                ),
                                                Text(
                                                  wpdata[index].amount + " ₹",
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "Account number : ",
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                ),
                                                Text(
                                                  wpdata[index].accountnumber,
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "IFSC code : ",
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                ),
                                                Text(
                                                  wpdata[index].ifsc,
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                            Row(
                                              children: [
                                                Text(
                                                  "UPI ID : ",
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                ),
                                                Text(
                                                  wpdata[index].upiId,
                                                  style: TextStyle(
                                                      fontSize: ResponsiveInfo
                                                              .isMobile(context)
                                                          ? ResponsiveInfo
                                                                  .isSmallMobile(
                                                                      context)
                                                              ? 12
                                                              : 14
                                                          : 17,
                                                      color: Colors.black),
                                                )
                                              ],
                                            ),
                                            (wpdata[index]
                                                        .status
                                                        .compareTo("1") ==
                                                    0)
                                                ? Row(
                                                    children: [
                                                      Text(
                                                        "Transaction Id : ",
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      Text(
                                                        wpdata[index]
                                                            .transactionid,
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color:
                                                                Colors.black),
                                                      )
                                                    ],
                                                  )
                                                : Container(),
                                            (wpdata[index]
                                                        .status
                                                        .compareTo("0") ==
                                                    0)
                                                ? Row(
                                                    children: [
                                                      Text(
                                                        "Status : ",
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      Text(
                                                        "Processing",
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color: Colors.blue),
                                                      )
                                                    ],
                                                  )
                                                : Row(
                                                    children: [
                                                      Text(
                                                        "Status : ",
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color:
                                                                Colors.black),
                                                      ),
                                                      Text(
                                                        "Transffered",
                                                        style: TextStyle(
                                                            fontSize: ResponsiveInfo
                                                                    .isMobile(
                                                                        context)
                                                                ? ResponsiveInfo
                                                                        .isSmallMobile(
                                                                            context)
                                                                    ? 12
                                                                    : 14
                                                                : 17,
                                                            color:
                                                                Colors.green),
                                                      )
                                                    ],
                                                  )
                                          ],
                                        ))
                                  ],
                                ),
                              ],
                            ),
                          )));
                })
          ]),
        ));
  }

  getWalletRequest() async {
    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
      Uri.parse(UrlData.baseurl + UrlData.getWalletRquests),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },
    );

    //  _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);
    dynamic jsondata = jsonDecode(response);
    WithdrawalRequests wp = WithdrawalRequests.fromJson(jsondata);

    if (wp.status == 1) {
      setState(() {
        wpdata = wp.requestdata;
      });
    } else {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));
    }
  }
}
