import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/SubcategoryList.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class FilterPage extends StatefulWidget {
  final String title;
  final String categoryid;

  const FilterPage({Key? key, required this.title, required this.categoryid}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FilterPage(categoryid);
}

class _FilterPage extends State<FilterPage> {
  bool _switchValue = false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;
  List<String>st=[];
  double startprice=0;
  double endprice=0;

  String categoryid="";
  List<Subcategorydata> subcategorydata=[];

  TextEditingController minpricecontroller=new TextEditingController();
  TextEditingController maxpricecontroller=new TextEditingController();

int fcount=0;
  _FilterPage(this.categoryid);

  @override
  void initState() {
    // TODO: implement initState
getSubcategories();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;

    RangeValues _currentRangeValues = const RangeValues(40, 80);
    ;

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(

          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white,size:ResponsiveInfo.isMobile(context)?25:40 ,),
            onPressed: () => Navigator.of(context).pop(),
          ),


    flexibleSpace:Container(
      color: Color(0xfff55245),

    width: double.infinity,
    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,
      child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(30, 30, 10, 0):EdgeInsets.fromLTRB(60, 30, 20, 0):EdgeInsets.fromLTRB(60, 60,30, 0),

        child:  Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [

            Expanded(child: Text(
              "Filter",
              style: TextStyle(
                  fontSize: ResponsiveInfo.isMobile(context)
                      ? ResponsiveInfo.isSmallMobile(context)
                      ? 12
                      : 16
                      : 20,
                  color: Colors.white),
            ),flex: 2,),
            Expanded(child: InkWell(child: Icon(Icons.check,color: Colors.white,size:ResponsiveInfo.isMobile(context)?25:40 ,),onTap: (){

              st.clear();

              fcount=0;
if(subcategorydata.length>0)
  {
    for(Subcategorydata sbd in subcategorydata)
      {

        if(sbd.isSelected)
          {
            fcount++;

            st.add(sbd.id);


          }



      }


  }

if(minpricecontroller.text.trim().isNotEmpty) {
  startprice = double.parse(minpricecontroller.text.toString());
}

if(maxpricecontroller.text.trim().isNotEmpty) {
  endprice = double.parse(maxpricecontroller.text.toString());
              }


if(startprice<=endprice)
  {

    var mjobject=new Map();
    mjobject['subcategoryid']=st;
    mjobject['start_price']=startprice;
    mjobject['end_price']=endprice;

    var js=json.encode(mjobject);

    Navigator.pop(context,{"filterparameter":js,"fcount":fcount});






  }
else{

  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    content: Text("minimum price is must be less than maximum price"),
  ));

}








            },))



          ],


        ),

      )





    ),
    centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color: Color(0xffEBEBEB),
          child: Stack(children: <Widget>[




                Align(
                    alignment: Alignment.topLeft,
                    child: Padding(
                      padding: ResponsiveInfo.isMobile(context)
                          ? ResponsiveInfo.isSmallMobile(context)
                          ? EdgeInsets.fromLTRB(10, 10, 5, 5)
                          : EdgeInsets.fromLTRB(20, 20, 10, 10)
                          : EdgeInsets.fromLTRB(30, 30, 20, 20),
                      child: Text(
                        'Price',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: ResponsiveInfo.isMobile(context)
                                ? ResponsiveInfo.isSmallMobile(context)
                                ? 13
                                : 16
                                : 19),
                      ),
                    )),

          Padding(
              padding: ResponsiveInfo.isMobile(context)
                  ? ResponsiveInfo.isSmallMobile(context)
                  ? EdgeInsets.fromLTRB(10, 30, 5, 5)
                  : EdgeInsets.fromLTRB(20, 50, 10, 10)
                  : EdgeInsets.fromLTRB(30, 80, 20, 20),

               child: Row(

                    children: [


                      Expanded(child: Padding(
                  padding: ResponsiveInfo.isMobile(context)
                      ? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):
                  EdgeInsets.all(16):EdgeInsets.all(22),



                     child: Container(

                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                        ),
                        child:Padding(

                          padding: ResponsiveInfo.isMobile(context)
                              ? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):
                          EdgeInsets.all(8):EdgeInsets.all(12),
                          child:TextField(

                            keyboardType: TextInputType.number,
                            controller: minpricecontroller,

                            style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                            decoration: InputDecoration(
                              border: InputBorder.none,

                              hintText: 'Min',
                            ),

                          )


                        )



                        )

                      ),flex: 1,),
                      Expanded(child: Padding(
                          padding: ResponsiveInfo.isMobile(context)
                              ? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(10):
                          EdgeInsets.all(16):EdgeInsets.all(22),



                          child: Container(

                              decoration: BoxDecoration(
                                  border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                              ),
                              child:Padding(

                                  padding: ResponsiveInfo.isMobile(context)
                                      ? ResponsiveInfo.isSmallMobile(context)?EdgeInsets.all(5):
                                  EdgeInsets.all(8):EdgeInsets.all(12),
                                  child:TextField(

                                    keyboardType: TextInputType.number,
                                    controller: maxpricecontroller,
                                    style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,

                                      hintText: 'Max',
                                    ),

                                  )


                              )



                          )

                      ),flex: 1,),




                    ],



                  ),),






            Align(
              alignment: Alignment.topLeft,
              child:    Padding(
                  padding: ResponsiveInfo.isMobile(context)
                      ? ResponsiveInfo.isSmallMobile(context)
                      ? EdgeInsets.fromLTRB(10, 100, 5, 5)
                      : EdgeInsets.fromLTRB(20, 150, 10, 10)
                      : EdgeInsets.fromLTRB(30, 210, 20, 20),
                  child: Text(
                    'Subcategory',
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: ResponsiveInfo.isMobile(context)
                            ? ResponsiveInfo.isSmallMobile(context)
                            ? 13
                            : 16
                            : 19),
                  ),
                ),),

             Padding(
                    padding: ResponsiveInfo.isMobile(context)
                        ? ResponsiveInfo.isSmallMobile(context)
                        ? EdgeInsets.fromLTRB(10, 130, 5, 5)
                        : EdgeInsets.fromLTRB(20, 180, 10, 10)
                        : EdgeInsets.fromLTRB(30, 260, 20, 20),
                    child: ListView.builder(
                        itemCount: subcategorydata.length,
                        itemBuilder: (BuildContext context,int index){
                          return ListTile(
                              leading: Checkbox(
                                checkColor: Colors.blueGrey,

                                value: subcategorydata[index].isSelected,
                                onChanged: (bool? value) {
                                  setState(() {
                                    subcategorydata[index].isSelected = value!;
                                  });
                                },
                              ),

                              title:Text(subcategorydata[index].subCatname,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?14:20,color: Colors.black),)
                          );
                        }
                    ),
                  ),



















          ]),
        ));
  }



   void getSubcategories() async
  {

    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");

    final datacount = await SharedPreferences.getInstance();
    var date = new DateTime.now().toIso8601String();
    var dataasync = await http.get(
      Uri.parse(UrlData.baseurl + UrlData.getSubcategories+"/"+categoryid),
      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
      },
    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;

    SubcategoryList sub=SubcategoryList.fromJson(jsonDecode(response));

    if(sub.status==1)
      {


        setState(() {

          subcategorydata.clear();
          subcategorydata.addAll(sub.subcategorydata);

        });


      }



  }
}
