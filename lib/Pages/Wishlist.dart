import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/WishlistProduct.dart';



import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class WishlistPage extends StatefulWidget {
  final String title;



  const WishlistPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WishlistPage();
}

class _WishlistPage extends State<WishlistPage> {

  bool _switchValue=false;
  List<Data>wpdata=[];

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  @override
  void initState() {
    // TODO: implement initState

getWishlist();

    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("Wishlist",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.white),),
          centerTitle: false,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[


          ListView.builder(

            itemCount: wpdata.length,
            itemBuilder: (BuildContext context, int index) {
              return Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8):EdgeInsets.all(13):EdgeInsets.all(24),

                child:Card(

                    elevation: 5,

                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white70, width: 0),
                      borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                    ),

                    child:    Container(
                      height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?180:200:250,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Row(

                            children: [

                              Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(18),

                                child:(wpdata[index].image1!=null&&wpdata[index].image1.isNotEmpty)? Image.network(UrlData.productimageurl+wpdata[index].image1,
                                  width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,fit: BoxFit.fill,
                                  errorBuilder:
                                      (BuildContext context, Object exception, StackTrace? stackTrace) {
                                    return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                                    );
                                  },
                                ):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                                ),

                              ),

                              Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 0):EdgeInsets.fromLTRB(0, 16, 0, 0):EdgeInsets.fromLTRB(0, 22, 0, 0),

                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  mainAxisAlignment: MainAxisAlignment.center,

                                  children: [


                                    SizedBox(


                                      child:  Text(wpdata[index].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),
                                      width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?130:170:200 ,
                                    ),


                                    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10):EdgeInsets.all(15),
                                        child:Row(
                                          crossAxisAlignment: CrossAxisAlignment.center,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          children: [

                                            (wpdata[index].isOffer.compareTo("1")==0) ?   Padding(padding: EdgeInsets.all(5),

                                              child: Text("₹ "+wpdata[index].price,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),

                                              )
                                              ,):Padding(padding: EdgeInsets.all(5),

                                              child: Text("₹ "+wpdata[index].price,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                              )
                                              ,)
                                            ,
                                            (wpdata[index].isOffer.compareTo("1")==0) ?   Padding(padding: EdgeInsets.all(5),

                                              child: Text("₹ "+wpdata[index].discountedPrice,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                                              )
                                              ,):Container()
                                          ],


                                        )
                                    ),




                                  ],


                                )


                              )






                            ],


                          ),
                          
                          Align(
                            
                            alignment: Alignment.bottomRight,
                            child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.fromLTRB(0, 0, 10, 0):EdgeInsets.fromLTRB(0, 0, 20, 0):EdgeInsets.fromLTRB(0, 0, 25, 0),

                              child:TextButton(onPressed: () {

                                Widget yesButton = TextButton(
                                  child: Text("Yes"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    removeProductToWishList(wpdata[index].stockid,index);
                                  },
                                );

                                Widget noButton = TextButton(
                                  child: Text("No"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                );

                                // set up the AlertDialog
                                AlertDialog alert = AlertDialog(
                                  title: Text("Kolica"),
                                  content: Text("Do you want to remove product from wishlist ?"),
                                  actions: [yesButton, noButton],
                                );

                                showDialog(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return alert;
                                  },
                                );







                              }, child: Text("Delete", style: TextStyle(color: Colors.redAccent,fontSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:20 ),),



                              )

                            )



                            ,
                            
                          )





                        ],


                      ),




                    )

                )

              )


                ;



            }
        )





          ]),
        ));
  }

  removeProductToWishList(String stockid,int index) async{
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.deleteFromWishlist),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: {

          'stockid':stockid
        }


    );
    _progressDialog.dismissProgressDialog(context);
    String response = dataasync.body;
    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {



setState(() {

  wpdata.removeAt(index);
});


    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }
  }

  getWishlist() async {
    final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
    // _progressDialog.showProgressDialog(
    //     context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.getWishlist),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );


    //  _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);
    dynamic jsondata = jsonDecode(response);
    WishlistProduct wp=WishlistProduct.fromJson(jsondata);

    if(wp.status==1)
      {
        setState(() {

          wpdata=wp.data;
        });


      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No data found"),
      ));


    }




  }


}


