import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_ecommerce/Database/DatabaseHelper.dart';
import 'package:flutter_app_ecommerce/Database/DatabaseTables.dart';
import 'package:flutter_app_ecommerce/Pages/Cart.dart';
import 'package:flutter_app_ecommerce/Pages/FilterPage.dart';
import 'package:flutter_app_ecommerce/Pages/MyAddress.dart';
import 'package:flutter_app_ecommerce/Pages/OrderDetails.dart';
import 'package:flutter_app_ecommerce/Pages/productpages/DescriptionPage.dart';
import 'package:flutter_app_ecommerce/Pages/productpages/RelatedPage.dart';
import 'package:flutter_app_ecommerce/Pages/productpages/ReviewPage.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/ProductDatabyCategory.dart';
import 'package:flutter_app_ecommerce/domain/ProductDetailsWeb.dart';

import 'package:flutter_app_ecommerce/domain/ProductRating.dart';
import 'package:flutter_app_ecommerce/domain/ProductSpec.dart';
import 'package:flutter_app_ecommerce/domain/RelatedProduct.dart';

import 'package:page_indicator/page_indicator.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';


import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

import 'SearchPage.dart';

class ProductDetailsPage extends StatefulWidget {
  final String title;
  final String id;
  final String stockid;





  const ProductDetailsPage({Key? key, required this.title, required this.id, required this.stockid}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProductDetailsPage(id,stockid);
}

class _ProductDetailsPage extends State<ProductDetailsPage> with TickerProviderStateMixin {

  late TabController tabController;
  bool _switchValue=false;
  List<Widget> nsdwidget = [];
  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  List<String> sliderurl = [
  ];

   String id="";
   String stockid="";

   int selectedindex=0;

  List<Specdata> _specdata=[];
   String productname="",price="",description="";
  List<Ratingdata> ratingdata=[];

 late ProductDetailsWeb productDetails;

 double avgrating=0;
 String subcatid="0";

  List<Relateddata>  prdc=[];
  int qty=1;

  bool iswishlist=false;

  double singleprice=0;
double myrating=0;
 List<String>headingdata=["Specification","Reviews","Related"];

    List<Widget> _pages = <Widget>[
    MyDescriptionPage(title: 'home'),
    MyReviewPage(title: "Network"),
    RelatedPage(title: "report"),

  ];


    TextEditingController tcd=new TextEditingController();

  _ProductDetailsPage(this.id, this.stockid);

  int _selectedIndex=0;

  String shopid="0",productid="0";

  @override
  void initState() {
    // TODO: implement initState

    tabController = TabController(
      initialIndex: 0,
      length: 3,
      vsync: this,
    );

    getProductDetails();
    getProductSpec();
    getRatings();
    checkProductWishlist();


    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    for (var i = 0; i < sliderurl.length; i++) {



      nsdwidget.add(Container(
        height: (MediaQuery.of(context).size.width) / 1.3,
        width: double.infinity,

            child:Column(

              children: [



                Align(
                  alignment: Alignment.topLeft,

                  child:   Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(10):EdgeInsets.all(16):EdgeInsets.all(22),



                      child:  Text(productname,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?15:18:22,color: Colors.black),)),
                ),
                Align(
                  alignment: Alignment.topLeft,
 child: Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(10):EdgeInsets.all(16):EdgeInsets.all(22),
   child:  Container(



                        child:    AbsorbPointer(child:  RatingBar.builder(
                          initialRating: avgrating,
                          minRating: 1,
                          direction: Axis.horizontal,
                          allowHalfRating: true,
                          itemCount: 5,
                          itemSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? 10:15: 20,

                          itemPadding: EdgeInsets.symmetric(horizontal: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?2.0:3:6),
                          itemBuilder: (context, _) => Icon(
                            Icons.star,size: 3,
                            color: Colors.amber,
                          ),
                          onRatingUpdate: (rating) {
                            print(rating);
                          },
                        )),

                      ))






                )

          ,



                Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(10):EdgeInsets.all(16):EdgeInsets.all(22),



                    child: (sliderurl[i]!=null&&sliderurl[i].isNotEmpty)? Image.network(UrlData.productimageurl+sliderurl[i],
                      width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:90:160,fit: BoxFit.fill,
                      errorBuilder:
                          (BuildContext context, Object exception, StackTrace? stackTrace) {
                        return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                        );
                      },
                    ):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 45:80:120,fit: BoxFit.fill
                    ),),


                (productDetails.data.isOffer.compareTo("1")==0)? Row(

                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,

                  children: [

                    Padding(padding: EdgeInsets.all(5),

                      child: Text("₹ "+productDetails.data.mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),

                      )
                      ,)
                    ,
                    Padding(padding: EdgeInsets.all(5),

                      child: Text("₹ "+productDetails.data.salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                      )
                      ,)
                  ],


                ):Center(

                  child:      Padding(padding: EdgeInsets.all(5),

                    child: Text("₹ "+productDetails.data.mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),

                    )
                    ,)

                )






              ],


            )


         ));
    }



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:
        PreferredSize(
            preferredSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?Size.fromHeight(60.0):Size.fromHeight(80.0): Size.fromHeight(100.0),
            child:


            AppBar(

              backgroundColor: Color(0xfff55245),
              flexibleSpace: Container(

                  color: Color(0xfff55245),
                  child: Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?EdgeInsets.fromLTRB(60, 35, 50, 10) : EdgeInsets.fromLTRB(60, 40, 50, 10): EdgeInsets.fromLTRB(60, 60, 50, 10),

                    child: Container(



                        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?40:50:80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(color: Colors.white, spreadRadius: 3),
                          ],
                        ),

                        child:
                        GestureDetector(
                            onTap: (){

                              Navigator.push(
                                  context, MaterialPageRoute(builder: (_) => SearchPage(title: "Cart",)));


                            },

                            child: Row(
                              textDirection: TextDirection.ltr,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[

                                Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                                    child:

                                    ResponsiveInfo.isMobile(context)? Image.asset("images/search.png",width: 25,height: 25,

                                    ):Image.asset("images/search.png",width: 45,height: 45,

                                    )
                                )
                                ,
                                Expanded(child: new Theme(data: new ThemeData(
                                    hintColor: Colors.black45
                                ), child: Text("Search ...",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?14:19 ))))
                              ],
                            )
                        )




                    ),


                  )



              ),
              elevation: 0,
            )),
        body:

            Column(

              children: [

                Flexible(child:  Stack(
                  children: [
                    Container(
                      width: double.infinity,
                      height: double.infinity,
                      color: Color(0xffEFECEC),

                    ),

                    SingleChildScrollView(

                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,

                        children: [
                          Align(
                              alignment: Alignment.topRight,
                              child:Padding(

                                  padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(10):EdgeInsets.all(18),
                                  child:GestureDetector(

                                      onTap: (){

                                        if(iswishlist)
                                          {
                                            removeProductToWishList();
                                          }
                                        else {
                                          addProductToWishList();
                                        }
                                      },

                                      child:(iswishlist)?Icon( Icons.favorite,
                                        color:Colors.redAccent,

                                        size:ResponsiveInfo.isMobile(context)? 24.0:36.0,):

                                      Icon( Icons.favorite,
                                        color:Colors.black26,

                                        size:ResponsiveInfo.isMobile(context)? 24.0:36.0,)
                                  )
                              )



                          ),

                          Card(

                            child:Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(5):EdgeInsets.all(8):EdgeInsets.all(15),
                              child: Container(
                                  height: (MediaQuery.of(context).size.width) / 1.3,
                                  width: double.infinity,
                                  child: PageIndicatorContainer(
                                      length: sliderurl.length,
                                      align: IndicatorAlign.bottom,
                                      indicatorSpace: 14.0,
                                      padding: const EdgeInsets.all(10),
                                      indicatorColor: Colors.black12,
                                      indicatorSelectorColor: Colors.blueGrey,
                                      shape: IndicatorShape.circle(size: 10),
                                      child: PageView.builder(
                                        scrollDirection: Axis.horizontal,
                                        // controller: controller,
                                        itemBuilder: (BuildContext context, int index) {
                                          return getNetWorkWidget(index);
                                        },
                                        itemCount: sliderurl.length,
                                        // children: nsdwidget,
                                      ))),
                            )
                          )

                          ,









                 SizedBox(child: Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(15,5,15,5):EdgeInsets.fromLTRB(21,10,21,10):EdgeInsets.fromLTRB(27,10,27,10),
                    child: ListView(
                      scrollDirection: Axis.horizontal,
                      children: List.generate(
                        headingdata.length,
                            (i) => GestureDetector(
                          child:Padding(padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(5):EdgeInsets.all(15),
                              child:Column(
                                children: [
                                  Container(child:   Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                                      child:  Container(

                                        width:ResponsiveInfo.isMobile(context)? 120:200,
                                        height:ResponsiveInfo.isMobile(context)? 50:90,
                                        decoration: BoxDecoration(
                                            color:(selectedindex==i)? Color(0xfff55245):Colors.white60,
                                            border: Border.all(
                                              color: Color(0xfff55245),
                                            ),
                                            borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                                        ),
                                        child: TextButton(onPressed: () {


                                          setState(() {

                                            selectedindex=i;
                                          });



                                          // Navigator.pushReplacement(context, MaterialPageRoute(
                                          //     builder: (context) => MyHomePage(title: "Signup",)
                                          // )
                                          // );


                                        },
                                          child: Text(headingdata[i],style: TextStyle(color: (selectedindex==i)?Colors.white:Colors.black,fontSize:ResponsiveInfo.isMobile(context)? 13:19),),





                                        ),




                                      ))),

                                ],
                              )
                          ),
                          onTap: (){

                            setState(() {

                              selectedindex=i;
                            });



                          },
                        ),
                      ),
                    )),height:ResponsiveInfo.isMobile(context)?100:150 ,)

                    ,


                          (selectedindex==0)?Column(

                            crossAxisAlignment: CrossAxisAlignment.start,
                           mainAxisAlignment: MainAxisAlignment.start,

                           children: [
                             Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(15,5,15,5):EdgeInsets.fromLTRB(21,10,21,10):EdgeInsets.fromLTRB(27,10,27,10),
                                 child: (description.isNotEmpty)? Text(description,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?14:20),):Container())
                             ,
                             Container(

                                 child:Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(15,5,15,5):EdgeInsets.fromLTRB(21,10,21,10):EdgeInsets.fromLTRB(27,10,27,10),

                                     child:ListView.builder(
                                       physics: BouncingScrollPhysics(),

                                       shrinkWrap: true,

                                       itemCount: _specdata.length,
                                       itemBuilder: (context, i) {
                                         return  Padding(padding:  EdgeInsets.all(5),
                                             child:Row(
                                               children: [

                                                 Text(_specdata[i].specHead+" : ",style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54)     :TextStyle(fontSize: 22,color: Colors.black),)

                                                 ,
                                                 Text(_specdata[i].specValue,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54)     :TextStyle(fontSize: 22,color: Colors.black),)

                                               ],
                                             )
                                         )



                                         ;


                                       },
                                     )

                                 )






                             )

                           ],


                         ):(selectedindex==1)?
                            
                           

                         
                                                            
                                Column(

                                    children:[

                                      Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(5):EdgeInsets.all(12),

                                        child: TextButton(onPressed: () {


                                          showPostReviewDialog();

                                        },
                                        child: Text("Post your rating",style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)?14:19 ),),),


                                      ),




                                    Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(15,5,15,5):EdgeInsets.fromLTRB(21,10,21,10):EdgeInsets.fromLTRB(27,10,27,10),

                                        child:ListView.builder(
                                          physics: BouncingScrollPhysics(),

                                          shrinkWrap: true,

                                          itemCount: ratingdata.length,
                                          itemBuilder: (context, i) {
                                            return  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(12),
                                                child:Card(

                                                shape: RoundedRectangleBorder(
                                                borderRadius: BorderRadius.circular(10),
                                                ),


                                               child: Row(
                                                  children: [

                                                    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(5):EdgeInsets.all(8):EdgeInsets.all(11),
                                                      child: (ratingdata[i].image!=null&&ratingdata[i].image.isNotEmpty)? ClipOval(child:Image.network(UrlData.imageurl+ratingdata[i].image,
                                                        width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,fit: BoxFit.fill,
                                                        errorBuilder:
                                                            (BuildContext context, Object exception, StackTrace? stackTrace) {
                                                          return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,fit: BoxFit.fill
                                                          );
                                                        },
                                                      )):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?40:60:90,fit: BoxFit.fill
                                                      ),),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      mainAxisAlignment: MainAxisAlignment.start,
                                                      children: [
                                                        Text(ratingdata[i].customerName,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54)     :TextStyle(fontSize: 22,color: Colors.black),)
                                                        ,
                                                        Text(ratingdata[i].description,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54)     :TextStyle(fontSize: 22,color: Colors.black),),
                                                        AbsorbPointer(child:  RatingBar.builder(
                                                          initialRating: double.parse(ratingdata[i].rating),
                                                          minRating: 1,
                                                          direction: Axis.horizontal,
                                                          allowHalfRating: true,
                                                          itemCount: 5,
                                                          itemSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? 10:15: 20,
                                                          itemPadding: EdgeInsets.symmetric(horizontal: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?2.0:3:6),
                                                          itemBuilder: (context, _) => Icon(
                                                            Icons.star,size: 3,
                                                            color: Colors.amber,
                                                          ),
                                                          onRatingUpdate: (rating) {
                                                            print(rating);
                                                          },
                                                        ))
                                                      ],
                                                    )
                                                  ],

                                                )
                                                )


                                            )



                                            ;


                                          },
                                        )

                                    )




]

                                

                             


                           









                          ):Container(child:
                         (prdc.length>0)?


                         Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.fromLTRB(15,5,15,5):EdgeInsets.fromLTRB(21,10,21,10):EdgeInsets.fromLTRB(27,10,27,10),

                             child:ListView.builder(
                               physics: BouncingScrollPhysics(),

                               shrinkWrap: true,

                               itemCount: prdc.length,
                               itemBuilder: (context, i) {
                                 return
                                   GestureDetector(

                                     onTap: (){

                                   Navigator.push(
                                       context, MaterialPageRoute(builder: (_) => ProductDetailsPage(title: "ProductDetails", id: prdc[i].productid, stockid: prdc[i].stockid,)));

                                 },



                                 child:  Card(

                                     shape: RoundedRectangleBorder(
                                     borderRadius: BorderRadius.circular(10),
                                 ),


                                  child:


                               Container(width: double.infinity,height: ResponsiveInfo.isMobile(context)?160:250,

                                   child: Row(

                                     children: [

                                       Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? EdgeInsets.all(5):EdgeInsets.all(8):EdgeInsets.all(11),
                                         child: (prdc[i].image1!=null&&prdc[i].image1.isNotEmpty)? ClipOval(child:Image.network(UrlData.productimageurl+prdc[i].image1,
                                           width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,fit: BoxFit.fill,
                                           errorBuilder:
                                               (BuildContext context, Object exception, StackTrace? stackTrace) {
                                             return Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90,fit: BoxFit.fill
                                             );
                                           },
                                         )):Image.asset("images/pi.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 40:60:90 ,height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?40:60:90,fit: BoxFit.fill
                                         ),),
                                       Column(
                                         crossAxisAlignment: CrossAxisAlignment.center,
                                         mainAxisAlignment: MainAxisAlignment.center,
                                         children: [

                                   SizedBox(


                                 child:  Text(prdc[i].productName,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:14:17,color: Colors.black),maxLines: 3,),
                                     width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?130:170:200 ,
                                   ),


                                           Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),
                                               child:Row(
                                                 crossAxisAlignment: CrossAxisAlignment.center,
                                                 mainAxisAlignment: MainAxisAlignment.center,
                                                 children: [
                                                   Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),
                                                     child: Text("₹ "+prdc[i].mrp,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black54,decoration: TextDecoration.lineThrough,)     :TextStyle(fontSize: 22,color: Colors.black54,decoration: TextDecoration.lineThrough),
                                                     )
                                                     ,)
                                                   ,
                                                   Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(5):EdgeInsets.all(10),
                                                     child: Text("₹ "+prdc[i].salespriWithGst,style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 13,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),
                                                     )
                                                     ,)
                                                 ],
                                               )
                                           ),
                                           (double.parse(prdc[i].cashbackPercent)>0)? Container(
                                             width:ResponsiveInfo.isMobile(context)?120:160,
                                             height:ResponsiveInfo.isMobile(context)? 50:80,
                                             color: Color(0xffFCE8E7),
                                             child: Center(
                                                 child:Text(prdc[i].cashbackPercent+" % Cashback",maxLines:1,style: TextStyle(fontSize:ResponsiveInfo.isMobile(context)? 12:20,color: Colors.black),)
                                             ),
                                           ):Container()

                                         ],
                                       )



                                     ],




                                   ),



                                 )



                                 )

                                   )

                                 ;


                               },
                             )

                         )


                          : Container()



                            ,)




                        ],



                      ),



                    )










                  ],







                ),flex: 4,),

    Flexible(child:Row(
      mainAxisAlignment: MainAxisAlignment.center,
    crossAxisAlignment: CrossAxisAlignment.center,

    children: [
      
      Padding(padding:  ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 35,10, 20):EdgeInsets.fromLTRB(15, 50,15, 30):EdgeInsets.fromLTRB(25, 90,25, 50),

      child: Container(
        height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 60:85:130,
        child:Text("₹ "+price,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?17:19:24,color: Colors.black),) ,





      ),


      ),
    Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  EdgeInsets.fromLTRB(10, 25, 5, 20):EdgeInsets.fromLTRB(15, 35, 10, 30):EdgeInsets.fromLTRB(15, 55, 15, 40),

    child:Container(
      height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?  40:60:90,
        decoration: BoxDecoration(

            border: Border.all(
              color: Color(0xff3d3b3a),
            ),
            borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?15:30))


        ),
      child: Row(

        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [

          TextButton(onPressed: () {

decreaseQty();
          },
          child: Text("-",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),

          ),
          Text(qty.toString()+"",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Color(0xfff55245)),),
          TextButton(onPressed: () {

            increaseQty();
          },
            child: Text("+",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),

          ),




        ],




      ),







    )


    ),







      


    ],
    )



    ,flex: 1),



                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [

                    Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 10):EdgeInsets.fromLTRB(0, 20, 20, 20),
                        child:  Container(

                          width:ResponsiveInfo.isMobile(context)? 120:200,
                          height:ResponsiveInfo.isMobile(context)? 50:90,
                          decoration: BoxDecoration(
                              color: Color(0xfff55245),
                              border: Border.all(
                                color: Color(0xfff55245),
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                          ),
                          child: TextButton(onPressed: () {



                            checkStockAvailability(1);




                          },
                            child: Text("Buy now",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                          ),




                        )),
                    Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 10, 10):EdgeInsets.fromLTRB(0, 20, 20, 20),
                        child:  Container(

                          width:ResponsiveInfo.isMobile(context)? 120:200,
                          height:ResponsiveInfo.isMobile(context)? 50:90,
                          decoration: BoxDecoration(
                              color: Color(0xfff55245),
                              border: Border.all(
                                color: Color(0xfff55245),
                              ),
                              borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                          ),
                          child: TextButton(onPressed: () {



                            // Navigator.pushReplacement(context, MaterialPageRoute(
                            //     builder: (context) => MyHomePage(title: "Signup",)
                            // )
                            // );

                            checkStockAvailability(0);


                          },
                            child: Text("Add to cart",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                          ),




                        )),
                  ],


                )



              ],

            )



       );

  }



  increaseQty()
  {

    double d=singleprice;
    setState(() {
      qty=qty+1;

      double a=d*qty;
      price=a.roundToDouble().toString();

    });
  }

  decreaseQty()
  {
    double d=singleprice;
    if(qty>1) {

      setState(() {

        qty = qty - 1;
        double a=d*qty;
        price=a.roundToDouble().toString();
      });

    }
    else{


    }
  }

  void _onItemTap(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  Widget getNetWorkWidget(int p) {
    return nsdwidget[p];
  }


  checkStockAvailability(int code) async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl + UrlData.checkStockAvailability + "/" + stockid + "/" +
          qty.toString()+"/"+shopid+"/"+productid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!
      },


    );


    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
      {

        if(code==0) {
          addtoCart(0);
        }
        else{

          addtoCart(1);
        }

      }
    else{
      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));


    }


  }

  addtoCart(int code) async{

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.addtoCart),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },
      body:{

        'stockid':stockid,
        'quantity':qty.toString()


      }


    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
      {

        int cartid=jsondata['cartid'];

        if(code==1)
          {

            Navigator.push(
                context,
                MaterialPageRoute(builder: (_) => MyAddressPage(title: "Cart", cartid: cartid.toString(), code: '0',)));
          }
        else {
          Navigator.push(
              context,
              MaterialPageRoute(builder: (_) => CartPage(title: "Cart",)));
        }
      }
    else{

      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));

    }


  }



  getProductDetails() async
  {

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getProductDetails+"/"+id+"/"+stockid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },


    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;





    print(response);

    dynamic jsondata = jsonDecode(response);

    ProductDetailsWeb productDetailsWeb=ProductDetailsWeb.fromJson(jsondata);


    List<String>sr=[];

    if(productDetailsWeb.status==1)
      {

        List<Map<String, dynamic>> a =
        await new DatabaseHelper().queryAllRows(DatabaseTables.History);


        String idp="0";

        bool isHistoryexist=false;

        for (Map ab in a) {

          print(ab.length.toString()+" \n");
          print("data \n");


          int id = ab["keyid"];
          String data = ab["data"];

          var jsondata = jsonDecode(data);

          idp=jsondata['id'];

          print(jsondata);


          if(idp.compareTo(productDetailsWeb.data.id)==0) {

            isHistoryexist=true;
            break;

          }



        }

        if(!isHistoryexist) {
          Map<String, dynamic> assetdata = new Map();
          assetdata['id'] = productDetailsWeb.data.productid;
          assetdata['product'] = response;
          var js = json.encode(assetdata);
          Map<String, dynamic> data_To_Table = new Map();
          data_To_Table['data'] = js.toString();
          new DatabaseHelper().insert(data_To_Table, DatabaseTables.History);
        }


        String av=productDetailsWeb.avgrating;

        subcatid=productDetailsWeb.data.subCatid;
        shopid=productDetailsWeb.data.shopId;
        productid=productDetailsWeb.data.productid;
        getRelatedProducts(subcatid);
        sr.add(productDetailsWeb.data.image1);
        sr.add(productDetailsWeb.data.image2);
        sr.add(productDetailsWeb.data.image3);
        sr.add(productDetailsWeb.data.image4);
        sr.add(productDetailsWeb.data.image5);



        setState(() {



          productDetails=productDetailsWeb;

          productname=productDetailsWeb.data.productName;
          description=productDetailsWeb.data.description;


          sliderurl.addAll(sr);

          avgrating=double.parse(av);

          if(double.parse(productDetailsWeb.data.discountPercent)>0) {
            price = productDetailsWeb.data.salespriWithGst;
            singleprice=double.parse(price);
          }
          else{
            price = productDetailsWeb.data.mrp;

            singleprice=double.parse(price);

          }


        });






      }



  }

  showPostReviewDialog()
  {


    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0):BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            RatingBar.builder(
              initialRating: 0,
              minRating: 1,
              direction: Axis.horizontal,
              allowHalfRating: true,
              itemCount: 5,
              itemSize:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ? 25:45: 70,

              itemPadding: EdgeInsets.symmetric(horizontal: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context) ?2.0:3:6),
              itemBuilder: (context, _) => Icon(
                Icons.star,size: 20,
                color: Colors.amber,
              ),
              onRatingUpdate: (rating) {
                print(rating);
                myrating=rating;




              },
            ),
            Padding(
              padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(15.0):EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(

                keyboardType: TextInputType.text,
                controller: tcd,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Description',


                ),
              )),
            ),
            Padding(
              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(25.0),

              child: Container(

                width:ResponsiveInfo.isMobile(context)?120: 200,
                height: ResponsiveInfo.isMobile(context)?50:90,
                decoration: BoxDecoration(

                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      if(myrating>0)
                      {

postRating(myrating, tcd.text.toString());
tcd.text="";
Navigator.pop(context);

                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Choose a rating"),
                        ));
                      }


                        // if(otpcodecontroller.text.toString().compareTo(otpcode.toString())==0)
                        // {
                        //   // Navigator.of(
                        //   //     context, rootNavigator: true)
                        //   //     .pop();
                        //
                        //   otpcodecontroller.text="";
                        //
                        //   Navigator.pop(context);
                        //
                        //   Navigator.push(
                        //       context, MaterialPageRoute(builder: (_) => ForgotpasswordPage(title: "Product", mobile: mobilenumber,)));
                        //
                        // }
                        // else{
                        //
                        //   ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        //     content: Text("Please enter otp code"),
                        //   ));
                        // }


                        //   checkMobileNumber(otpcodeController.text.toString());



                    },

                    child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),










            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);

  }



  postRating(double rating,String description) async
  {
    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.postRating),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },
      body: {

        'stockid': stockid,
        'rating': rating.toString(),
        'description':description

      }


    );

    String response = dataasync.body;
    print(response);

    dynamic jsondata = jsonDecode(response);


    if(jsondata['status']==1)
      {



      }
    else{

      String msg=jsondata['message'];

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(msg),
      ));
    }


    getRatings();
  }



  getRelatedProducts(String subid) async
  {
    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getRelatedProduct+"/"+subid),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },


    );

    String response = dataasync.body;
    print(response);
    dynamic jsondata = jsonDecode(response);
    RelatedProduct related=RelatedProduct.fromJson(jsondata);
    if(related.status==1)
    {

      setState(() {


        prdc=related.relateddata;
      });


    }
    else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No related products found"),
      ));

    }

  }

  getProductSpec() async
  {
    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getProductSpec+"/"+id),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },


    );

    String response = dataasync.body;
    print(response);
    dynamic jsondata = jsonDecode(response);
   ProductSpec psc=ProductSpec.fromJson(jsondata);
   if(psc.status==1)
     {

       setState(() {

         _specdata.addAll(psc.specdata);
       });


     }
   else{


   }






  }




  getRatings()async
  {
    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.getRating),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },

      body: {

        'stockid':stockid
      }


    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    ProductRating productRating=ProductRating.fromJson(jsondata);
    
    if(productRating.status==1)
      {
        ratingdata.clear();
        
        



        setState(() {

          //avgrating=a;
          ratingdata.addAll(productRating.ratingdata);

        });

        
      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No rating found"),
      ));
    }

  }

  Future<double>getAverageRating() async{


      final datastorage = await SharedPreferences.getInstance();

      var dataasync = await http.post(
          Uri.parse(UrlData.baseurl+UrlData.getAverageRating),

          headers: <String, String>{
            'Content-Type': 'application/x-www-form-urlencoded',
            UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

          },

          body: {

            'stockid':stockid
          }


      );

      String response = dataasync.body;

      dynamic jsondata = jsonDecode(response);
      double av=0;

      if(jsondata['status']==1)
        {

          String avgrate=jsondata['average_rating'];

           av=double.parse(avgrate);




        }
      else{

        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("No data found"),
        ));
      }

return av;

  }

  removeProductToWishList() async{

    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.deleteFromWishlist),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: {

          'stockid':stockid
        }


    );

    String response = dataasync.body;
    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {



      checkProductWishlist();


    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }
  }

  addProductToWishList() async{

    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.addToWishList),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: {

          'stockid':stockid
        }


    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {



      checkProductWishlist();


    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));
    }
  }

  removeFromWishlist() async{



  }


  checkProductWishlist() async{


    final datastorage = await SharedPreferences.getInstance();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.checkProductWishlist),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: {

          'stockid':stockid
        }


    );

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1)
    {


setState(() {

  iswishlist=true;
});



    }
    else{

      setState(() {

        iswishlist=false;
      });
    }

  }


}


