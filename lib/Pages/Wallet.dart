import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/OrderDetails.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/WalletTransfer.dart';
import 'package:flutter_app_ecommerce/domain/branch_i_f_s_c.dart';


import 'dart:ui' as ui;

import 'package:shared_preferences/shared_preferences.dart';


import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';
import 'package:intl/intl.dart';


import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class WalletPage extends StatefulWidget {
  final String title;



  const WalletPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _WalletPage();
}

class _WalletPage extends State<WalletPage> {

  bool _switchValue=false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];

  TextEditingController amountcontroller=new TextEditingController();

  String balance=" 0";
  String balanceamount="0";

  List<Data> transaction_data=[];

  String selectedmonth="",selectedyear="";

  String selectedperiodData="";

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  @override
  void initState() {
    // TODO: implement initState
    selectedyear=  DateTime.now().year.toString();
    selectedmonth= DateTime.now().month.toString();


    getWalletBalance();
    getTransactions();



    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;
    Orientation orientation = MediaQuery.of(context).orientation;

    double width = screenSize.width;
    double height = screenSize.height;



    return Scaffold(

        resizeToAvoidBottomInset: true,

        appBar:  AppBar(
          backgroundColor: Color(0xffEBEBEB),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.black54),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text("My Wallet",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:16:20,color: Colors.black),),
          centerTitle: true,
          elevation: 0,
        ),
        body: Container(
          height: double.infinity,
          width: double.infinity,
          color:Color(0xffEBEBEB),
          child: Stack(alignment: Alignment.topLeft, children: <Widget>[


          Padding(padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 5, 10, 10):EdgeInsets.fromLTRB(15, 10, 15, 20):EdgeInsets.fromLTRB(25, 15, 25, 30)

              ,child: Card(

              elevation: 8,
              child: Container(
                width: double.infinity,
                height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?150:180:220,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [

                    Padding(padding: EdgeInsets.all(5),
                      child:
                      Center(child:Text("Your balance",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?16:19:25,color: Colors.black),)
                      ,
                      )
                    )



                  ,          Padding(padding: EdgeInsets.all(5),
       child:   Text(balance,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?16:19:25,color: Colors.black),)

                    ),


                    Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(20),


                    child:    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,

                      children: [

//                         Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(10, 8, 10, 10):EdgeInsets.fromLTRB(20, 20, 20, 20),
//                             child:  Container(
//
//                               width:ResponsiveInfo.isMobile(context)? 100:180,
//                               height:ResponsiveInfo.isMobile(context)? 40:80,
//                               decoration: BoxDecoration(
//                                   color: Color(0xfff55245),
//                                   border: Border.all(
//                                     color: Color(0xfff55245),
//                                   ),
//                                   borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))
//
//
//                               ),
//                               child: TextButton(onPressed: () {
//
//
//
// addMoneyToWallet();
//
//
//                               },
//                                 child: Text("Add money",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 13:17),),
//
//
//
//
//
//                               ),
//
//
//
//
//                             )),
                        Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(10, 8, 10, 10):EdgeInsets.fromLTRB(20, 20, 20, 20),
                            child:  Container(

                              width:ResponsiveInfo.isMobile(context)? 100:180,
                              height:ResponsiveInfo.isMobile(context)? 40:80,
                              decoration: BoxDecoration(
                                  color: Color(0xfff55245),
                                  border: Border.all(
                                    color: Color(0xfff55245),
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                              ),
                              child: TextButton(onPressed: () {


                                showWithDrawalForm(balanceamount);



                              },
                                child: Text("Withdraw",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 13:17),),





                              ),




                            )),
                      ],
                    )

                      ,)




                  ],


                ),


              ),



            ),

          ),

      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 170, 10, 10):EdgeInsets.fromLTRB(15, 210, 15, 20):EdgeInsets.fromLTRB(25, 260, 25, 30),

        child: GestureDetector(

          child: Card(
            elevation: 8,

            child: Container(

              width: double.infinity,
              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?40:60:80,

              child: Row(

                children: [

                  Expanded(child: Padding(padding: EdgeInsets.fromLTRB(5, 0, 0, 0),

                      child:




                      Center(child:  Text(selectedperiodData,maxLines: 1,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),))),flex: 2,),

                  Expanded(child: Container(width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:80:100,height:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?50:80:100,color: Color(0xfff55245),child: Align(
                    alignment: Alignment.center,


                    child:  Icon(Icons.date_range_sharp,size:ResponsiveInfo.isMobile(context)?28:40 ,color: Colors.white60,)


                  ) ,),flex: 1)

                    // Image.asset("images/calendar.png",width:ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?30:60:70,height: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)?30:60:70 ,),)),flex: 1,)


                ],


              ),


            ),


          ),

          onTap: (){


            showDialog(
                context: context,
                builder: (_) {
                  return MyDialog();
                }).then((value) => {


              selectedmonth=value["selecteddata"].toString().split(",")[0],
              selectedyear=value["selecteddata"].toString().split(",")[1],



getTransactions()








            });


          },
        )


        ,


      ),

      Padding(padding:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?EdgeInsets.fromLTRB(10, 240, 10, 10):EdgeInsets.fromLTRB(15, 290, 15, 20):EdgeInsets.fromLTRB(25, 360, 25, 30),

          child:ListView.builder(

              itemCount: transaction_data.length,
              itemBuilder: (BuildContext context, int index) {
                return Card(

                    elevation: 5,

                    shape: RoundedRectangleBorder(
                      side: BorderSide(color: Colors.white70, width: 0),
                      borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                    ),

                    child:    Container(
                      height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?60:90:110,
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [

                          Expanded(child:  Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [

                              Text(transaction_data[index].typeFrom,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:18,color: Colors.black),)

                             , Text(new DateFormat("yyyy-MM-dd hh:mm:ss").parse(transaction_data[index].date).day.toString()+"-"+new DateFormat("yyyy-MM-dd hh:mm:ss").parse(transaction_data[index].date).month.toString()+"-"+new DateFormat("yyyy-MM-dd hh:mm:ss").parse(transaction_data[index].date).year.toString(),style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?12:15:18,color: Colors.black),)

                            ],

                          ),
                            flex: 3,
                          )
                          ,

                          (double.parse(transaction_data[index].credit)>0)?
                  Expanded(child:Text("+ ₹ "+transaction_data[index].credit,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:17:20,color: Color(
                      0xff0a6f27)),)
                  ,flex: 1,):

                          Expanded(child:Text("- ₹"+transaction_data[index].debit,style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?14:17:20,color: Color(
                              0xffff0000)),)
                            ,flex: 1,)











                        ],


                      ),




                    )

                )




                ;



              }
          )




      )





          ]),
        ));
  }




  addMoneyToWallet()
  {


    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0):BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(15.0):EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: amountcontroller,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter the amount ',


                ),
              )),
            ),


            Padding(
              padding: ResponsiveInfo.isMobile(context)?const EdgeInsets.all(15):EdgeInsets.all(25.0),
              child:TextButton(
                onPressed: (){




                },
                child: Align(
                    alignment: Alignment.center,

                    child: Text('minimum amount is ₹ 100',style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 12,color: Colors.black54):TextStyle(fontSize: 15,color: Colors.black54),)
                ),
              ),),


            Padding(
              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(25.0),

              child: Container(

                width:ResponsiveInfo.isMobile(context)?120: 200,
                height: ResponsiveInfo.isMobile(context)?50:90,
                decoration: BoxDecoration(

                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      String amountdata=amountcontroller.text.toString();
                      double amount=double.parse(amountdata);


                      if(amountcontroller.text.toString().isNotEmpty) {

                        if(amount>=100)
                        {

                          submitPayment(amount);
                          Navigator.of(
                              context, rootNavigator: true)
                              .pop();

                          // otpcodecontroller.text="";
                          //
                          // Navigator.pop(context);
                          //
                          // Navigator.push(
                          //     context, MaterialPageRoute(builder: (_) => ForgotpasswordPage(title: "Product", mobile: mobilenumber,)));

                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Please enter the minimum amount"),
                          ));
                        }


                        //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter amount "),
                        ));
                      }


                    },

                    child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),










            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);




  }





  submitPayment(double amount) async
  {

    String trid=DateTime.now().millisecondsSinceEpoch.toString();

    final datastorage = await SharedPreferences.getInstance();
     ProgressDialog _progressDialog = ProgressDialog();
      _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.addmoneyToWallet),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'amount':amount.toString(),
          'transactionid':date,
          'description':"Amount added to wallet"

        }

    );



     _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    int status = jsondata['status'];

    String message=jsondata['message'];


    if(status==1)
      {

        getWalletBalance();
      }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));

    }




  }



  getTransactions() async
  {

    int a=int.parse(selectedmonth)-1;



    setState(() {

      selectedperiodData=months[a]+"/"+selectedyear;
    });




  final datastorage = await SharedPreferences.getInstance();
  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
  var date = new DateTime.now().toIso8601String();

  var dataasync = await http.post(
      Uri.parse(UrlData.baseurl+UrlData.walletTransactions),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

      },

      body: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',
        'month':selectedmonth,
        'year':selectedyear

      }

  );



  _progressDialog.dismissProgressDialog(context);

  String response = dataasync.body;

  dynamic jsondata = jsonDecode(response);
  int status = jsondata['status'];

  String message=jsondata['message'];


  print(response);

  if(status==1)
  {

    WalletTransfer walletTransactions=WalletTransfer.fromJson(jsondata);


    if(walletTransactions.data.length>0)
      {

        setState(() {

          transaction_data.clear();

          transaction_data.addAll(walletTransactions.data);


        });
      }
    else{

      setState(() {

        transaction_data.clear();

      });
    }





  }
  else{


    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //   content: Text('Failed'),
    // ));

  }



  }









  getWalletBalance() async
  {

     final datastorage = await SharedPreferences.getInstance();
    // ProgressDialog _progressDialog = ProgressDialog();
  //  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getWalletBalance),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }

    );



   // _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    int status = jsondata['status'];

    String message=jsondata['message'];

    String amount=jsondata['balance'].toString();

    if(status==1) {
      setState(() {
        balance = "₹ " + amount;

        balanceamount=balance;
      });
    }
    else{


      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text('Failed'),
      ));
    }

  }




  showWithDrawalForm(String balance)
  {
    showDialog(
        context: context,
        builder: (_) {
          return WithDrawalRequestDialog(balance);
        }).then((value) => {










    });


  }





}







class WithDrawalRequestDialog extends StatefulWidget {

  String balance;


  WithDrawalRequestDialog(this.balance);

  @override
  _WithDrawalRequestDialogState createState() => new _WithDrawalRequestDialogState(balance);
}

class _WithDrawalRequestDialogState extends State<WithDrawalRequestDialog> {

  String balance;


  _WithDrawalRequestDialogState(this.balance);

  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  TextEditingController amountcontroller=new TextEditingController();
  TextEditingController bankaccountcontroller=new TextEditingController();
  TextEditingController pancardnumbercontroller=new TextEditingController();
  TextEditingController ifscController=new TextEditingController();
  TextEditingController upiidcontroller=new TextEditingController();


  bool isIFscVisible=false;

  String branch="";

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();






    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Container(
        height: double.infinity,
        width: double.infinity,
        child:SingleChildScrollView(

          child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[

              Padding(
                padding: ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                child:TextButton(
                  onPressed: (){




                  },
                  child: Align(
                      alignment: Alignment.center,

                      child: Text('Withdrawal Request',style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 15,color: Colors.black):TextStyle(fontSize: 19,color: Colors.black),)
                  ),
                ),),





              Padding(
                padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                child: new Theme(data: new ThemeData(
                    hintColor: Colors.black38
                ), child: TextField(
                  controller: amountcontroller,
                  keyboardType: TextInputType.number,

                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    hintText: 'Enter the amount ',


                  ),
                )),
              ),

              Padding(
                padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                child: new Theme(data: new ThemeData(
                    hintColor: Colors.black38
                ), child: TextField(
                  controller: bankaccountcontroller,
                  keyboardType: TextInputType.number,

                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    hintText: 'Bank account number ',


                  ),
                )),
              ),


              Padding(
                padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                child: new Theme(data: new ThemeData(
                    hintColor: Colors.black38
                ), child: TextField(
                  controller: pancardnumbercontroller,
                  keyboardType: TextInputType.text,

                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    hintText: 'PAN card number',


                  ),
                )),
              ),

              Padding(
                padding: ResponsiveInfo.isMobile(context)? ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(10)    : EdgeInsets.all(15) :EdgeInsets.all(30) ,





                    child: Row(
                      textDirection: ui.TextDirection.rtl,
                      children: <Widget>[

                        Expanded(child: Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(10, 8, 10, 10):EdgeInsets.fromLTRB(20, 20, 20, 20),
                            child:  Container(

                              width:ResponsiveInfo.isMobile(context)? 100:180,
                              height:ResponsiveInfo.isMobile(context)? 40:80,
                              decoration: BoxDecoration(
                                  color: Color(0xfff55245),
                                  border: Border.all(
                                    color: Color(0xfff55245),
                                  ),
                                  borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                              ),
                              child: TextButton(onPressed: () {

                                if(ifscController.text.toString().isNotEmpty) {
                                  getBranchByIfsc(ifscController.text, 0);
                                }
                                else{

                                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                                    content: Text("Enter IFSC code"),
                                  ));
                                }



                              },
                                child: Text("Verify",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 13:17),),





                              ),




                            )),flex: 1,)
                        ,
          Expanded(child:  Padding(
                          padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                          child: new Theme(data: new ThemeData(
                              hintColor: Colors.black38
                          ), child: TextField(
                            controller: ifscController,

                            onChanged: (text) {



                            },

                            decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38, width: 0.5),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.black38, width: 0.5),
                              ),
                              hintText: 'IFSC code',



                            ),
                          )),
                        ),flex: 2),
                      ],
                    )



                //padding: EdgeInsets.symmetric(horizontal: 15),
                ,
              )

         ,



              (isIFscVisible)? Padding(
                padding: ResponsiveInfo.isMobile(context)?const EdgeInsets.all(5):EdgeInsets.all(10.0),
                child:TextButton(
                  onPressed: (){




                  },
                  child: Align(
                      alignment: Alignment.center,

                      child: Text(branch,style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 12,color: Color(
                          0xff083e0b)):TextStyle(fontSize: 18,color:  Color(
                          0xff083e0b)),)
                  ),
                ),):Container(),

              Padding(
                padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),
                child: new Theme(data: new ThemeData(
                    hintColor: Colors.black38
                ), child: TextField(
                  controller: upiidcontroller,



                  decoration: InputDecoration(
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(color: Colors.black38, width: 0.5),
                    ),
                    hintText: 'UPI ID (optional)',


                  ),
                )),
              ),





              Padding(
                padding:ResponsiveInfo.isMobile(context)?const EdgeInsets.all(10):EdgeInsets.all(20.0),

                child: Container(

                  width:ResponsiveInfo.isMobile(context)?120: 200,
                  height: ResponsiveInfo.isMobile(context)?50:90,
                  decoration: BoxDecoration(

                      color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                  child:Align(
                    alignment: Alignment.center,
                    child: TextButton(

                      onPressed:() {







                        if(amountcontroller.text.toString().isNotEmpty) {

                          String amountdata=amountcontroller.text.toString();

                          double d=double.parse(amountdata);

                           if(d>=100) {
                            if (bankaccountcontroller.text.isNotEmpty) {
                              if (pancardnumbercontroller.text.isNotEmpty) {
                                RegExp regExp = new RegExp(
                                  r"[A-Z]{5}[0-9]{4}[A-Z]{1}",
                                  caseSensitive: false,
                                  multiLine: false,
                                );

                                if (regExp.hasMatch(
                                    pancardnumbercontroller.text.toString())) {
                                  if (ifscController.text.isNotEmpty) {
                                    if (isIFscVisible) {
                                      // submitWithDrawalRequest();

                                      getBranchByIfsc(ifscController.text, 1);
                                    }
                                    else {
                                      ScaffoldMessenger.of(context)
                                          .showSnackBar(SnackBar(
                                        content: Text(
                                            "Verify your IFSC code"),
                                      ));
                                    }
                                  }
                                  else {
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(
                                        SnackBar(
                                          content: Text("Enter IFSC code"),
                                        ));
                                  }
                                }
                                else {
                                  ScaffoldMessenger.of(context).showSnackBar(
                                      SnackBar(
                                        content: Text(
                                            "Enter a valid PAN card number"),
                                      ));
                                }
                              }
                              else {
                                ScaffoldMessenger.of(context).showSnackBar(
                                    SnackBar(
                                      content: Text("Enter PAN card number"),
                                    ));
                              }
                            }
                            else {
                              ScaffoldMessenger.of(context).showSnackBar(
                                  SnackBar(
                                    content: Text("Enter bank account number"),
                                  ));
                            }
                          }
                          else{

                            ScaffoldMessenger.of(context).showSnackBar(
                                SnackBar(
                                  content: Text("Minimum withdrawal amount is 100"),
                                ));


                          }







                          //   checkMobileNumber(otpcodeController.text.toString());
                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Please enter amount "),
                          ));
                        }


                      },

                      child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                  ),



                  //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                ),


                // ,
              ),










              // Padding(padding: EdgeInsets.only(top: 50.0)),
              // TextButton(onPressed: () {
              //   Navigator.of(context).pop();
              // },
              //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
            ],
          ),


        )



       ,


      ),

    );
  }



  submitWithDrawalRequest() async
  {

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.sendWithDrawalRequest),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'reuestedamount':amountcontroller.text.toString(),
          'bankaccount':bankaccountcontroller.text.toString(),
          'ifsc':ifscController.text.toString(),
          'upi':upiidcontroller.text.toString(),
          'pancard':pancardnumbercontroller.text.toString()



        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    print(response);

    if(jsondata['status']==1) {

      // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      //   content: Text("Successfully submited request.You will get money in your bank account soon" ),
      // ));


      Widget okButton = TextButton(
        child: Text("OK"),
        onPressed: () {

          Navigator.pop(context);

        },
      );

      // set up the AlertDialog
      AlertDialog alert = AlertDialog(
        title: Text("Kolica"),
        content: Text("Successfully submited request.You will get money in your bank account soon.You can check the status in withdrawal request section"),
        actions: [
          okButton,
        ],
      );

      // show the dialog
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return alert;
        },
      );



    }
    else  if(jsondata['status']==2) {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Insufficient balance" ),
      ));

    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to submit request" ),
      ));

    }
  }







getBranchByIfsc(String ifsc,int code) async
{
  var dataasync = await http.get(
      Uri.parse(UrlData.ifscbaseurl+ifsc),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',


      },


  );





  String response = dataasync.body;
  print(response);
  dynamic jsondata = jsonDecode(response);

  try {

    if(response.compareTo("\"Not Found\"")!=0) {
      BranchIFSC branchIFSC = BranchIFSC.fromJson(jsondata);

      if (branchIFSC.branch.isNotEmpty) {
        if (code == 0) {
          setState(() {
            isIFscVisible = true;
            branch = branchIFSC.branch + "," + branchIFSC.centre;
          });
        }
        else {
          submitWithDrawalRequest();
        }
      }
      else {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text("Failed to verify ifsc"),
        ));
      }
    }
    else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to verify ifsc"),
      ));

    }



  }on Exception catch (_) {
    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Failed to verify ifsc"),
    ));
  }


}


}















class MyDialog extends StatefulWidget {



  @override
  _MyDialogState createState() => new _MyDialogState();
}

class _MyDialogState extends State<MyDialog> {
  Color _c = Colors.redAccent;

  List<String>months=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];

  String currentyear="";


  String currentmonth="";

  int currentmonthindex=0,currentyearnumber=0;

  String month="",year="";

  @override
  void initState() {
    // TODO: implement initState

    var now = DateTime.now();

    // date=now.day.toString()+"-"+now.month.toString()+"-"+now.year.toString();

    month=now.month.toString();
    year=now.year.toString();



    setState(() {
      int monthnumber=int.parse(month);
      currentmonthindex=monthnumber-1;

      currentmonth=months[monthnumber-1];
      currentyear=year;

      currentyearnumber=int.parse(currentyear);
    });
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      content: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Padding(
              padding: EdgeInsets.all(15.0),
              child: Container(
                  width: 150,

                  child:Row(

                    children: [

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {

                            if(currentmonthindex>0) {
                              currentmonthindex =
                                  currentmonthindex -1;
                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });
                            }



                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child: Text(currentmonth,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {

                            if(currentmonthindex<11) {
                              currentmonthindex =
                                  currentmonthindex + 1;

                              setState(() {
                                currentmonth =
                                months[currentmonthindex];
                              });

                            }



                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),

                      Expanded(child: Column(

                        children: [


                          TextButton(onPressed: () {


                            currentyearnumber =
                                currentyearnumber - 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });




                          },  child: Image.asset("images/up.png",width: 25,height: 25,)),

                          Padding(padding: EdgeInsets.all(5),child:  Text(currentyear,style: TextStyle(fontSize: 22,color: Colors.blue),),),

                          TextButton(onPressed: () {
                            currentyearnumber =
                                currentyearnumber + 1;

                            setState(() {
                              currentyear=currentyearnumber.toString();
                            });
                          },  child: Image.asset("images/down.png",width: 25,height: 25)),

                        ],




                      ),flex: 2,),


                    ],


                  )


              ),
            ),







            Padding(
              padding: EdgeInsets.all(15.0),

              child: Container(

                width: 150,
                height: 55,
                decoration: BoxDecoration(

                    color:  Colors.redAccent, borderRadius: BorderRadius.circular(10)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {

                      int ab=currentmonthindex+1;


                      String currentyearmonth=ab.toString()+","+currentyearnumber.toString();



                      //  Navigator.pop(context,'selecteddata':currentyearmonth);

                      Navigator.of(context).pop({'selecteddata':currentyearmonth});


                    },

                    child: Text('Submit', style: TextStyle(color: Colors.white) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),


            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
      // actions: <Widget>[
      //   FlatButton(
      //       child: Text('Switch'),
      //       onPressed: () => setState(() {
      //         _c == Colors.redAccent
      //             ? _c = Colors.blueAccent
      //             : _c = Colors.redAccent;
      //       }))
      // ],
    );
  }





}