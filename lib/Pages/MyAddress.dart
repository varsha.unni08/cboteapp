import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/AddAddress.dart';
import 'package:flutter_app_ecommerce/Pages/SingleOrderPage.dart';
import 'package:flutter_app_ecommerce/Pages/placeOrder.dart';
import 'package:flutter_app_ecommerce/constants/DataConstants.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/domain/address_data.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';

class MyAddressPage extends StatefulWidget {
  final String title;
  final String cartid;

  final String code;

  const MyAddressPage({Key? key, required this.title, required this.cartid, required this.code}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _MyAddressPage(cartid,code);
}

class _MyAddressPage extends State<MyAddressPage> {
  bool _switchValue = false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;

  bool _value = false;
  int val = 0;

  List<Data>addresslis=[];

  TextEditingController txtdeliveryoptionscontroller=new TextEditingController();

  String cartid="0";
  String code="0";


  _MyAddressPage(this.cartid,this.code);

  @override
  void initState() {
    // TODO: implement initState

    getAddressList();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
          backgroundColor: Color(0xfff55245),
          leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Colors.white),
            onPressed: () => Navigator.of(context).pop(),
          ),
          title: Text(
            "My Address",
            style: TextStyle(
                fontSize: ResponsiveInfo.isMobile(context)
                    ? ResponsiveInfo.isSmallMobile(context)
                    ? 12
                    : 16
                    : 20,
                color: Colors.white),
          ),
          centerTitle: false,
        ),
        body:Container(
    width: double.infinity,
    height: double.infinity,
    child:Column(

        children: [ 
          
          Flexible(child:  Stack(
              children: [


                Align(
                    alignment: Alignment.topLeft,
                    child: ListView.builder(

                        itemCount: addresslis.length,
                        itemBuilder: (BuildContext context, int index) {
                          return Padding(   padding: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? EdgeInsets.all(8):EdgeInsets.all(12):EdgeInsets.all(17),

                              child:

                              Container(
                                // color: Colors.amber[colorCodes[index]],
                                child: Center(
                                    child: Card(
                                      shape: RoundedRectangleBorder(
                                        side: BorderSide(color: Colors.white70, width: 0),
                                        borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(8):BorderRadius.circular(12):BorderRadius.circular(15),
                                      ),
                                      elevation: 5,
                                      child: Container(
                                        color: Colors.white,
                                        width: double.infinity,
                                        child:
                                        ListTile(
                                          title: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              mainAxisAlignment: MainAxisAlignment.start,
                                              children: [
                                                Text(addresslis[index].name,  style: TextStyle(
                                                  fontSize: ResponsiveInfo.isMobile(context)
                                                      ? ResponsiveInfo.isSmallMobile(context)
                                                      ? 15
                                                      : 18
                                                      : 23,
                                                  color: Colors.black)),
                                                Text(addresslis[index].name+","+addresslis[index].houseno+"\n"+addresslis[index].area+","+addresslis[index].landmark+"\n"+addresslis[index].town+","+addresslis[index].phone+"\n"+addresslis[index].pincode,  style: TextStyle(
                                                    fontSize: ResponsiveInfo.isMobile(context)
                                                        ? ResponsiveInfo.isSmallMobile(context)
                                                        ? 12
                                                        : 14
                                                        : 17,
                                                    color: Colors.black),maxLines: 5,)

                                              ,


                                                (addresslis[index].selected)? Padding(
                                                    padding: EdgeInsets.all(2),
                                                    child: Row(
                                                      children: [
                                                        Expanded(
                                                          child: TextButton(
                                                            onPressed: () async {

                                                              Map results = await Navigator.of(context)
                                                                  .push(new MaterialPageRoute<dynamic>(
                                                                builder: (BuildContext context) {
                                                                  return new MyNewAddressPage(
                                                                      title: "Address", id: addresslis[index].id,);
                                                                },
                                                              ));
                                                              if (results != null &&
                                                                  results.containsKey('added')) {
                                                                setState(() {
                                                                  var acc_selected =
                                                                  results['added'];

                                                                  int acs =
                                                                  acc_selected as int;

                                                                  if(acs!=0) {
                                                                    getAddressList();
                                                                  }




                                                                });
                                                              }








                                                            },
                                                            child: Text(
                                                              "Edit",
                                                              style: TextStyle(
                                                                  color: Colors.lightGreen),
                                                            ),
                                                          ),
                                                          flex: 2,
                                                        ),

                                                        Expanded(
                                                          child: TextButton(
                                                            onPressed: () {

                                                              Widget yesButton = TextButton(
                                                                  child: Text("Yes"),
                                                                  onPressed: () async {

                                                                    Navigator.pop(context);

                                                                    deleteAddress(addresslis[index].id,index);


                                                                  });



                                                              Widget noButton = TextButton(
                                                                child: Text("No"),
                                                                onPressed: () {
                                                                  Navigator.pop(context);
                                                                },
                                                              );

                                                              // set up the AlertDialog
                                                              AlertDialog alert = AlertDialog(
                                                                title: Text("Kolica"),
                                                                content: Text("Do you want to delete now ?"),
                                                                actions: [yesButton, noButton],
                                                              );

                                                              // show the dialog
                                                              showDialog(
                                                                context: context,
                                                                builder: (BuildContext context) {
                                                                  return alert;
                                                                },
                                                              );



                                                            },
                                                            child: Text(
                                                              "Delete",
                                                              style: TextStyle(
                                                                  color: Colors.redAccent),
                                                            ),
                                                          ),
                                                          flex: 2,
                                                        ),
                                                      ],
                                                    )):Container()




                                              ]),
                                          leading:  Checkbox(
                                            value: addresslis[index].selected,
                                            onChanged: (bool? value) {
                                              setState(() {

                                                for(int i=0;i<addresslis.length;i++)
                                                  {

                                                    addresslis[i].selected=false;
                                                  }



                                                addresslis[index].selected = value!;
                                              });
                                            },
                                          ),
                                        ),


                                      ),
                                    )),
                              ));
                        })),








                Align(
                    alignment: Alignment.bottomRight,
                    child: Padding(
                        padding: EdgeInsets.all(12),
                        child: FloatingActionButton(
                          onPressed: () async {



                            Map results = await Navigator.of(context)
                                .push(new MaterialPageRoute<dynamic>(
                              builder: (BuildContext context) {
                                return new MyNewAddressPage(
                                  title: "Address", id: '0',);
                              },
                            ));
                            if (results != null &&
                                results.containsKey('added')) {
                              setState(() {
                                var acc_selected =
                                results['added'];

                                int acs =
                                acc_selected as int;

                                if(acs!=0) {
                                  getAddressList();
                                }




                              });
                            }


                          },
                          child: Icon(
                            Icons.add,
                            color: Colors.white,
                            size: 29,
                          ),
                          backgroundColor:  Color(0xfff55245),
                          elevation: 5,
                          splashColor: Colors.grey,
                        ))),

              ]

          ),flex: 4,),
          
          (code.compareTo("1")!=0)?  Flexible(child: Container(

            width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)? 90.0:120:160,

            child:   Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                child:  Container(

                  width:ResponsiveInfo.isMobile(context)? 120:200,
                  height:ResponsiveInfo.isMobile(context)? 50:90,
                  decoration: BoxDecoration(
                      color: Color(0xfff55245),
                      border: Border.all(
                        color: Color(0xfff55245),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                  ),
                  child: TextButton(onPressed: () {

                    String a="0";

                    for(int i=0;i<addresslis.length;i++)
                      {
                        if(addresslis[i].selected)
                          {
                            a=addresslis[i].id;
                            break;
                          }

                      }

                    if(a.compareTo("0")!=0) {
                      Dialog forgotpasswordDialog = Dialog(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(12.0)),
                        //this right here
                        child: Container(
                          height: 300.0,
                          width: 500.0,

                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Padding(
                                padding: EdgeInsets.all(15.0),
                                child: new Theme(data: new ThemeData(
                                    hintColor: Colors.black38
                                ), child: TextField(

                                  keyboardType: TextInputType.text,
                                  controller: txtdeliveryoptionscontroller,

                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderSide: BorderSide(
                                          color: Colors.black38, width: 0.5),
                                    ),
                                    hintText: 'Delivery Instructions',


                                  ),
                                )),
                              ),
                              Padding(
                                padding: EdgeInsets.all(15.0),

                                child: Container(

                                  width: ResponsiveInfo.isMobile(context)?150:230,
                                  height: ResponsiveInfo.isMobile(context)?55:75,
                                  decoration: BoxDecoration(

                                      color: Color(0xfff55245),
                                      borderRadius: BorderRadius.circular(10)),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: TextButton(

                                      onPressed: () {


                                        if(cartid.compareTo("0")==0) {
                                          Navigator.push(
                                              context, MaterialPageRoute(
                                              builder: (_) =>
                                                  PlaceOrderPage(
                                                    title: "Order place",
                                                    addressid: a,
                                                    deliveryinstructions: txtdeliveryoptionscontroller
                                                        .text.toString(),)));
                                        }
                                        else{

                                          Navigator.push(
                                              context, MaterialPageRoute(
                                              builder: (_) =>
                                                  SingleOrderPage(
                                                    title: "Order place",
                                                    addressid: a,
                                                    deliveryinstructions: txtdeliveryoptionscontroller
                                                        .text.toString(), cartid: cartid,)));

                                        }

                                      },

                                      child: Text('Submit', style: TextStyle(
                                          color: Colors.white),),),
                                  ),


                                  //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                ),


                                // ,
                              ),
                              Padding(
                                padding: EdgeInsets.all(15.0),

                                child: Container(

                                  width: 150,
                                  height: 55,
                                  decoration: BoxDecoration(

                                      borderRadius: BorderRadius.circular(10)),
                                  child: Align(
                                    alignment: Alignment.center,
                                    child: TextButton(

                                      onPressed: () {

    if(cartid.compareTo("0")==0) {
      Navigator.push(
          context, MaterialPageRoute(
          builder: (_) =>
              PlaceOrderPage(
                title: "Placeorder", addressid: a, deliveryinstructions: '',)));
    }
    else{

      Navigator.push(
          context, MaterialPageRoute(
          builder: (_) =>
              SingleOrderPage(
                title: "Placeorder", addressid: a, deliveryinstructions: '', cartid: cartid,)));


    }

                                      },

                                      child: Text('Skip', style: TextStyle(
                                          color: Colors.black),),),
                                  ),


                                  //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                ),


                                // ,
                              ),
                              // Padding(padding: EdgeInsets.only(top: 50.0)),
                              // TextButton(onPressed: () {
                              //   Navigator.of(context).pop();
                              // },
                              //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                            ],
                          ),
                        ),
                      );


                      showDialog(context: context,
                          builder: (
                              BuildContext context) => forgotpasswordDialog);
                    }
                    else{


                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Please select your address"),
                      ));

                    }





                  },
                    child: Text("Submit",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                  ),




                )) ,





          ),flex: 1,):Container()
          
          
          
          
         
        ])

        )





    );

  }

  getAddressList() async
  {


    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.getAddress),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',



        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;


    print(response);
    dynamic jsondata = jsonDecode(response);

    if(jsondata['status']==1) {
      AddressData addressData = AddressData.fromJson(jsondata);

      print(addressData.data.length);

      setState(() {
        addresslis.clear();
        addresslis.addAll(addressData.data);
      });
    }
    else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("No address found"),
      ));


    }


  }


  deleteAddress(String id,int index) async
  {

    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.deleteAddress),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          UrlData.header_token : datastorage.getString(DataConstants.tokenkey)!

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',
          'addressid':id

        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    dynamic jsondata = jsonDecode(response);
    int status = jsondata['status'];

    String message=jsondata['message'];

    if(status==1)
    {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Address deleted successfully"),
      ));



      setState(() {
        addresslis.removeAt(index);


      });




    }
    else{
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed"),
      ));


    }


  }



}