import 'dart:collection';
import 'dart:io';
import 'dart:math';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/homepage.dart';
import 'package:flutter_xlider/flutter_xlider.dart';

import 'package:shared_preferences/shared_preferences.dart';

import 'package:url_launcher/url_launcher.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:open_file/open_file.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:video_player/video_player.dart';
import 'package:flutter/material.dart';

class FinalPage extends StatefulWidget {
  final String title;

  const FinalPage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _FinalPage();
}

class _FinalPage extends State<FinalPage> {
  bool _switchValue = false;

  final List<String> entries = <String>['A', 'B', 'C'];
  final List<int> colorCodes = <int>[600, 500, 100];
  double _lowerValue = 50;
  double _upperValue = 180;

  bool _value = false;
  int val = -1;
  bool isChecked=false;
  late VideoPlayerController _controller;
  @override
  void initState() {
    // TODO: implement initState

    _controller = VideoPlayerController.asset(
        'images/s.mp4')
      ..initialize().then((_) {


        // Ensure the first frame is shown after the video is initialized, even before the play button has been pressed.
        setState(() {});
      });

    startTime();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {


    _controller.play();

    return WillPopScope(
        onWillPop: _onBackPressed,
        child:    Scaffold(
      resizeToAvoidBottomInset : false,

      body: Container(


        width: double.infinity,
        height: double.infinity,
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(


          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,


          children: [

            Container(

              width: double.infinity,
              height: ResponsiveInfo.isMobile(context)?280:360,



              child: Stack(



                children: [




                  Center(
                    child: _controller.value.isInitialized
                        ? Container(
                        width: double.infinity,
                      height:double.infinity,
                      child: VideoPlayer(_controller),
                    )
                        : Container(),
                  ),   Align(

                    alignment: Alignment.center,
                    child:  ResponsiveInfo.isMobile(context)?Image.asset('images/recipt.png',width: 130,height: 130,):Image.asset('images/cboteicon.png',width: 260,height: 260,),

                  ),



                ],


              ),



            ),




            ResponsiveInfo.isMobile(context)?  Text("Thank you for purchase.....",style: TextStyle(fontSize: 20,color: Colors.black),):
            Text("Thank you for purchase.....",style: TextStyle(fontSize: 30,color: Colors.black),),



















          ],





        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    ));
  }

  Future<bool> _onBackPressed() async {



    // Navigator.of(context)
    //     .pushNamedAndRemoveUntil('/login', (Route<dynamic> route) => false);

    // Navigator.pushReplacement(context, MaterialPageRoute(
    //     builder: (context) => MyHomePage(title: "Home page",)
    // )
    // );

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                MyHomePage(title: "Home page",)),
            (Route<dynamic> route) => false);

    return true ;
  }


  startTime() async {

    await Future.delayed(Duration(seconds: 3));

    Navigator.pushAndRemoveUntil(
        context,
        MaterialPageRoute(
            builder: (BuildContext context) =>
                MyHomePage(title: "Home page",)),
            (Route<dynamic> route) => false);




  }

}