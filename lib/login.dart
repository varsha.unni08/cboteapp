import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Pages/forgotpassword.dart';
import 'package:flutter_app_ecommerce/Signup.dart';
import 'package:flutter_app_ecommerce/domain/ProfileStatus.dart';


import 'constants/DataConstants.dart';
import 'constants/UrlConstants.dart';
import 'design/ResponsiveInfo.dart';
import 'homepage.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';




class MyLoginPage extends StatefulWidget {
  MyLoginPage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyLoginPageState createState() => _MyLoginPageState();
}

class _MyLoginPageState extends State<MyLoginPage> {
  TextEditingController mobilenumbercontroller = new TextEditingController();
  TextEditingController passwordcontroller = new TextEditingController();
  TextEditingController otpcodecontroller = new TextEditingController();
  TextEditingController forgotpasswordmobilecontroller = new TextEditingController();

  String otpcode="";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }


  @override
  Widget build(BuildContext context) {



    Size screenSize = MediaQuery.of(context).size;
    double width=screenSize.width;
    double height=screenSize.height;


    return Scaffold(
      resizeToAvoidBottomInset : false,
        body: Stack(

              children: <Widget>[

                Container(
                  width: width,
                  height: height/1.9,

                  child: Image.asset("images/loginbg.png",width: width,height: height/1.9,fit: BoxFit.fill,),

                ),



      Align(
        alignment: Alignment.center,

    child:  Container(
          width: width,
          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?height/1.2:height/1.4:height/1.3,
               child:

                   Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(15, 0, 15, 0):EdgeInsets.fromLTRB(30, 0, 30, 0),

                     child:Card(

                       shape: RoundedRectangleBorder(
                         borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(15):BorderRadius.circular(30),
                       ),

                       elevation: 10,
                       
                       child: Column(

                         
                         children: [
                           
                           Image.asset("images/kolica.png",width: ResponsiveInfo.isMobile(context)?130:260,height:ResponsiveInfo.isMobile(context)?130: 260,fit: BoxFit.fill,),

                           Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 0)   :EdgeInsets.fromLTRB(30, 20, 30, 0),

                             child:Container(


                               child: Padding(
                                 padding: ResponsiveInfo.isMobile(context)?  EdgeInsets.all(12) :EdgeInsets.all(30) ,

                                 child:Container(

                                     decoration: BoxDecoration(
                                         border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                                     ),

                                     child: Row(
                                       textDirection: TextDirection.ltr,
                                       children: <Widget>[

                                         Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                                         child: ResponsiveInfo.isMobile(context)? Image.asset("images/pc.png",width: 25,height: 25,

                                         ):Image.asset("images/pc.png",width: 45,height: 45,

                                         )
                                         )
                                       ,
                                         Expanded(child: new Theme(data: new ThemeData(
                                             hintColor: Colors.black45
                                         ), child: TextField(
                                           controller: mobilenumbercontroller,

                                           keyboardType: TextInputType.number,
                                           style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                                           decoration: InputDecoration(
                                             border: InputBorder.none,

                                             hintText: 'Phone number',
                                           ),


                                         )))
                                       ],
                                     )

                                 )

                                 //padding: EdgeInsets.symmetric(horizontal: 15),
                                 ,
                               ),







                             )



                             ,),






                           Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 0)    :EdgeInsets.fromLTRB(30, 25, 30, 0),

                             child: Container(

                               child: Padding(
                             padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(10):EdgeInsets.all(30),

                             child:Container(

                                 decoration: BoxDecoration(
                                     border: Border.all(color: Colors.black45,width: ResponsiveInfo.isMobile(context)? 0.5:1.0)
                                 ),

                                 child: Row(
                                   textDirection: TextDirection.ltr,
                                   children: <Widget>[

                                     Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.all(8):EdgeInsets.all(18),

                                         child:

                                         ResponsiveInfo.isMobile(context)? Image.asset("images/padlock.png",width: 25,height: 25,

                                         ):Image.asset("images/padlock.png",width: 45,height: 45,

                                         )
                                     )
                                     ,
                                     Expanded(child: new Theme(data: new ThemeData(
                                         hintColor: Colors.black45
                                     ), child: TextField(
                                       obscureText: true,
                                       controller: passwordcontroller,
                                       style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),

                                       decoration: InputDecoration(
                                         border: InputBorder.none,

                                         hintText: 'Password',
                                       ),


                                     )))
                                   ],
                                 )

                             )

                             //padding: EdgeInsets.symmetric(horizontal: 15),
                             ,
                           ),



                             )



                             ,),


                           Container(



                               child:  Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.only(top: 8.0,left:0.0,right: 20.0,bottom: 0)      : EdgeInsets.only(top: 20.0,left:0.0,right: 40.0,bottom: 0),

                                 child: Align(
                                   alignment: Alignment.centerRight,

                                   child: TextButton(
                                     onPressed:() {

                                       Dialog forgotpasswordDialog = Dialog(
                                         shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0): BorderRadius.circular(24.0)), //this right here
                                         child: Container(
                                           height: height/1.4,
                                           width: width/1.5,

                                           child: Column(
                                             mainAxisAlignment: MainAxisAlignment.center,
                                             children: <Widget>[



                                               Padding(
                                                 padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0)  : EdgeInsets.all(30.0),
                                                 child: new Theme(data: new ThemeData(
                                                     hintColor: Colors.black38
                                                 ), child:



                                                 TextField(
                                                   controller: forgotpasswordmobilecontroller,
                                                   keyboardType: TextInputType.number,
                                                   style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 13, height: 1.3):TextStyle(fontSize: 20, height: 1.5),

                                                   decoration: InputDecoration(
                                                     focusedBorder: OutlineInputBorder(
                                                       borderSide: BorderSide(color: Colors.black38, width: 1),
                                                     ),
                                                     enabledBorder: OutlineInputBorder(
                                                       borderSide: BorderSide(color: Colors.black38, width:1),
                                                     ),
                                                     hintText: 'Enter the mobile number',


                                                   ),
                                                 )),
                                               ),
                                               Padding(
                                                 padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(30.0),

                                                 child: Container(

                                                   width:ResponsiveInfo.isMobile(context)?120: 200,
                                                   height: ResponsiveInfo.isMobile(context)?50:90,
                                                   decoration: BoxDecoration(

                                                       color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(10):BorderRadius.circular(20)),
                                                   child:Align(
                                                     alignment: Alignment.center,
                                                     child: TextButton(

                                                       onPressed:() {

                                                         if(forgotpasswordmobilecontroller.text.isNotEmpty)
                                                           {



                                                             checkMobileNumber(forgotpasswordmobilecontroller.text);

                                                             forgotpasswordmobilecontroller.text="";
                                                             Navigator.pop(context);


                                                           }





                                                       },

                                                       child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13)   : TextStyle(color: Colors.white,fontSize: 23) ,) ,),
                                                   ),



                                                   //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
                                                 ),


                                                 // ,
                                               ),
                                               // Padding(padding: EdgeInsets.only(top: 50.0)),
                                               // TextButton(onPressed: () {
                                               //   Navigator.of(context).pop();
                                               // },
                                               //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
                                             ],
                                           ),
                                         ),
                                       );



                                       showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);






                                     },

                                     child:ResponsiveInfo.isMobile(context)? Text('Forgot password ?',style: TextStyle(color: Color(0xfff55245), fontSize: 14)

                                     )    :Text('Forgot password ?',style: TextStyle(color: Color(0xfff55245), fontSize: 20)



                                     ),
                                   ),
                                 ),)),



                           Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                               child:  Container(

                                 width:ResponsiveInfo.isMobile(context)? 120:200,
                                 height:ResponsiveInfo.isMobile(context)? 50:90,
                                 decoration: BoxDecoration(
                                     color: Color(0xfff55245),
                                     border: Border.all(
                                       color: Color(0xfff55245),
                                     ),
                                     borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                                 ),
                                 child: TextButton(onPressed: () {

                                   login(mobilenumbercontroller.text,passwordcontroller.text);



                                   // Navigator.pushReplacement(context, MaterialPageRoute(
                                   //     builder: (context) => MyHomePage(title: "Signup",)
                                   // )
                                   // );


                                 },
                                   child: Text("Sign in",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                                 ),




                               )),


                           Row(

                             mainAxisAlignment: MainAxisAlignment.center ,
                             crossAxisAlignment: CrossAxisAlignment.center ,

                             children: [

                               Text("New to cbote ? ",style: ResponsiveInfo.isMobile(context)?TextStyle(fontSize: 14,color: Colors.black)     :TextStyle(fontSize: 22,color: Colors.black),),

                               TextButton( onPressed: () {

                                 Navigator.pushReplacement(context, MaterialPageRoute(
                                     builder: (context) => MySignupPage(title: "Signup",)
                                 )
                                 );


                               },
                               child: Text("Sign up",style:ResponsiveInfo.isMobile(context)? TextStyle(fontSize: 14,color: Colors.blueAccent):TextStyle(fontSize: 22,color: Colors.blueAccent)


                               ),)


                             ],



                           )





                           
                           
                         ],
                         
                         
                         
                       ),





                     )


                   )



               ))











                ,
              ] ),







         );



}



void login(String username,String password) async
{

  final datastorage = await SharedPreferences.getInstance();

  if(username.isNotEmpty&&password.isNotEmpty) {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.get(
        Uri.parse(
            UrlData.baseurl + UrlData.login + "/" + username + "/" + password),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        }
    );

    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;

    print(response);

    dynamic jsondata = jsonDecode(response);
    String token = jsondata['token'];
    int status = jsondata['status'];

    String message = jsondata['message'];

    if(status==1)
      {



        datastorage.setString(DataConstants.tokenkey, token);

        Navigator.pushReplacement(context, MaterialPageRoute(
            builder: (context) => MyHomePage(title: "Signup",)
        )
        );

      }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("login failed"),
      ));

    }




  }
  else{

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Please enter full data"),
    ));

  }
}



void checkMobileNumber(String mobile) async
{
  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(
      context, textToBeDisplayed: "Please wait for a moment......");


  var dataasync = await http.get(
      Uri.parse(
          UrlData.baseurl + UrlData.mobilenumberexists + "/" + mobile),

      headers: <String, String>{
        'Content-Type': 'application/x-www-form-urlencoded',

      }
  );

  _progressDialog.dismissProgressDialog(context);

  String response = dataasync.body;

  print(response);

  dynamic jsondata = jsonDecode(response);

  int status = jsondata['status'];

  String message = jsondata['message'];

  if(status==1)
    {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text(message),
      ));

      var rng = new Random();
      var code = rng.nextInt(9000) + 1000;



      sendOtpcode(mobile, code.toString());


      showOtpCodeDialog( mobile);
    }
  else{

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text(message),
    ));
  }




}

sendOtpcode(String mobile,String code) async
{
  var rng = new Random();
  var code = rng.nextInt(9000) + 1000;

  otpcode=code.toString();

  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");


  var dataasync = await http.get(
    Uri.parse(UrlData.smsbaseurl+"/"+UrlData.smsapikey+"/SMS/"+mobile+"/"+code.toString()+"/"+UrlData.smstemplate),

    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',

    },



  );



  _progressDialog.dismissProgressDialog(context);

  String response = dataasync.body;


  // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
  //   content: Text('otp code is : '+code.toString()),
  // ));
}



  showOtpCodeDialog(String mobilenumber)
  {

    Dialog forgotpasswordDialog = Dialog(
      shape: RoundedRectangleBorder(borderRadius:ResponsiveInfo.isMobile(context)? BorderRadius.circular(12.0):BorderRadius.circular(20.0)), //this right here
      child: Container(
        height: ResponsiveInfo.isMobile(context)? 300.0:450,
        width:ResponsiveInfo.isMobile(context)? 600.0:750,

        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding:ResponsiveInfo.isMobile(context)?  EdgeInsets.all(15.0):EdgeInsets.all(25.0),
              child: new Theme(data: new ThemeData(
                  hintColor: Colors.black38
              ), child: TextField(
                controller: otpcodecontroller,
                keyboardType: TextInputType.number,

                decoration: InputDecoration(
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.black38, width: 0.5),
                  ),
                  hintText: 'Enter OTP code',


                ),
              )),
            ),
            Padding(
              padding:ResponsiveInfo.isMobile(context)? EdgeInsets.all(15.0):EdgeInsets.all(25.0),

              child: Container(

                width:ResponsiveInfo.isMobile(context)?120: 200,
                height: ResponsiveInfo.isMobile(context)?50:90,
                decoration: BoxDecoration(

                    color: Color(0xfff55245), borderRadius:ResponsiveInfo.isMobile(context)?  BorderRadius.circular(10):BorderRadius.circular(18)),
                child:Align(
                  alignment: Alignment.center,
                  child: TextButton(

                    onPressed:() {


                      if(otpcodecontroller.text.toString().isNotEmpty) {

                        if(otpcodecontroller.text.toString().compareTo(otpcode.toString())==0)
                        {
                          // Navigator.of(
                          //     context, rootNavigator: true)
                          //     .pop();

                          otpcodecontroller.text="";

                          Navigator.pop(context);

                          Navigator.push(
                              context, MaterialPageRoute(builder: (_) => ForgotpasswordPage(title: "Product", mobile: mobilenumber,)));

                        }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("incorrect otp code"),
                          ));
                        }


                        //   checkMobileNumber(otpcodeController.text.toString());
                      }
                      else{

                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text("Please enter otp code"),
                        ));
                      }


                    },

                    child: Text('Submit', style:ResponsiveInfo.isMobile(context)? TextStyle(color: Colors.white,fontSize: 13):TextStyle(color: Colors.white,fontSize: 18) ,) ,),
                ),



                //  child:Text('Submit', style: TextStyle(color: Colors.white) ,) ,)
              ),


              // ,
            ),

            Padding(
              padding: const EdgeInsets.all(15),
              child:TextButton(
                onPressed: (){
                  //TODO FORGOT PASSWORD SCREEN GOES HERE
                  var rng = new Random();
                  var c = rng.nextInt(9000) + 1000;



                  sendOtpcode(mobilenumber,c.toString());
                },
                child: Align(
                    alignment: Alignment.center,

                    child: Text('Did not get OTP code ?  Resend')
                ),
              ),),








            // Padding(padding: EdgeInsets.only(top: 50.0)),
            // TextButton(onPressed: () {
            //   Navigator.of(context).pop();
            // },
            //     child: Text('Got It!', style: TextStyle(color: Colors.purple, fontSize: 18.0),))
          ],
        ),
      ),
    );



    showDialog(context: context, builder: (BuildContext context) => forgotpasswordDialog);


  }







  }