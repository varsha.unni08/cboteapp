import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/Signup.dart';
import 'package:flutter_app_ecommerce/design/ResponsiveInfo.dart';
import 'package:flutter_app_ecommerce/login.dart';


import 'homepage.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Ecommerce',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: WelcomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class WelcomePage extends StatefulWidget {
  WelcomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _WelcomePageState createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {





  @override
  void initState() {
    // TODO: implement initState

    startTime();
    super.initState();
  }




  @override
  Widget build(BuildContext context) {

    double width = MediaQuery.of(context).size.width;

    double dw=width/1.4;
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      resizeToAvoidBottomInset : false,

      body: Container(


        width: double.infinity,
        height: double.infinity,
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(




          children: [

            Container(

              width: double.infinity,
              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?250:350:500,

              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/bg.png"),
                  fit: BoxFit.fill,
                ),
              ),

              child: Stack(

                children: [


                  Align(

                    alignment: Alignment.center,
                    child:  ResponsiveInfo.isMobile(context)?Image.asset('images/kolicaicon.png',width: 180,height: 180,):Image.asset('images/kolicaicon.png',width: 320,height: 320,),

                  )



                ],


              ),



            ),

            Container(



              child:

              Padding(padding: EdgeInsets.all(10),
                child: Image.asset("images/kolicaletter.png",width: dw,height: dw/5.08,),


              )


            ),
            
            
          ResponsiveInfo.isMobile(context)?  Text("Your nearest online shop",style: TextStyle(fontSize: 20,color: Colors.black),):
          Text("Your nearest online shop",style: TextStyle(fontSize: 30,color: Colors.black),),




        Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 29, 0, 10):EdgeInsets.fromLTRB(0, 50, 0, 20),
      child:  Container(

          width:ResponsiveInfo.isMobile(context)? 120:200,
          height:ResponsiveInfo.isMobile(context)? 50:90,
          decoration: BoxDecoration(
            color: Color(0xfff55245),
              border: Border.all(
                color: Color(0xfff55245),
              ),
              borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


          ),
          child: TextButton(onPressed: () {

            Navigator.pushReplacement(context, MaterialPageRoute(
                builder: (context) => MyLoginPage(title: "Login",)
            )
            );


          },
          child: Text("Sign in",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





          ),




        )),

            Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 10, 0, 10):EdgeInsets.fromLTRB(0, 20, 0, 20),
                child:  Container(

                  width:ResponsiveInfo.isMobile(context)? 120:200,
                  height:ResponsiveInfo.isMobile(context)? 50:90,

                  child: TextButton(onPressed: () {

                    Navigator.pushReplacement(context, MaterialPageRoute(
                        builder: (context) => MySignupPage(title: "Signup",)
                    )
                    );

                  },
                    child: Text("Sign up",style: TextStyle(color: Color(0xfff55245),fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                  ),




                ))












          ],





        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );
  }

  startTime() async {




    await Future.delayed(Duration(seconds: 5));






  }
}
