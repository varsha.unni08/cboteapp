import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/interfaces/WidgetEventListener.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../login.dart';

class LoginWarningWidget extends StatefulWidget{
  LoginWarningWidget({Key? key, required this.title, required this.widgetEventListener}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;
  final WidgetEventListener widgetEventListener;

  @override
  _LoginWarningWidget createState() => _LoginWarningWidget(widgetEventListener);


}

class _LoginWarningWidget extends State<LoginWarningWidget> {

  WidgetEventListener widgetEventListener;


  _LoginWarningWidget(this.widgetEventListener);

  @override
  void initState() {
    // TODO: implement initState


    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    Size screenSize = MediaQuery.of(context).size;

    return Scaffold(

      body: Container(

        child: Align(

          alignment: Alignment.center,

          child: Column(

            children: [
              
              Image.asset('images/alarm.png',width: 100,height: 100,fit: BoxFit.fill,),



              Padding(padding: EdgeInsets.all(8),

              child: Text("You did not sign in to this app .please sign in ",style: TextStyle(fontSize: 12,color: Colors.black),) ,)

             ,

              Padding(padding: EdgeInsets.all(8),

                  child:  TextButton(onPressed:()async{

                    Map results = await Navigator.of(context)
                        .push(new MaterialPageRoute<dynamic>(
                      builder: (BuildContext context) {
                        return new MyLoginPage(title: "account setup",);
                      },
                    ));

                    if (results != null &&
                        results.containsKey('completionstatus')) {
                      setState(() {
                        var accountsetupdata =
                        results['completionstatus'];

                        int acs =
                        accountsetupdata as int;

                        if(acs>0)
                          {

                            if(widgetEventListener!=null)
                              {

                                widgetEventListener.closeWidget();
                              }

                         }


                      });
                    }


              } , child: Text("Sign in",style: TextStyle(fontSize: 12))))

            ],



          ),



        ),



      ),



    );


  }

}