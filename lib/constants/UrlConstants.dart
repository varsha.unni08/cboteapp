class UrlData{

   static String header_token="token";

   static String baseurl="https://centroidsolutions.in/Kolica/KolicaApi/";

   static String imageurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/";

   static String profileimageurl="https://centroidsolutions.in/Kolica/KolicaApi/images/";

   static String ifscbaseurl="https://ifsc.razorpay.com/";

   static String referal="ReferalApi/getReferal/";

   static String categorybaseurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/categoryimage/";

   static String sliderbaseurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/sliderimages/";

   static String advertiseurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/advertisement/";

   static String productimageurl="https://centroidsolutions.in/Kolica/KolicaAdmin/images/productimages/products/";

   static String termsurl="https://www.centroidsolutions.in/Kolica/KolicaApi/termsandConditions.html";

   static String privacyurl="https://www.centroidsolutions.in/Kolica/KolicaApi/privacypolicy.html";

   static String playstoreurl="https://play.google.com/store/apps/details?id=com.centroid.flutter_app_ecommerce";

   static String smsapikey="3a47962e-9eb0-11ec-a4c2-0200cd936042";
   static String smsbaseurl="http://2factor.in/API/V1/";
   static String smstemplate="cbote";

   static String login="UserApi/getUserDetails";
   static String mobilenumberexists="UserApi/checkUserMobile";
   static String updatePassword="UserApi/updatePassword";
   static String registration="UserApi/registration";
   static String getProfile="UserApi/getUserProfile";
   static String uploadUserprofile="UserApi/uploadProfileImage";
   static String addAddress="UserAddressApi/addAddress";
   static String getAddress="UserAddressApi/getAddress";
   static String deleteAddress="UserAddressApi/deleteAddress";
   static String getAddressById="UserAddressApi/getAddressById";
   static String updateAddress="UserAddressApi/updateAddress";
   static String getWalletBalance="WalletApi/getWalletBalance";
   static String addmoneyToWallet="WalletApi/addmoneyToWallet";
   static String walletTransactions="WalletApi/getWalletTransactions";
   static String sendWithDrawalRequest="WalletApi/sendWithDrawalRequest";
   static String getEncryptedUserData="UserApi/getEncryptedUserData";
   static String getCategories="CategoryApi/getCategories";
   static String getSlider="SliderApi/getSlider";
   static String getOfferProducts="ProductApi/getOfferProducts";
   static String getAdvertisements  ="AdvertiseApi/getAdvertisements";
   static String getNotifications="NotificationApi/getNotifications";
   static String updateReadStatus="NotificationApi/updateReadStatus";
   static String searchProducts="ProductApi/searchProducts";
   static String productbyCategory="ProductApi/getProductsByCategory";
   static String getProductAccordingToLocation="ProductApi/getProductAccordingToLocation";
   static String getSubcategories="CategoryApi/getSubCategories";
   static String getFilteredProducts="ProductApi/getFilteredProducts";
   static String getProductDetails="ProductApi/getProductDetails";
   static String getProductSpec="ProductApi/getProductSpec";
   static String getRating="ProductApi/getRating";
   static String getRelatedProduct="ProductApi/getRelatedProducts";
   static String postRating="ProductApi/postRating";
   static String getAverageRating="ProductApi/getAverageRating";
   static String checkStockAvailability="ProductApi/checkStockAvailability";
   static String addtoCart="CartApi/addToCart";
   static String getCart="CartApi/getCart";
   static String calculateAmount="CartApi/calculateTotalAmount";
   static String updateCart="CartApi/updateCart";
   static String deleteFromCart="CartApi/deleteFromCart";
   static String calculateTotalCashbackAmount="CartApi/calculateTotalCashbackAmount";
   static String placeOrder="OrderApi/placeOrder";
   static String addToWishList="WishlistApi/addProductToWishlist";
   static String checkProductWishlist="WishlistApi/checkProductWishlist";
   static String deleteFromWishlist="WishlistApi/deleteFromWishlist";
   static String getWishlist="WishlistApi/getWishlist";
   static String getCartCount="CartApi/getCartCount";
   static String getSalesOrder="OrderApi/getSalesOrder";
   static String subtractBalance="WalletApi/subtractBalance";
   static String cancelMyOrder="OrderApi/cancelMyOrder";
   static String getSalesOrderItems="OrderApi/getSalesOrderItems";
   static String calculateSingleItemTotalAmount="CartApi/calculateSingleItemTotalAmount";
   static String calculateSingleCashbackAmount="CartApi/calculateSingleCashbackAmount";
   static String placeSingleOrder="OrderApi/placeSingleOrder";
   static String cancelOrderDetails="OrderApi/cancelOrderDetails";
   static String updateReturnOrderStatus="OrderApi/updateReturnOrderStatus";
   static String getWalletRquests="WalletApi/getWalletRquests";
   static String checkPincode="UserAddressApi/checkPincode";
   static String getCustomerCareNumber="SettingsApi/getCustomerCareNumber";
   static String getCategoryDetails="CategoryApi/getCategoryDetails";
   static String getVerion="VersionApi/getVersion";
   static String getFilterProducts="ProductApi/getFilterBySubCategory";
   static String cancelOrderQty="OrderApi/cancelOrderQty";
   static String getCancelledSalesOrderItems="OrderApi/getCancelledSalesOrderItems";
   static String cancelFullOrder="OrderApi/cancelFullOrder";
   static String getOrderInfo="OrderApi/getOrderInfo";
   static String checkUserPincode="UserApi/checkPincode";










   static String filterparameters="";


   static String producttype="2";
   static String categorytype="1";
   static String pricewise="3";
   static String cashback="4";





   
}