import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';
import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter_app_ecommerce/domain/Profile.dart';



class DataConstants{

   static final String sender="CGSAVE";
   static final String forgotpasstemplateid="1007856104698741987";
   static final String registrationtemplateid="1007625690429475781";
   static final String registration_Confirm_templateid="1007134283594642980";

   static final String route="2";

   static final String type="1";

   static final String apikey="bf25917c3254cfe9f50694f24884f23a";


  static final String tokenkey="tokenkey";



  // uploadBackupFile.php check this also





   static final String backuptoken="BACKUPTOKEN";

   static final String userkey="userkey";

   static final String firstuser="FIRSTUSER";

   static final String firstpassword="FIRSTPassword";
    static final  String AccSettingsFirst="AccSettingsFirst";




















  //  static String buildServerMessage(int Type, String otp,String username,String password)
  // {
  //   String message="";
  //
  //   if(Type==ServerMessageType.forgot_password)
  //   {
  //
  //     message="Your OTP for forgot Password is "+otp+" .CGSAVE";
  //   }
  //
  //   if(Type==ServerMessageType.registration_Confirm_password)
  //   {
  //
  //     message="Your registration is successful. Your Registration ID "+username+" Passowrd "+password+" .CGSAVE";
  //   }
  //
  //   if(Type==ServerMessageType.registration)
  //   {
  //
  //     message="Dear "+username+" Welcome to SAVE App - My Personal App. Your OTP is "+otp+" CGSAVE";
  //   }
  //
  //
  //   return message;
  // }


  static final List<String> arrofLanguages = ['Select your language','हिंदी', 'English','മലയാളം','தமிழ்','ಕನ್ನಡ','తెలుగు','मराठी'];

  //  static class ServerMessage{
  //
  // public static String sender="CGSAVE";
  // public static String forgotpasstemplateid="1007856104698741987";
  // public static String registrationtemplateid="1007625690429475781";
  // public static String registration_Confirm_templateid="1007134283594642980";
  //
  // public static String route="2";
  //
  // public static String type="1";
  //
  // public static String apikey="bf25917c3254cfe9f50694f24884f23a";
  //
  // }


static  bool checkEmailPattern(var email)
{

  bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email);

  return emailValid;
}


static  String generateToken(ProfileData prfd)
{

var rng = new Random();
int a= rng.nextInt(1000)+10;
var ll=new DateTime.now().millisecond;


String userid=a.toString()+"%"+prfd.id.toString()+"&"+ll.toString()+prfd.password;


  return userid;
}





static String bankAccountType="Bank";
 static String cashAccountType="Cash";
 static String allAccountType="";
 static String customerAccountType="Customers";
 static String incomeAccountType="Income account";
 static String billnumber="Billnumber";
 static String billvoucherNumber="Save_Bill_000";


  static int credit=1;
  static int debit=0;






  static int paymentvoucher=1;
  static int receiptvoucher=2;

  static int billvoucher=3;

  static int bankvoucher=5;

  static int journalvoucher=4;

  static int wallet=6;


 static Image imageFromBase64String(String base64String) {
   return Image.memory(
     base64Decode(base64String),
     fit: BoxFit.fill,
   );
 }

 static Uint8List dataFromBase64String(String base64String) {
   return base64Decode(base64String);
 }

 static String base64String(Uint8List data) {
   return base64Encode(data);
 }


 static String whatsapp="919846290789";
 static String domain="https://mysaving.in/";
}