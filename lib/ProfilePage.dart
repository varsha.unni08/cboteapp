

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_app_ecommerce/Pages/MyOrders.dart';
import 'package:flutter_app_ecommerce/domain/ProfileStatus.dart';

import 'Pages/MyAddress.dart';
import 'Pages/Wallet.dart';
import 'Pages/Wishlist.dart';
import 'constants/DataConstants.dart';
import 'constants/UrlConstants.dart';
import 'design/ResponsiveInfo.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'dart:convert';
import 'package:image_picker/image_picker.dart';
import 'package:image_cropper/image_cropper.dart';
import 'dart:io';

class ProfilePage extends StatefulWidget {
  final String title;

  const ProfilePage({Key? key, required this.title}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ProfilePage();
}

class _ProfilePage extends State<ProfilePage> {


  String profiledata="";

  String profileimage="";


  @override
  void initState() {
    // TODO: implement initState

    getProfileDetails();

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset : false,
      appBar: AppBar(
        elevation: 0,
        backgroundColor:Color(0xffEFECEC),
        leading: IconButton(
          icon: Icon(Icons.arrow_back,color: Colors.black, ),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text("Profile",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),
        centerTitle: true,
      ),
        body: Stack(children: <Widget>[
        new Container(
          color: Color(0xffEBEBEB),
      ),

    SingleChildScrollView(
    child: Column(children: <Widget>[
    Align(
    alignment: Alignment.center,
    child: Container(
        width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120:160:220,
        height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?120:160:220,
    child: Padding(
    padding: EdgeInsets.all(10),
    child: Stack(children: [
      new Container(
          width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,
          height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,

        child: ClipOval(

          child: Image.network(
    profileimage,
              errorBuilder: (context, error, stackTrace) {
                return Container(

                  width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,
                  height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,

                  alignment: Alignment.center,
                  child: Image.asset("images/userprofileicon.png",    width: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,
                    height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180,),
                );
              },
    fit: BoxFit.fill),
    clipper: MyClip(context),
    ),




 ),







    Align(

    alignment: Alignment.bottomRight,
    child: FloatingActionButton(
      mini: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?true:true:false,
    onPressed: () async {
      final ImagePicker _picker = ImagePicker();
      // Pick an image
      final XFile? image = await _picker.pickImage(source: ImageSource.gallery);

      // print(image!.path) ;

      if(image!=null) {
        File? croppedFile = await new ImageCropper()
            .cropImage(
            sourcePath: image.path,
            aspectRatioPresets: [
              CropAspectRatioPreset.square,
              CropAspectRatioPreset.ratio3x2,
              CropAspectRatioPreset.original,
              CropAspectRatioPreset.ratio4x3,
              CropAspectRatioPreset.ratio16x9
            ],
            androidUiSettings: AndroidUiSettings(
                toolbarTitle: 'Cropper',
                toolbarColor: Colors.deepOrange,
                toolbarWidgetColor: Colors.white,
                initAspectRatio: CropAspectRatioPreset
                    .original,
                lockAspectRatio: false),
            iosUiSettings: IOSUiSettings(
              minimumAspectRatio: 1.0,
            )
        );


        uploadImage(croppedFile);
      }

    }



    ,
    child: Icon(
    Icons.edit,
    color: Colors.white,
    size: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?20:30:50,
    ),
    backgroundColor: Color(0xfff55245),
    tooltip: 'Capture Picture',
    elevation: 5,
    splashColor: Colors.grey,
    ))
    ])))),


        Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?8:10:15),

          child:Center(child:


          Text(profiledata,style: TextStyle(color: Colors.black, fontSize:ResponsiveInfo.isMobile(context)? 15:20))),



        ),

      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?8:10:15),

        child:GestureDetector(

          onTap: (){

            Navigator.push(
                context, MaterialPageRoute(builder: (_) => WishlistPage(title: "Wishlist",)));

          },


          child: Card(

          elevation: 8,
          shape: RoundedRectangleBorder(
            side: BorderSide(color: Colors.white70, width: 0),
            borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
          ),
          child: Container(
            width: double.infinity,
            height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90 ,

            child:
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,

              children: [
                
                Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.fromLTRB(0, 0, 35, 0):EdgeInsets.fromLTRB(0, 0, 100, 0),

                  child:Image.asset("images/heart.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                  ,


                ),

                Center(

                  child: Text("Wishlist",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                ),

                Padding(

                  padding:EdgeInsets.fromLTRB(80, 0, 0, 0) ,

                  child: Image.asset("images/next.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                  ,

                )




              ],

            ),
            )


          ),


        )



      ),
      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?8:10:15),

          child:GestureDetector(

            onTap: (){

              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => MyOrderPage(title: "Orders",)));

            },

            child: Card(

            elevation: 8,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 0),
              borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
            ),
            child: Container(
              width: double.infinity,
              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90 ,
              child:
                Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,

                children: [

                  Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.fromLTRB(0, 0, 35, 0):EdgeInsets.fromLTRB(0, 0, 100, 0),

                    child:Image.asset("images/bag.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                    ,


                  ),

                  Center(

                    child: Text("My Orders",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                  ),

                  Padding(

                    padding:EdgeInsets.fromLTRB(80, 0, 0, 0) ,

                    child: Image.asset("images/next.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                    ,

                  )




                ],

              ),)
            ),


          )



      ),
      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?8:10:15),

          child: GestureDetector(

            onTap: (){

              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => WalletPage(title: "Wallet",)));


              // Navigator.push(
              //     context, MaterialPageRoute(builder: (_) => MyOrderPage(title: "Orders",)));

            },

            child:Card(

            elevation: 8,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 0),
              borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
            ),
            child: Container(
              width: double.infinity,
              height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90 ,
              child:



              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,

                children: [

                  Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.fromLTRB(0, 0, 35, 0):EdgeInsets.fromLTRB(0, 0, 100, 0),

                    child:Image.asset("images/purse.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                    ,


                  ),

                  Center(

                    child: Text("My wallet",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                  ),

                  Padding(

                    padding:EdgeInsets.fromLTRB(80, 0, 0, 0) ,

                    child: Image.asset("images/next.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                    ,

                  )




                ],

              ))
            ),


          )



      ),
      Padding(padding: EdgeInsets.all(ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?8:10:15),

          child:      GestureDetector(

            onTap: (){

              Navigator.push(
                  context, MaterialPageRoute(builder: (_) => MyAddressPage(title: "My Address", cartid: '0', code: '1',)));

            },

            child:Card(

            elevation: 8,
            shape: RoundedRectangleBorder(
              side: BorderSide(color: Colors.white70, width: 0),
              borderRadius: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?BorderRadius.circular(10):BorderRadius.circular(15):BorderRadius.circular(20),
            ),
            child:GestureDetector(

              onTap: (){

                Navigator.push(
                    context, MaterialPageRoute(builder: (_) => MyAddressPage(title: "My Address", cartid: '0', code: '1',)));

              },


              child:Container(
                width: double.infinity,
                height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?50:70:90 ,
                child:




                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,

                  children: [

                    Padding(padding: ResponsiveInfo.isMobile(context)?EdgeInsets.fromLTRB(0, 0, 35, 0):EdgeInsets.fromLTRB(0, 0, 100, 0),

                      child:Image.asset("images/home.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                      ,


                    ),

                    Center(

                      child: Text("Address list",style: TextStyle(fontSize: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?13:16:19,color: Colors.black),),


                    ),

                    Padding(

                      padding:EdgeInsets.fromLTRB(80, 0, 0, 0) ,

                      child: Image.asset("images/next.png",width:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60, height:ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?30:45:60)
                      ,

                    )




                  ],

                ),)
            )






            ),


          )



      )







    ]

    )
  )]));
}



getProfileDetails() async
{
  ProgressDialog _progressDialog = ProgressDialog();
  _progressDialog.showProgressDialog(
      context, textToBeDisplayed: "Please wait for a moment......");
  final datacount = await SharedPreferences.getInstance();
  var date = new DateTime.now().toIso8601String();
  var dataasync = await http.post(
    Uri.parse(UrlData.baseurl + UrlData.getProfile),
    headers: <String, String>{
      'Content-Type': 'application/x-www-form-urlencoded',
      UrlData.header_token : datacount.getString(DataConstants.tokenkey)!
    },
  );
  _progressDialog.dismissProgressDialog(context);
  String response = dataasync.body;

  print(response);

  dynamic jsondata = jsonDecode(response);

  ProfileStatus profileStatus=ProfileStatus.fromJson(jsondata);

  if(profileStatus.status==1)
    {

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Success"),
      ));

      setState(() {

        if(profileStatus.data!=null) {
          profiledata = profileStatus.data.customer_name+"\n"+profileStatus.data.phone1.toString();
          profileimage=UrlData.profileimageurl+profileStatus.data.image;




        }


      });

    }
  else{

    ScaffoldMessenger.of(context).showSnackBar(SnackBar(
      content: Text("Failed"),
    ));

  }





}
  uploadImage(File? file) async {
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(
        context, textToBeDisplayed: "Please wait for a moment......");


    final datacount = await SharedPreferences.getInstance();
    var request = http.MultipartRequest('POST', Uri.parse(UrlData.baseurl+UrlData.uploadUserprofile));
    request.headers.addAll( { UrlData.header_token: datacount.getString(DataConstants.tokenkey)!,
      'Content-Type': 'application/x-www-form-urlencoded'});


    request.files.add(
        http.MultipartFile.fromBytes(
            'file',
            File(file!.path).readAsBytesSync(),
            filename: file.path.split("/").last
        )
    );

    _progressDialog.dismissProgressDialog(context);
    var res = await request.send();
    var responseData = await res.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);

    getProfileDetails();

    print(responseString);

  }

}

class MyClip extends CustomClipper<Rect> {

  BuildContext context;


  MyClip(this.context);

  Rect getClip(Size size) {
    return Rect.fromLTWH(0, 0, ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180, ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?80:120:180);
  }

  bool shouldReclip(oldClipper) {
    return false;
  }


}