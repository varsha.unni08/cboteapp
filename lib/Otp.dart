import 'dart:convert';

import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_app_ecommerce/constants/UrlConstants.dart';
import 'package:flutter_app_ecommerce/homepage.dart';

import 'dart:io';

import 'constants/DataConstants.dart';
import 'design/ResponsiveInfo.dart';
import 'login.dart';

import 'package:custom_progress_dialog/custom_progress_dialog.dart';
import 'package:http/http.dart' as http;

import 'package:shared_preferences/shared_preferences.dart';

void main() {
  // runApp(MyApp());


  runApp(MyApp());

  // sleep(const Duration(seconds:3));
}




class Otppage extends StatefulWidget{


  Otppage({Key? key, required this.title, required this.mobile, required this.email, required this.password, required this.name, required this.pincode_id}) : super(key: key);

  final String title;
  final String mobile;
  final String email;
  final String password;
  final String name;
  final String pincode_id;


  @override
  _MyOtpState createState() => _MyOtpState(mobile,email,password,name,pincode_id);
}


class _MyOtpState extends State<Otppage> {
  int _counter = 0;

  int code=0;


  String dropdownValue = 'Select country';

  TextEditingController otpcodeController=new TextEditingController();


   String mobile="";
   String email="";
   String password="";

   String otpcode="";
   String name="";
   String pincodeid="";


  _MyOtpState(this.mobile, this.email, this.password,this.name,this.pincodeid);



  @override
  void initState() {
    // TODO: implement initState

    sendOtpcode();


    super.initState();


    // sleep(const Duration(seconds:6));
    // startTime();
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    double dw=width/1.4;

    return Scaffold(
      resizeToAvoidBottomInset : false,

      body: Container(


        width: double.infinity,
        height: double.infinity,
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(




          children: [

            Container(

              width: double.infinity,
              height: ResponsiveInfo.isMobile(context)?ResponsiveInfo.isSmallMobile(context)?220:320:450,

              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("images/bg.png"),
                  fit: BoxFit.fill,
                ),
              ),

              child: Stack(

                children: [


                  Align(

                    alignment: Alignment.center,
                    child:  ResponsiveInfo.isMobile(context)?Image.asset('images/ph.png',width: 130,height: 130,):Image.asset('images/ph.png',width: 260,height: 260,),

                  )



                ],


              ),



            ),




            ResponsiveInfo.isMobile(context)?  Text("\n\n A code is sent to "+mobile,style: TextStyle(fontSize: 14,color: Colors.black),):
            Text("\n\n A code is sent to "+mobile,style: TextStyle(fontSize: 25,color: Colors.black),),

            Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(15, 10, 15, 0)   :EdgeInsets.fromLTRB(30, 20, 30, 0),

              child:Container(


                child: Padding(
                  padding: ResponsiveInfo.isMobile(context)?  EdgeInsets.all(8) :EdgeInsets.all(16) ,

                  child:Container(

                      decoration: BoxDecoration(
                          border: Border.all(color: Colors.black45,width:ResponsiveInfo.isMobile(context)? 0.5:1)
                      ),

                      child: Row(
                        textDirection: TextDirection.ltr,
                        children: <Widget>[

                          Padding(padding: ResponsiveInfo.isMobile(context)? EdgeInsets.all(8):EdgeInsets.all(16),

                              child: ResponsiveInfo.isMobile(context)? Image.asset("images/otp.png",width: 25,height: 25,

                              ):Image.asset("images/otp.png",width: 45,height: 45,

                              )
                          )
                          ,
                          Expanded(child: new Theme(data: new ThemeData(
                              hintColor: Colors.black45
                          ), child: TextField(

                            controller: otpcodeController,
                            keyboardType: TextInputType.number,
                            obscureText: true,
                            style:ResponsiveInfo.isMobile(context)?  TextStyle(fontSize: 15, height: 1.3):TextStyle(fontSize: 23, height: 1.5),
                            decoration: InputDecoration(
                              border: InputBorder.none,

                              hintText: 'OTP code',
                            ),


                          )))
                        ],
                      )

                  )

                  //padding: EdgeInsets.symmetric(horizontal: 15),
                  ,
                ),







              )



              ,),


            Padding(padding:ResponsiveInfo.isMobile(context)? EdgeInsets.fromLTRB(0, 29, 0, 10):EdgeInsets.fromLTRB(0, 50, 0, 20),
                child:  Container(

                  width:ResponsiveInfo.isMobile(context)? 120:200,
                  height:ResponsiveInfo.isMobile(context)? 50:90,
                  decoration: BoxDecoration(
                      color: Color(0xfff55245),
                      border: Border.all(
                        color: Color(0xfff55245),
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(ResponsiveInfo.isMobile(context)?20:40))


                  ),
                  child: TextButton(onPressed: () {


                    if(otpcodeController.text.isNotEmpty)
                      {
                        if(otpcodeController.text.compareTo(otpcode)==0)
                          {

                            // Navigator.pushReplacement(context, MaterialPageRoute(
                            //     builder: (context) => MyHomePage(title: "Login",)
                            // )
                            // );

                            registerUser();

                          }
                        else{

                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text("Enter correct OTP code"),
                          ));

                        }

                      }
                    else{

                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text("Enter OTP code"),
                      ));

                    }




                  },
                    child: Text("Submit",style: TextStyle(color: Colors.white,fontSize:ResponsiveInfo.isMobile(context)? 17:26),),





                  ),




                )),














          ],





        ),
      ),
      // This trailing comma makes auto-formatting nicer for build methods.
    );





  }


  registerUser() async
  {
    final datastorage = await SharedPreferences.getInstance();
    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");
    var date = new DateTime.now().toIso8601String();

    var dataasync = await http.post(
        Uri.parse(UrlData.baseurl+UrlData.registration),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        },

        body: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

          'name': name,
          'email': email,
          'password': password,
          'mobile': mobile,
          'pincode_id':pincodeid

        }

    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;



    dynamic jsondata = jsonDecode(response);

    int status = jsondata['status'];

    String token=jsondata['token'];

    if(status==1)
    {
      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Registration completed successfully"),
      ));

      datastorage.setString(DataConstants.tokenkey, token);

      Navigator.pushReplacement(context, MaterialPageRoute(
          builder: (context) => MyHomePage(title: "HomePage",)
      )
      );

    }
    else{

      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
        content: Text("Failed to register user"),
      ));
    }



  }






  sendOtpcode() async
  {
    var rng = new Random();
    var code = rng.nextInt(9000) + 1000;



    ProgressDialog _progressDialog = ProgressDialog();
    _progressDialog.showProgressDialog(context,textToBeDisplayed: "Please wait for a moment......");


    var dataasync = await http.get(
        Uri.parse(UrlData.smsbaseurl+"/"+UrlData.smsapikey+"/SMS/"+mobile+"/"+code.toString()+"/"+UrlData.smstemplate),

        headers: <String, String>{
          'Content-Type': 'application/x-www-form-urlencoded',

        },



    );



    _progressDialog.dismissProgressDialog(context);

    String response = dataasync.body;


    setState(() {

      otpcode=code.toString();
    });

    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(
    //   content: Text('otp code is : '+code.toString()),
    // ));
  }




}